# DentalBookDomain

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `dental_book_domain` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [{:dental_book_domain, "~> 0.1.0"}]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/dental_book_domain](https://hexdocs.pm/dental_book_domain).

