defmodule DentalBookDomain do
  @moduledoc """
  Documentation for DentalBookDomain.
  """

  @doc """
  Hello world.

  ## Examples

      iex> DentalBookDomain.hello
      :world

  """
  def hello do
    :world
  end
end
