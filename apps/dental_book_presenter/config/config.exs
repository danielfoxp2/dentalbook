# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :dental_book_presenter,
  ecto_repos: [DentalBookPresenter.Repo]

# Configures the endpoint
config :dental_book_presenter, DentalBookPresenter.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "x+rZulsyOJpHEFVcWTbakBMo6khJgfbWtmBva/rxRbcIIETmTq2KeuMB010S1b3M",
  render_errors: [view: DentalBookPresenter.ErrorView, accepts: ~w(html json)],
  pubsub: [name: DentalBookPresenter.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# %% Coherence Configuration %%   Don't remove this line
config :coherence,
  user_schema: DentalBookPresenter.User,
  repo: DentalBookPresenter.Repo,
  module: DentalBookPresenter,
  logged_out_url: "/sessions/new",
  email_from_name: "Dentalbook",
  email_from_email: "esqueciminhasenha@dentalbook.com.br",
  opts: [:registerable, :trackable, :recoverable, :invitable, :authenticatable]

config :coherence, DentalBookPresenter.Coherence.Mailer,
  adapter: Swoosh.Adapters.Mailgun,
  api_key: System.get_env("AK_MAILGUN"), 
  domain: "dentalbook.com.br"
# %% End Coherence Configuration %%

# %% Rummage Configuration %% 
config :rummage_ecto, Rummage.Ecto,
  default_repo: DentalBookPresenter.Repo

# %% End Rummage Configuration %%

# %% Gettext Configuration %% 
config :dental_book_presenter, DentalBookPresenter.Gettext,
  default_locale: "pt_BR"
# %% End Gettext Configuration %%

# %% Money Configuration %%
config :money,
  default_currency: :BRL,
  separator: ".",
  delimeter: ",",
  symbol: true,
  symbol_on_right: false,
  symbol_space: true

config :task_after, 
  global_name: TaskAfter