use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :dental_book_presenter, DentalBookPresenter.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :dental_book_presenter, DentalBookPresenter.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "dental_book_presenter_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
