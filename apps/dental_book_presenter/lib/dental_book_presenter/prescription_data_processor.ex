defmodule DentalBookPresenter.PrescriptionDataProcessor do
  
  alias DentalBookPresenter.DateFormat

  def get_prescription_data(params) do
    %{
      patient_id: params["patient_id"],
      patient_name: params["patient_name"],
      dentist_name: params["dentist_name"],
      dentist_cro: params["dentist_cro"],
      emitted_date: DateFormat.now |> DateFormat.ecto_date_to_string,
      prescription_content: get_prescription_content_grouped_by_medicine_use_type_from(params["prescription_content"])
    }
  end
  
  defp get_prescription_content_grouped_by_medicine_use_type_from(prescriptions_contents) do
    prescriptions_contents
    |> Enum.map(&get_prescription_from/1)
    |> Enum.group_by(&use_type/1)
  end
  
  defp get_prescription_from(prescription_content) do
    Enum.reduce(prescription_content, %{}, &get_prescription_map(&1, &2))
  end  
  
  defp use_type(in_prescription) do
    use_type_index = "1"
    in_prescription[use_type_index]
  end
  
  defp get_prescription_map(param, map_accumulator) do
    {key, value} = get_key_value_from_content_inside(param)
    Map.put(map_accumulator, key, value)
  end
  
  defp get_key_value_from_content_inside(prescription_item) do
    divide(prescription_item, ":") 
    |> create_key_value 
  end
  
  defp divide(collection, separator), do: String.split(collection, separator)
  
  defp create_key_value([first, last]), do: {first, String.trim(last)}
end