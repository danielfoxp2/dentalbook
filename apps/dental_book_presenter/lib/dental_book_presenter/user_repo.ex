defmodule DentalBookPresenter.UserRepo do
  alias DentalBookPresenter.CompanyRepo
  alias DentalBookPresenter.CompanyUser
  alias DentalBookPresenter.User
  import Ecto.Query

  def get_rummage_user_query(user_id, is_admin, repo) when is_admin == true do
    get_user_ids_query(user_id, repo) 
    |> repo.all
    |> get_user_query
  end
 
  def get_rummage_user_query(user_id, is_admin, repo) do
    [user_id] |> get_user_query
  end

  def search_dentists(user_id, search_term, repo) do
    user_company_id = user_id |> CompanyRepo.get_id(repo)

    query = from u in User, 
    inner_join: cmp in CompanyUser,
    on: cmp.user_id == u.id
    and cmp.company_id == ^user_company_id,
    where: ilike(u.name, ^search_term)
    and u.role == "Profissional de Saúde"
    and not ilike(u.name, "%Sauron%")
    and not ilike(u.email, "%tenant_company%"), 
    select: %{id: u.id, name: u.name, cro: u.cro}

    repo.all(query)
  end

  def update(user, user_params, repo) do
    User.changeset(user, user_params)
    |> repo.update
  end

  def get_user({user_emails, repo}) do
    user_by_emails = from u in User,
    where: u.email in ^user_emails
    
    repo.one(user_by_emails)
  end

  defp get_user_ids_query(user_id, repo) do
    company_id = CompanyRepo.get_id(user_id, repo)

    user_query = from u in User,
    join: cmp in CompanyUser,
    on: cmp.company_id == ^company_id 
    and u.id == cmp.user_id,
    where: not ilike(u.name, "%Sauron%")
    and not ilike(u.email, "%tenant_company%") 

    from user in user_query, select: user.id
  end

  defp get_user_query(users_id) do
    from u in User,
    where: u.id in ^users_id
  end

end