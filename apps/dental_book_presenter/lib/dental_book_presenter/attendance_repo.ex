defmodule DentalBookPresenter.AttendanceRepo do
  import Ecto.Query
  alias DentalBookPresenter.Appointment
  alias DentalBookPresenter.AppointmentProcedure
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.Repo
  alias DentalBookPresenter.Patient
  alias DentalBookPresenter.DateFormat

  def get_scheduled_patients_of_the_week(user_id) do
    Apartmentex.all(Repo, get_scheduled_attendances_query(), Tenant.get_name(user_id, Repo))
  end

  defp get_scheduled_attendances_query() do
    from patient in Patient,
    inner_join: appointment in Appointment,
    on: appointment.patient_id == patient.id,
    inner_join: procedure in AppointmentProcedure,
    on: procedure.appointment_id == appointment.id,
    where: procedure.attendance_date_at >= ^DateFormat.now()
    and procedure.attendance_date_at <= ^DateFormat.seven_days_from_now(),
    order_by: [procedure.attendance_date_at, procedure.attendance_hour],
    select: %{
      appointment_id: appointment.id,
      patient_name: patient.name, 
      patient_cellphone: patient.cellphone, 
      attendance_date: procedure.attendance_date_at, 
      attendance_hour: procedure.attendance_hour
    }
  end

end