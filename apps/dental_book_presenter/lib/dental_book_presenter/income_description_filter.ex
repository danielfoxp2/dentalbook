defmodule DentalBookPresenter.IncomeDescriptionFilter do
  import Ecto.Query

  def build(:not_selected), do: true
  def build(income_description) do
    searched_description = "%#{income_description}%"
    dynamic([income], ilike(income.description, ^searched_description))
  end
end