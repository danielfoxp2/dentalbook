defmodule DentalBookPresenter.IncomeByProcedure do
  alias DentalBookPresenter.IncomeBudget
 
  def group(all_incomes) do
    all_incomes
    |> group_by_procedure_name
    |> calculate_total_by_procedure
    |> calculate_total_amount
    |> calculate_percentage_by_procedure
    |> order_procedures_by_amount
  end

  defp group_by_procedure_name(all_incomes) do
    all_incomes
    |> Enum.group_by(&procedure_name/1)
    |> Map.to_list
  end

  defp calculate_total_by_procedure(incomes_grouped_by_procedure) do
    incomes_grouped_by_procedure
    |> Enum.map(&total_by_procedure/1)
  end

  defp calculate_total_amount(incomes_grouped_by_procedure) do
    total_incomes_result = incomes_grouped_by_procedure |> Enum.reduce(Money.new(0), &get_total_of_all_incomes/2)
    {total_incomes_result, incomes_grouped_by_procedure}
  end

  defp calculate_percentage_by_procedure({total_incomes_result, incomes_grouped_by_procedure}) do
    incomes_fully_calculated = Enum.map(incomes_grouped_by_procedure, &get_percentage(&1, total_incomes_result))
    {total_incomes_result, incomes_fully_calculated}
  end

  defp order_procedures_by_amount({total_incomes_result, incomes_fully_calculated}) do
    ordered_procedures_incomes = incomes_fully_calculated |> Enum.sort(&by_amount/2)
    {total_incomes_result, ordered_procedures_incomes}
  end

  defp procedure_name(income), do: get(income.procedure_name) |> String.trim

  defp total_by_procedure({procedure_name, incomes}) do
    total_by_procedure = incomes |> IncomeBudget.get_total_as_money_of
    %{procedure_name: procedure_name, incomes_amount: total_by_procedure}
  end

  defp get_total_of_all_incomes(%{incomes_amount: incomes_amount} = params, calculated_till_now) do
    calculated_till_now 
    |> Money.add(incomes_amount)
  end

  defp get_percentage(%{incomes_amount: incomes_values} = procedure_income, total_incomes_result) do
    amount_percentage = calculate_percentage(incomes_values.amount, total_incomes_result.amount)

    %{
      procedure_name: procedure_income.procedure_name, 
      incomes_amount: procedure_income.incomes_amount, 
      percentage_of_the_total: amount_percentage
    }
  end

  defp by_amount(%{incomes_amount: current_element}, %{incomes_amount: next_element}) do
    current_element.amount > next_element.amount
  end

  defp calculate_percentage(procedure_income, total_incomes_result) do
    (procedure_income / total_incomes_result) * 100
    |> Float.round(2)
  end

  defp get(procedure_name) when is_nil(procedure_name), do: "Receitas Avulsas"
  defp get(procedure_name), do: procedure_name

end