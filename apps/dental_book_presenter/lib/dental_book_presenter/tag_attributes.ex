defmodule DentalBookPresenter.TagAttributes do
  defstruct [
    consider_id: true,
    field_id: nil,
    field_name: nil, 
    field_type: nil,
    field_value: nil,
    field_class: nil,
    field_enabled?: true    
  ]
end