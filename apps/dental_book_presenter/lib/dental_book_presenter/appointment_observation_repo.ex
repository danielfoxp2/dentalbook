defmodule DentalBookPresenter.AppointmentObservationRepo do
  import Ecto.Query
  alias DentalBookPresenter.AppointmentUI
  alias DentalBookPresenter.AppointmentObservation
  alias DentalBookPresenter.Repo
  alias DentalBookPresenter.Appointment

  def insert_observations(appointment_created, tenant_name, appointment_params) do
    observations = AppointmentUI.get_observations_from(appointment_params["observations"], appointment_created.id)

    appointment_observations_changesets = 
    get_appointment_observations_relation(appointment_created, observations)
    |> Enum.map(&create_changeset_from/1)

    insert_appointment_observations_relations(appointment_observations_changesets, tenant_name)    
  end

  def search_observations_of(appointment_id) do
    from appointment_observation in AppointmentObservation,
    where: appointment_observation.appointment_id == ^appointment_id,
    select: %{
      observation_id: appointment_observation.observation_id,
      observation: appointment_observation.observation,
      creation_date: appointment_observation.creation_date,
      creation_hour: appointment_observation.creation_hour
    }
  end
  
  def delete_observations_of(%Appointment{} = appointment, tenant_name) do
    {result, _} = Apartmentex.delete_all(Repo, get_appointment_observations_query(appointment.id), tenant_name)
    {:ok, result}
  end

  def delete_observations_of(appointment_id, tenant_name) do
    {result, _} = Apartmentex.delete_all(Repo, get_appointment_observations_query(appointment_id), tenant_name)
    {:ok, result}
  end

  defp get_appointment_observations_query(appointment_id) do
    from obs in AppointmentObservation,
    where: obs.appointment_id == ^appointment_id
  end

  defp get_appointment_observations_relation(appointment_created, observations) do
    Enum.map observations, fn(appointment_observation) -> 
    %{ 
        appointment_id: appointment_created.id, 
        observation_id: appointment_observation.observation_id, 
        observation: appointment_observation.observation,
        creation_date: appointment_observation.creation_date,
        creation_hour: appointment_observation.creation_hour
    } 
    end
  end

  defp create_changeset_from(appointment_observation_relation) do
    AppointmentObservation.changeset(%AppointmentObservation{}, appointment_observation_relation)
  end

  defp insert_appointment_observations_relations(appointment_observations_changesets, tenant_name) do
    observations_details = Enum.map appointment_observations_changesets, fn appointment_observation_changeset -> 
      Apartmentex.insert(Repo, appointment_observation_changeset, tenant_name)
    end
  
    get_result_of_insert(observations_details)
  end

  defp get_result_of_insert(inserted_items) do
    Enum.find inserted_items, {:ok, inserted_items}, fn(inserted_item) -> 
      :error == elem(inserted_item, 0)
    end  
  end

end