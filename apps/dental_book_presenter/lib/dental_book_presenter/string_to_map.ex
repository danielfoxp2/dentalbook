defmodule DentalBookPresenter.StringToMap do
  
  def convert(string) do
    {status, map} = string 
    |> String.replace("\n", "") 
    |> String.trim
    |> String.replace("%", "")
    |> String.replace("=&gt;", ":")
    |> String.replace("=>", ":")
    |> Poison.decode
  
    map
  end

end