defmodule DentalBookPresenter.IncomeDateIntervalFilter do
  import Ecto.Query
  alias DentalBookPresenter.DateFormat
  
  def build({:not_selected, :not_selected}), do: true
  def build({initial_date_param, final_date_param}) do
    {initial_date, final_date} = get_interval_filter(initial_date_param, final_date_param)

    due_date_begin = dynamic([income], income.due_date_at >= ^initial_date)
    due_date_end = dynamic([income], income.due_date_at <= ^final_date)
    paid_at_begin = dynamic([income], income.paid_at >= ^initial_date)
    paid_at_end = dynamic([income], income.paid_at <= ^final_date)

    dynamic((^due_date_begin and ^due_date_end) or (^paid_at_begin and ^paid_at_end))
  end

  defp get_interval_filter(initial_date, :not_selected), do: {format(initial_date), format(Ecto.Date.cast({9999, 12, 31}))}
  defp get_interval_filter(:not_selected, final_date), do: {format(Ecto.Date.cast({2000, 1, 1})), format(final_date)}
  defp get_interval_filter(initial_date, final_date), do: {format(initial_date), format(final_date)}

  defp format({_, date}), do: date
  defp format(date), do: DateFormat.string_to_ecto_date(date)

end