defmodule DentalBookPresenter.PatientFinancialHistory do
  alias DentalBookPresenter.IncomeBudget
  alias DentalBookPresenter.DateFormat
  
  def get_data_in(patient_history) do
    %{
      all_paid_bills: get_all_paid_bills_in(patient_history),
      all_unpaid_bills_without_overdue: get_all_unpaid_bills_without_overdue_in(patient_history),
      all_overdue_unpaid_bills: get_all_overdue_unpaid_bills_in(patient_history),
      total_paid_bills: get_total_paid_in(patient_history),
      total_unpaid_bills_without_overdue: get_total_unpaid_bills_without_overdue(patient_history),
      total_overdue_bills: get_total_overdue_bills(patient_history)
    }
  end

  defp get_all_paid_bills_in(patient_history) do
    Enum.filter(patient_history, &is_a_paid_clinical_procedure?/1)
  end

  defp get_all_unpaid_bills_without_overdue_in(patient_history) do
    Enum.filter(patient_history, &is_unpaid_and_not_overdue?/1)
  end

  defp get_all_overdue_unpaid_bills_in(patient_history) do
    Enum.filter(patient_history, &is_unpaid_and_overdue?/1)
  end

  defp get_total_paid_in(patient_history) do
    get_all_paid_bills_in(patient_history)
    |> IncomeBudget.get_total_of
  end

  defp get_total_unpaid_bills_without_overdue(patient_history) do
    get_all_unpaid_bills_without_overdue_in(patient_history)
    |> IncomeBudget.get_total_of
  end

  defp get_total_overdue_bills(patient_history) do
    get_all_overdue_unpaid_bills_in(patient_history)
    |> IncomeBudget.get_total_of
  end
  
  defp is_a_paid_clinical_procedure?(clinical_procedure) do
    clinical_procedure.paid_at != nil && clinical_procedure.paid_at != ""
  end

  defp is_unpaid_and_not_overdue?(clinical_procedure) do
    is_unpaid? = is_a_paid_clinical_procedure?(clinical_procedure) == false
    is_done? = clinical_procedure.done == true
    is_not_overdue? = due_date_is_not_overdue?(clinical_procedure.due_date_at)

    is_unpaid? && is_done? && is_not_overdue? 
  end

  defp is_unpaid_and_overdue?(clinical_procedure) do
    is_unpaid? = is_a_paid_clinical_procedure?(clinical_procedure) == false
    is_done? = clinical_procedure.done == true
    is_overdue? = due_date_is_not_overdue?(clinical_procedure.due_date_at) == false

    is_unpaid? && is_done? && is_overdue? 
  end

  defp due_date_is_not_overdue?(due_date) do
    Ecto.Date.compare(due_date, DateFormat.now)
    |> is_not_overdue?
  end

  defp is_not_overdue?(:gt), do: true
  defp is_not_overdue?(:eq), do: true
  defp is_not_overdue?(:lt), do: false

end 