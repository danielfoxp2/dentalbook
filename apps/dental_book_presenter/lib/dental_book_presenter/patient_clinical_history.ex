defmodule DentalBookPresenter.PatientClinicalHistory do
  def get_data_in(patient_history) do
    Enum.filter(patient_history, &get_clinical_done_procedure/1)
  end

  defp get_clinical_done_procedure(clinical_procedure), do: clinical_procedure.done == true
end 