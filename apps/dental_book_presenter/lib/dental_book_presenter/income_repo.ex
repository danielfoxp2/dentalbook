defmodule DentalBookPresenter.IncomeRepo do
  import Ecto.Query
  alias DentalBookPresenter.{AppointmentUI, Income, Repo, Appointment, AppointmentProcedure}

  def insert_incomes(inserted_procedures, tenant_name, appointment_params) do
    list_of_procedure_income = get_procedures_incomes(appointment_params)
    inserted_appointments_procedures = get_inserted_appointment_procedures(inserted_procedures)

    merge_incomes_with_appointment_procedures(list_of_procedure_income, inserted_appointments_procedures, [])
    |> get_incomes_changesets
    |> insert_appointment_incomes(tenant_name)
  end

  defp get_procedures_incomes(appointment_params) do
    appointment_params["procedures_incomes"]
    |> Enum.map(fn (procedure_income_tuple) -> get_procedure_income(procedure_income_tuple) end)
  end
  defp get_procedure_income({_, procedure_income}), do: procedure_income

  defp get_inserted_appointment_procedures(inserted_procedures) do
    inserted_procedures
    |> Enum.map(fn {key, inserted_ap} -> inserted_ap end)
  end

  defp merge_incomes_with_appointment_procedures([], [], final_result), do: final_result
  defp merge_incomes_with_appointment_procedures(list_of_procedure_income, inserted_appointments_procedures, partial_result) do
    [first_procedure_income | reminders_procedures_incomes] =  list_of_procedure_income
    [first_appointment_procedure |  reminders_appointment_procedures] =  inserted_appointments_procedures

    result = AppointmentUI.get_incomes_params_from(first_procedure_income, first_appointment_procedure.id)

    partial_result = partial_result ++ [result]
    merge_incomes_with_appointment_procedures(reminders_procedures_incomes, reminders_appointment_procedures, partial_result)
  end

  def search_appointment_incomes(appointment_id) do
    from ic in get_incomes_query(appointment_id),
    select: %{
      id: ic.id, 
      description: ic.description, 
      due_date: ic.due_date_at,
      paid_at: ic.paid_at, 
      value: ic.value, 
      procedure_id: ic.procedure_id
    }
  end
 
  def delete_incomes_of(%Appointment{} = appointment, tenant_name) do
    {entries_affected, result} = Apartmentex.delete_all(Repo, get_incomes_query(appointment), tenant_name, returning: true)
    if result > 0 do
      {:ok, entries_affected}
    else
      {:error, result}
    end
  end

  def delete_incomes_of({appointment_procedures, _}, tenant_name) do
    {entries_affected, result} = Apartmentex.delete_all(Repo, get_incomes_query(appointment_procedures), tenant_name, returning: true)
    if result > 0 do
      {:ok, entries_affected}
    else
      {:error, result}
    end
  end

  defp get_incomes_changesets(income_params) do
    Enum.map income_params, fn(income_param) -> Income.changeset(%Income{}, income_param) end
  end

  defp insert_appointment_incomes(incomes_changesets, tenant_name) do
    incomes_details = Enum.map incomes_changesets, fn income_changeset ->
      Apartmentex.insert(Repo, income_changeset, tenant_name)
    end 

    get_result_of_insert(incomes_details)
  end

  defp get_result_of_insert(inserted_items) do
    Enum.find inserted_items, {:ok, :not_utilized}, fn(inserted_item) -> 
      :error == elem(inserted_item, 0)
    end  
  end

  defp get_incomes_query(%Appointment{} = appointment) do
    from ic in Income,
    inner_join: ap in AppointmentProcedure,
    on: ap.id == ic.appointment_procedure_id,
    where: ap.appointment_id == ^appointment.id
  end

  defp get_incomes_query(appointment_procedures) do
    incomes = Enum.map(appointment_procedures, fn appointment_procedure -> appointment_procedure.income_id end)

    from ic in Income,
    where: ic.id in ^incomes
  end
end