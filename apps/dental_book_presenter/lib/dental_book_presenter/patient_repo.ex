defmodule DentalBookPresenter.PatientRepo do
  import Ecto.Query
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.Repo
  alias DentalBookPresenter.Appointment
  alias DentalBookPresenter.AppointmentProcedure
  alias DentalBookPresenter.Procedure
  alias DentalBookPresenter.Patient
  alias DentalBookPresenter.Income
  alias DentalBookPresenter.Anamnesis
  alias DentalBookPresenter.SickNote
  alias DentalBookPresenter.Prescription

  def get_history_of(patient_id, user_id) do
    Apartmentex.all(Repo, get_history_query(patient_id), Tenant.get_name(user_id, Repo))
  end

  def get_anamnesis_history(patient_id, user_id) do
    Apartmentex.all(Repo, get_anamnesis_history_query(patient_id), Tenant.get_name(user_id, Repo))
    |> Enum.map(fn anamnesis -> update_in(anamnesis.inserted_at, &shift_time_zone_of(&1)) end)
  end

  def get_sick_notes_history(patient_id, user_id) do
    Apartmentex.all(Repo, get_sick_notes_history_query(patient_id), Tenant.get_name(user_id, Repo))
    |> Enum.map(fn sick_note -> update_in(sick_note.inserted_at, &shift_time_zone_of(&1)) end)
  end

  def get_answered_anamnesis(anamnesis_id, user_id) do
    Apartmentex.one!(Repo, get_answered_anamnesis_query(anamnesis_id), Tenant.get_name(user_id, Repo))
  end

  def get_prescriptions_history(patient_id, user_id) do
    Apartmentex.all(Repo, get_prescription_history_query(patient_id), Tenant.get_name(user_id, Repo))
    |> Enum.map(fn prescription -> update_in(prescription.inserted_at, &shift_time_zone_of(&1)) end)
  end

  defp get_history_query(patient_id) do
    from appointment in Appointment, 
    inner_join: patient in Patient,
    on: appointment.patient_id == patient.id,
    inner_join: appointment_procedures in AppointmentProcedure,
    on: appointment.id == appointment_procedures.appointment_id,
    inner_join: procedures in Procedure,
    on: appointment_procedures.procedure_id == procedures.id,
    inner_join: incomes in Income,
    on: appointment_procedures.id == incomes.appointment_procedure_id,
    where: appointment.patient_id == ^patient_id,
    order_by: [desc: appointment_procedures.attendance_date_at, desc: appointment_procedures.attendance_hour],
    select: %{
              appointment_id: appointment.id,
              patient_name: patient.name,
              procedure_name: procedures.name,
              income_id: incomes.id,
              description: incomes.description,
              price: incomes.value,
              due_date_at: incomes.due_date_at,
              done: appointment_procedures.done,
              paid_at: incomes.paid_at,
              attendance_date_at: appointment_procedures.attendance_date_at,
              attendance_hour: appointment_procedures.attendance_hour
            }
  end

  defp get_anamnesis_history_query(patient_id) do
    from a in Anamnesis,
    where: fragment("?->>? = ?", a.responses, "patient_id", ^patient_id),
    order_by: [desc: a.inserted_at],
    select: %Anamnesis{
      id: a.id,
      responses: a.responses,
      inserted_at: a.inserted_at
    }
  end

  defp get_sick_notes_history_query(patient_id) do
    from sick_note in SickNote,
    where: sick_note.patient_id == ^patient_id,
    order_by: [desc: sick_note.inserted_at]
  end

  defp get_prescription_history_query(patient_id) do
    from prescription in Prescription,
    where: prescription.patient_id == ^patient_id,
    order_by: [desc: prescription.inserted_at]
  end

  defp shift_time_zone_of(date), do: Calendar.DateTime.shift_zone!(date, "Brazil/East")

  defp get_answered_anamnesis_query(anamnesis_id) do
    from a in Anamnesis,
    where: a.id == ^anamnesis_id
  end

end 