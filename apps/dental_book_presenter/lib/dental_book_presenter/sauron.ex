defmodule DentalBookPresenter.Sauron do
  alias DentalBookPresenter.Repo
  alias DentalBookPresenter.User
  alias DentalBookPresenter.CompanyUser

  def reborn({:ok, tenant}) do
    sauron_body = mount_sauron_body_for(tenant)
    company_id = get_company_id_of(tenant)

    Repo.transaction(fn ->
        with {:ok, sauron} <- bring_to_life(sauron_body),
             {:ok, _company_relation_user} <- add_to_the_world(sauron, company_id)        
        do {:ok, sauron}
        else {:error, value} ->  Repo.rollback(value) end
    end)   
  end
  def reborn(status), do: status

  defp get_company_id_of(tenant), do: number_of(tenant) 

  defp mount_sauron_body_for(tenant) do
    %{
      "name" => "Sauron_#{number_of(tenant)}",
      "email" => "#{tenant}@dentalbook.com.br",
      "admin" => "true",
      "role" => "Profissional de Saúde",
      "password" => "00lh0qu3tud0v3",
      "password_confirmation" => "00lh0qu3tud0v3"
    }  
  end

  defp bring_to_life(sauron_body) do
    %User{}
    |> User.changeset(sauron_body)
    |> Repo.insert
  end

  defp add_to_the_world(sauron, with_this_id) do
    %CompanyUser{}
    |> CompanyUser.changeset(home_of(sauron, with_this_id))
    |> Repo.insert
  end

  defp home_of(sauron, company_id) do
    %{company_id: company_id, user_id: sauron.id}
  end

  defp number_of(tenant) do
    {tenant_id, _} = String.replace(tenant, "tenant_company_", "")
    |> Integer.parse

    tenant_id
  end

end