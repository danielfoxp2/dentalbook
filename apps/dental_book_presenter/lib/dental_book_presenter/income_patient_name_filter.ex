defmodule DentalBookPresenter.IncomePatientNameFilter do
  import Ecto.Query

  def build(:not_selected), do: true
  def build(patient_searched_name) do
    searched_name = "%#{patient_searched_name}%"
    dynamic([appointment, ..., patient], ilike(patient.name, ^searched_name))
  end
end