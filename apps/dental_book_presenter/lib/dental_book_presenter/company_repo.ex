defmodule DentalBookPresenter.CompanyRepo do
  import Ecto.Query
  alias DentalBookPresenter.{CompanyUser, Company, MonthlyCompanyPayment}

  def get_id(user_id, repo) do
    company_user = repo.get_by(CompanyUser, user_id: user_id)
    company_user.company_id
  end

  def get_company_name_by(user_id, repo) do
    get_company_name_of(user_id)  
    |> repo.one
  end

  def get_actual_payment(user_id, repo) do
    user_id
    |> payment_query
    |> repo.one
  end

  def delete_company(company_id, repo) do
    Company
    |> repo.get!(company_id)
    |> repo.delete
  end

  def update_company_with(company_id, {_, company_data, _, _}, repo) do
    Company
    |> repo.get!(company_id)
    |> Company.changeset(company_data)
    |> repo.update
  end

  def get_query_to_delete_all_users_of(company_id) do
    from company_user in CompanyUser,
    where: company_user.company_id == ^company_id
  end

  defp get_company_name_of(user_id) do
    query = from company in Company,
    inner_join: company_user in CompanyUser,
    on: company_user.company_id == company.id 
    and company_user.user_id == ^user_id,
    select: company.name
  end

  defp payment_query(user_id) do
    today = NaiveDateTime.utc_now()

    query = from company_user in CompanyUser,
    inner_join: company_payments in MonthlyCompanyPayment,
    on: company_user.company_id == company_payments.company_id,
    where: company_payments.start_access <= ^today
           and company_payments.end_access >= ^today
           and company_user.user_id == ^user_id,
    select: company_payments
  end
end