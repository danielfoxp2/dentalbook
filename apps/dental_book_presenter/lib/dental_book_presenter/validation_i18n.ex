defmodule DentalBookPresenter.ValidationI18n do
  
  def translate() do
    "setCustomValidity('Campo obrigatório')"
  end

  def onchanged() do
    "try{setCustomValidity('')}catch(e){}"
  end
end