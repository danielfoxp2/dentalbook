defmodule DentalBookPresenter.FinancialRepo do
  import Ecto.Query
  alias DentalBookPresenter.Income
  alias DentalBookPresenter.Repo
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.Expense
  alias DentalBookPresenter.DateFormat

  def get_billing_of_today(user_id) do
    of_today_interval = get_today_interval()
    get_value_of(of_today_interval, user_id)
  end
  
  def get_billing_of_week(user_id) do 
    of_week_interval = get_week_interval()
    get_value_of(of_week_interval, user_id)
  end
  
  def get_billing_of_month(user_id) do 
    of_month_interval = get_month_interval()
    get_value_of(of_month_interval, user_id)
  end
  
  def get_billing_of_period(user_id, of_period), do: get_value_of(of_period, user_id)

  defp get_value_of(date_interval, user_id) do
    Income 
    |> get_period_billed(date_interval)
    |> get_cash_flow(get_period_billed(Expense, date_interval), user_id)
  end

  defp get_cash_flow(income_query, expense_query, user_id) do
    income = get_financial_value_from_database(income_query, user_id)
    expense = get_financial_value_from_database(expense_query, user_id)

    profit = income - expense

    income_formatted = get_money_string_of(income)
    expense_formatted = get_money_string_of(expense)
    profit_formatted = get_money_string_of(profit)

    {income_formatted, expense_formatted, profit_formatted}
  end

  defp get_financial_value_from_database(query, user_id) do
    case Apartmentex.one(Repo, query, Tenant.get_name(user_id, Repo)) do
      nil -> 0
      value -> value
    end
  end

  defp get_money_string_of(value) do
    value
    |> Money.new
    |> Money.to_string
  end

  defp get_period_billed(model, in_period) do
    {initial_date, final_date} = in_period
    get_bills_query_of(model, initial_date, final_date)
  end

  defp get_bills_query_of(model, initial_date, final_date) do
    from m in model,
    where: m.paid_at >= ^initial_date
    and m.paid_at <= ^final_date,
    select: sum(m.value)
  end

  defp get_today_interval(), do: {DateFormat.now, DateFormat.now}

  defp get_week_interval() do
    today = DateFormat.now
    day_of_week = get_which_day_of_week_is(today)
    monday = get_monday_date_of_week_from(today, day_of_week)
    {monday, today}
  end

  defp get_which_day_of_week_is(today) do
    {_, today_date} = Date.new(today.year, today.month, today.day)
    
    today_date 
    |> Date.day_of_week
  end

  defp get_monday_date_of_week_from(today, day_of_week) do
    days_to_ignore = 1
    distance_from_monday_of_week_ignoring_today = day_of_week - days_to_ignore
    today_in_number_of_days = :calendar.date_to_gregorian_days(today.year, today.month, today.day)

    today_in_number_of_days - distance_from_monday_of_week_ignoring_today
    |> :calendar.gregorian_days_to_date
  end

  defp get_month_interval() do
    today = DateFormat.now
    {_, first_day} = Ecto.Date.cast({today.year, today.month, 1})

    {first_day, today}
  end

end