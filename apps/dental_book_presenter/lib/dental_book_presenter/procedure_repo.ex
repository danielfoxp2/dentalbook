defmodule DentalBookPresenter.ProcedureRepo do

  import Ecto.Query
  alias DentalBookPresenter.{AppointmentUI, AppointmentProcedure, Repo, Appointment, ProcedureLoad, Procedure}
  
  def insert_procedures(appointment_created, tenant_name, appointment_params) do
    procedures = AppointmentUI.get_procedures_params_from(appointment_params["procedures_incomes"])
                                                                                                                                                         
    appointment_procedures_changesets = 
    get_appointment_procedures_relation(appointment_created, procedures)
    |> Enum.map(&create_changeset_from/1)
    
    insert_appointment_procedure_relations(appointment_procedures_changesets, tenant_name)
  end

  def search_appointment_procedure(appointment_id) do
    from ap in get_appointment_procedure_query(appointment_id),
    select: 
    %{
      procedure_id: ap.procedure_id, 
      procedure_status: ap.done, 
      attendance_date_at: ap.attendance_date_at, 
      attendance_hour: ap.attendance_hour,
      procedure_tooth: ap.procedure_tooth,
      procedure_tooth_faces: ap.procedure_tooth_faces
    }
  end

  def delete_procedures_of(%Appointment{} = appointment, tenant_name) do
    {result, _} = Apartmentex.delete_all(Repo, get_appointment_procedure_query(appointment.id), tenant_name)
    if result > 0 do
      {:ok, result}
    else
      {:error, result}
    end
  end

  def delete_procedures_of(appointment_id, tenant_name) do
    {result, _} = Apartmentex.delete_all(Repo, get_appointment_procedure_query(appointment_id), tenant_name)
    if result > 0 do
      {:ok, result}
    else
      {:error, result}
    end
  end

  def insert_standard_procedures_of_this(tenant) do
    Repo.insert_all(Procedure, ProcedureLoad.entries(), prefix: tenant)
    |> get_status_of_insertion
  end

  defp get_appointment_procedures_relation(appointment_created, procedures) do
    Enum.map procedures, fn(procedure) -> 
      %{ 
         appointment_id: appointment_created.id, 
         procedure_id: procedure.id, 
         attendance_date_at: procedure.attendance_date_at,
         attendance_hour: procedure.attendance_hour,
         done: procedure.is_done,
         procedure_tooth: procedure.procedure_tooth,
         procedure_tooth_faces: procedure.procedure_tooth_faces
        } 
    end
  end

  defp get_status_of_insertion({entries_affected, _returned_result}) when entries_affected > 0, do: {:ok, :everything_is_fine}
  defp get_status_of_insertion({entries_affected, returned_result}), do: {:error, returned_result}

  defp create_changeset_from(appointment_procedure_relation) do
    AppointmentProcedure.changeset(%AppointmentProcedure{}, appointment_procedure_relation)
  end

  defp insert_appointment_procedure_relations(appointment_procedures_changesets, tenant_name) do
    procedures_details = Enum.map appointment_procedures_changesets, fn appointment_procedure_changeset -> 
      Apartmentex.insert(Repo, appointment_procedure_changeset, tenant_name)
    end

    get_result_of_insert(procedures_details)
  end

  defp get_result_of_insert(inserted_items) do
    Enum.find inserted_items, {:ok, inserted_items}, fn(inserted_item) -> 
      :error == elem(inserted_item, 0)
    end  
  end
  
  defp get_appointment_procedure_query(appointment_id) do
    from ap in AppointmentProcedure,
    where: ap.appointment_id == ^appointment_id
  end

end