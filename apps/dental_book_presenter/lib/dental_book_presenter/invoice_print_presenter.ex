defmodule DentalBookPresenter.InvoicePrintPresenter do
  use DentalBookPresenter.Web, :view
  alias DentalBookPresenter.DateFormat
  alias DentalBookPresenter.MoneyFormat
  alias DentalBookPresenter.IncomeBudget

  def mount(invoice_data) do
    Enum.map(invoice_data, fn element ->
      content_tag :tr do
      [
        content_tag(:td, get_procedure_name_from(element)),
        content_tag(:td, get_tooth_from(element)),
        content_tag(:td, get_tooth_face_from(element)),
        content_tag(:td, get_attendance_date_from(element)),
        content_tag(:td, get_procedure_price_from(element))
      ]
      end
      |> safe_to_string
    end)
    |> Enum.join
  end

  def today() do
    DateFormat.now |> DateFormat.ecto_date_to_string
  end

  def get_invoice_value(invoice_data) do
    Enum.map(invoice_data, fn element ->
      %{price: get_procedure_price_from(element) |> MoneyFormat.parse_value_to_money}
    end)
    |> IncomeBudget.get_total_of
  end

  defp get_procedure_name_from(invoice_data) do
    invoice_data["appointment_ui[procedures_incomes][procedure_name]"]
  end

  defp get_tooth_from(invoice_data) do
    invoice_data["appointment_ui[procedures_incomes][procedure_tooth]"]
    |> Enum.join(", ")
  end

  defp get_tooth_face_from(invoice_data) do
    invoice_data["appointment_ui[procedures_incomes][procedure_tooth_faces]"]
  end

  defp get_attendance_date_from(invoice_data) do
    invoice_data["appointment_ui[procedures_incomes][attendance_date_at]"]
  end

  defp get_procedure_price_from(invoice_data) do
    invoice_data["appointment_ui[procedures_incomes][price]"]
  end
  
end