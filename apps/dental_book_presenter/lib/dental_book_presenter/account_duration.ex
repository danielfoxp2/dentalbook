defmodule DentalBookPresenter.AccountDuration do
  alias DentalBookPresenter.DateFormat
  alias DentalBookPresenter.AccountPlan
  alias DentalBookPresenter.Repo
  
  def get_end_access(of_this_plan_id, to_this_date) do
    of_this_plan_id
    |> get_plan_duration_in_days
    |> DateFormat.add(to_this_date)
  end
  
  defp get_plan_duration_in_days(of_this_plan_id) do
    plan = Repo.get(AccountPlan, of_this_plan_id)
    plan.duration
  end
end