defmodule DentalBookPresenter.IncomeSituationFilter do
  import Ecto.Query
  alias DentalBookPresenter.DateFormat

  def build({:not_selected, :not_selected, :not_selected}) do
    get_default_status_filter()
  end
  
  def build({paid, not_paid, overdue}) do
    paid_filter = get_status_filter(paid)
    not_paid_filter = get_status_filter(not_paid)
    overdue_filter = get_status_filter(overdue)

    dynamic(^paid_filter or ^not_paid_filter or ^overdue_filter)
  end
  
  defp get_default_status_filter() do
    procedure_done_filter = dynamic([income, appointment_procedure], appointment_procedure.done == true)
    detached_income = dynamic([income], is_nil(income.appointment_procedure_id))
    paid_procedure = dynamic([income], is_nil(income.paid_at) == false)

    dynamic(^procedure_done_filter or ^detached_income or ^paid_procedure)
  end

  defp get_status_filter(:not_selected), do: false
  defp get_status_filter(:paid), do: get_paid_filter()
  defp get_status_filter(:not_paid), do: get_not_paid_filter()
  defp get_status_filter(:overdue), do: get_overdue_filter()
  
  defp get_paid_filter() do
    dynamic([income], is_nil(income.paid_at) == false)
  end

  defp get_not_paid_filter() do
    today = DateFormat.now()

    is_procedure_done = dynamic([income, appointment_procedure], appointment_procedure.done == true)
    is_not_paid_income = dynamic([income], is_nil(income.paid_at))
    is_not_overdue = dynamic([income], income.due_date_at >= ^today)
    is_detached_income = dynamic([income], is_nil(income.appointment_procedure_id))

    dynamic((^is_procedure_done or ^is_detached_income) and ^is_not_paid_income and ^is_not_overdue)
  end

  defp get_overdue_filter() do
    today = DateFormat.now()
    
    is_procedure_done = dynamic([income, appointment_procedure], appointment_procedure.done == true)
    is_not_paid_income = dynamic([income], is_nil(income.paid_at))
    is_overdue = dynamic([income], income.due_date_at < ^today)
    is_detached_income = dynamic([income], is_nil(income.appointment_procedure_id))

    dynamic((^is_procedure_done or ^is_detached_income) and ^is_not_paid_income and ^is_overdue)
  end

end