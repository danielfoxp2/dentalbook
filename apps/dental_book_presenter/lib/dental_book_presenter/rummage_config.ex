defmodule DentalBookPresenter.RummageConfig do
  import Ecto.Query

  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.Repo
  alias DentalBookPresenter.Patient

  @previous_label "Anterior"
  @next_label "Próximo"

  def get_rummage_query_with_tenant_name(model, user_id) do
    queryable = from m in model
   
    queryable
    |> Ecto.Queryable.to_query
    |> Map.put(:prefix, Tenant.get_full_name(user_id, Repo))
  end

  def get_default_params_from(%{"rummage" => %{"paginate" => paginate}} = params, results_per_page) when paginate == %{} do
    %{
      "search" => %{}, 
      "sort"=> [],
      "paginate" => 
        %{
          "per_page" => results_per_page, 
          "page" => "1", 
          "previous_label" => @previous_label,
          "next_label" => @next_label 
        }
    }
  end
  
  def get_search_term(%{"search" => %{"name" => name}} = _params), do: name["search_term"]
  def get_search_term(params), do: ""

  def get_default_params_from(params, _results_per_page) do
    original_paginate_params = params["rummage"]["paginate"]
    navigation_paginate_params = %{"previous_label" => @previous_label,"next_label" => @next_label }
    merged_params = Map.merge(original_paginate_params, navigation_paginate_params)
    params = put_in(params["rummage"]["paginate"], merged_params) 
    params["rummage"]
  end


end