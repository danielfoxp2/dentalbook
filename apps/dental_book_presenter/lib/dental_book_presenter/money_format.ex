defmodule DentalBookPresenter.MoneyFormat do

  def pre_cast_value_of(%{"price" => price} = params) when price != nil do
    update_in(params["price"], &format(&1))   
  end  

  def pre_cast_value_of(%{"value" => value} = params) when value != nil do
    update_in(params["value"], &format(&1))   
  end  

  def pre_cast_value_of(params), do: params

  def parse_value_to_money(value, is_negative \\ false) do
    value 
    |> parse_money 
    |> make_it_negative_if(is_negative)
  end

  defp parse_money(value) do
    {_, parsed_money} = Money.parse(value)
    parsed_money
  end

  defp contains_separator?(value), do: contains_comma?(value) || contains_dot?(value)
  defp contains_comma?(value), do: String.contains?(value, ",")
  defp contains_dot?(value), do: String.contains?(value, ".")

  defp format(value) do
    String.replace(value, ".", "")
    |> String.replace(",", ".")
  end

  defp make_it_negative_if(value, is_negative) when is_negative == true, do: value |> Money.neg
  defp make_it_negative_if(value, is_negative) when is_negative == false, do: value 

end