defmodule DentalBookPresenter.GroupedIncomeFilter do
  import Ecto.Query

  def build(:no_filter), do: dynamic(^get_only_paid_income_filter())
  def build({start_date, end_date, procedures}) do
    only_paid_income_filter = get_only_paid_income_filter()
    start_date_filter = get_start_date_filter(start_date)
    end_date_filter = get_end_date_filter(end_date)
    procedures_filter = get_procedures_filter(procedures)

    dynamic(^only_paid_income_filter and ^start_date_filter and ^end_date_filter and ^procedures_filter)
  end

  defp get_only_paid_income_filter() do
    dynamic([income], is_nil(income.paid_at) == false)
  end

  defp get_start_date_filter(start_date) when is_nil(start_date), do: true
  defp get_start_date_filter(start_date) when start_date == "", do: true
  defp get_start_date_filter(start_date) do
    dynamic([income], income.paid_at >= ^start_date)
  end

  defp get_end_date_filter(end_date) when is_nil(end_date), do: true
  defp get_end_date_filter(end_date) when end_date == "", do: true
  defp get_end_date_filter(end_date) do
    dynamic([income], income.paid_at <= ^end_date)
  end

  defp get_procedures_filter(procedures) when is_nil(procedures), do: true
  defp get_procedures_filter(procedures) do
    dynamic([income, appointment_procedure, ...], appointment_procedure.procedure_id in ^procedures)
  end

end