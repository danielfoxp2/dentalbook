defmodule DentalBookPresenter.Orphans do
  require Logger
  alias DentalBookPresenter.ClientAccountService

  def delete_if_needed(of_this_user) do
    of_this_user
    |> ClientAccountService.payment_status
    |> delete_account(of_this_user.email)
  end

  defp delete_account(:not_paid, of_this_user_email) do
    Logger.info "Exclusão de conta órfã"
    Logger.info "usuário com conta órfã: #{inspect(of_this_user_email)}"

    ClientAccountService.delete_account_data(of_this_user_email)
  end
  defp delete_account(_, _), do: :client_has_a_valid_account

end