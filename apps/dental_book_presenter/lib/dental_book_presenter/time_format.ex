defmodule DentalBookPresenter.TimeFormat do

  @this __MODULE__
 
  def pre_cast_value_of(params) do
    update_in(params[:attendance_hour], &format(&1))
  end

  def now() do
    DateTime.utc_now 
    |> Calendar.DateTime.shift_zone!("Brazil/East")
    |> @this.to_string
  end

  def to_string(time) do
    {hour, minute} = get_formated(time)
    "#{hour}:#{minute}"
  end

  defp format(time) when is_nil(time), do: time
  defp format(time) when time == "", do: time
  defp format(time) do
    [hour, minute] = String.split(time, ":")
    %{hour: hour, minute: minute}
  end

  defp get_formated(%DateTime{} = time) do
    %{hour: time.hour, min: time.minute}
    |> get_formated
  end

  defp get_formated(time) do
    hour = time.hour |> convert
    minute = time.min |> convert
    {hour, minute}
  end

  defp convert(element) do
    element
    |> Integer.to_string |> String.pad_leading(2, "0")
  end
end