defmodule DentalBookPresenter.IncomeSearchFilters do
  import Ecto.Query
  alias DentalBookPresenter.IncomePatientNameFilter
  alias DentalBookPresenter.IncomeDescriptionFilter
  alias DentalBookPresenter.IncomeDateIntervalFilter
  alias DentalBookPresenter.IncomeSituationFilter

  def build(params) do
    patient_name_filter = params |> get_searched_patient_name |> IncomePatientNameFilter.build
    description_filter = params |> get_searched_description |> IncomeDescriptionFilter.build
    date_interval_filter = params |> get_date_interval |> IncomeDateIntervalFilter.build
    income_situation_filter = params |> get_status_options |> IncomeSituationFilter.build

    dynamic(^patient_name_filter and ^description_filter and ^date_interval_filter and ^income_situation_filter)
  end

  defp get_searched_description(params) do
    get(params["income_description"])
  end

  defp get(search_option) when is_nil(search_option), do: :not_selected
  defp get(search_option) when search_option == "", do: :not_selected
  defp get(search_option), do: search_option 

  defp get_status_options(params) do
  {  
     get_status_options(:paid, params["paid"]),
     get_status_options(:not_paid, params["not_paid"]),
     get_status_options(:overdue, params["overdue"])
  }
  end
  
  defp get_status_options(_, search_option) when is_nil(search_option), do: :not_selected
  defp get_status_options(status_option, _), do: status_option 
  
  defp get_date_interval(params) do
    {
      get(params["start_date"]),
      get(params["end_date"])
    }
  end  
  
  defp get_searched_patient_name(params) do
    get(params["patient_name"])
  end
end