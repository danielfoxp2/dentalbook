defmodule DentalBookPresenter.IncomeCreationDate do
  alias DentalBookPresenter.DateFormat 

  def create(attendance_date_string, paid_at_string) do
    attendance_date = attendance_date_string |> DateFormat.string_to_ecto_date 
    paid_at = paid_at_string |> DateFormat.string_to_ecto_date 
    
    get_lower_date_of(attendance_date, paid_at)
    |> get_lower_date_of(DateFormat.now())
  end

  defp get_lower_date_of(first_date, second_date) when first_date  |> is_nil, do: first_date
  defp get_lower_date_of(first_date, second_date) when first_date == "", do: first_date
  defp get_lower_date_of(first_date, second_date) when second_date |> is_nil, do: first_date
  defp get_lower_date_of(first_date, second_date) when second_date == "", do: first_date
  defp get_lower_date_of(first_date, second_date) do
     Ecto.Date.compare(first_date, second_date)
     |> get_lower_date(first_date, second_date)
  end

  defp get_lower_date(:eq, first_date, second_date), do: first_date 
  defp get_lower_date(:lt, first_date, second_date), do: first_date 
  defp get_lower_date(:gt, first_date, second_date), do: second_date 

end