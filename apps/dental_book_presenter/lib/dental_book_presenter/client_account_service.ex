defmodule DentalBookPresenter.ClientAccountService do

   alias DentalBookPresenter.User
   alias DentalBookPresenter.Company
   alias DentalBookPresenter.CompanyUser
   alias DentalBookPresenter.Repo
   alias DentalBookPresenter.Tenant
   alias DentalBookPresenter.CompanyRepo
   alias DentalBookPresenter.ProcedureRepo
   alias DentalBookPresenter.MonthlyCompanyPayment
   alias DentalBookPresenter.DateFormat
   alias DentalBookPresenter.Sauron
   alias DentalBookPresenter.AccountPlan
   alias DentalBookPresenter.AccountDuration

  def create_new_client_account(registration_params, user) do
    status = new_client_transaction(registration_params, user)
    load_standards_procedures_of_this(status)
    Sauron.reborn(status)
    delete_user_if_problems_occurred(user, status)
  end

  def insert_new_collaborator(collaborator_changeset, logged_user_id) do
    Repo.transaction(fn ->
      with {:ok, new_collaborator} <- insert_new_collaborator(collaborator_changeset),
           {:ok, company_id} <- get_company_id(logged_user_id),
           {:ok, _} <- insert_relation_for_company_and_user(company_id, new_collaborator.id)
      do :everything_is_fine 
      else {:error, value} -> Repo.rollback(value) end
    end)
  end

  def delete_collaborator_completly(collaborator_id) do
    Repo.transaction(fn ->
      with {:ok, company_id} <- get_company_id(collaborator_id),
           {:ok, company} <- delete_relation_for_company_and_user(company_id, collaborator_id),
           {:ok, _deleted_collaborator} <- delete_collaborator(collaborator_id)
      do :everything_is_fine 
      else {:error, value} -> Repo.rollback(value) end
    end)
  end

  def payment_status(user) do
    CompanyRepo.get_actual_payment(user.id, Repo)
    |> translate_payment_status
  end

  def get_user(user_email) do
    Repo.get_by(User, email: user_email)
  end

  def delete_account_data(user_email) do
    Repo.transaction(fn ->
      with {:ok, user} <- get_user_valid(user_email),
           {:ok, _} <- delete_tenant(user.id),
           {:ok, company_id} <- get_company_id(user.id),
           {:ok, _company_user} <- delete_relation_for_company_and_user(company_id),
           {:ok, _deleted_collaborator} <- delete_collaborator(user.id),
           {:ok, _super_user} <- delete_super_user(company_id),
           {:ok, company_id} <- CompanyRepo.delete_company(company_id, Repo)
      do :everything_is_fine 
      else {:error, value} -> Repo.rollback(value) end
    end)
  end

  def finish_account_creation({user_email, _, _, _} = complementary_client_data) do
    complete_account_creation(user_email, complementary_client_data)
    |> delete_all_data_of_account_if_theres_no_good(user_email)
  end

  defp get_user_valid(user_email) do
    user_email
    |> get_user
    |> validate_existance(user_email)
  end

  defp validate_existance(user, user_email) when is_nil(user), do: {:error, user_email}
  defp validate_existance(user, _user_email), do: {:ok, user}

  defp complete_account_creation(user_email, complementary_client_data) do
    {_, cpf_cnpj, recurrency_signature, client_remote_ip} = complementary_client_data
    start_access = Calendar.DateTime.now! "America/Sao_Paulo"

    Repo.transaction(fn ->
      with {:ok, user} <- get_user_valid(user_email),
           {:ok, company_id} <- get_company_id(user.id),
           {:ok, _} <- CompanyRepo.update_company_with(company_id, complementary_client_data, Repo),
           {:ok, _} <- insert_payment_of_month(start_access, company_id, recurrency_signature)
      do :everything_is_fine 
      else {:error, value} -> Repo.rollback(value) end
    end)
  end

  defp delete_all_data_of_account_if_theres_no_good({:ok, _} = status, _user_email), do: status
  defp delete_all_data_of_account_if_theres_no_good(_status, user_email), do: delete_account_data(user_email)

  defp translate_payment_status(company) when is_nil(company), do: :not_paid
  defp translate_payment_status(company), do: :paid

  defp insert_new_collaborator(collaborator_changeset) do
    Repo.insert(collaborator_changeset)
  end

  defp delete_collaborator(collaborator_id) do
    Repo.get(User, collaborator_id)
    |> Repo.delete
  end

  defp delete_super_user(company_id) do
    Repo.get_by(User, name: "Sauron_#{company_id}")
    |> Repo.delete
  end

  defp get_company_id(logged_user_id) do
    company_id = logged_user_id |> CompanyRepo.get_id(Repo)
    {:ok, company_id}
  end
  
  defp new_client_transaction(registration_params, user) do
    Repo.transaction(fn ->
      with {:ok, company} <- insert_company(registration_params),
           {:ok, company_relation_user} <- insert_relation_for_company_and_user(company.id, user.id),
           {:ok, tenant_name} <- create_new_tenant(user.id)           
      do tenant_name
      else {:error, value} ->  Repo.rollback(value) end
    end)   
  end

  defp insert_company(registration_params) do
    company_name = registration_params["empresa"]
    account_plan_id = get_account_plan_id_from(registration_params)

    company = %{name: company_name, account_plan_id: account_plan_id}
    company = Company.changeset(%Company{}, company) 
    |> Repo.insert
  end

  defp get_account_plan_id_from(registration_params) do
    registration_params["plan"]
  end

  defp delete_user_if_problems_occurred(user, {:ok, _} = status), do: status
  defp delete_user_if_problems_occurred(user, status) do
    Repo.delete!(user)
    status
  end

  defp insert_relation_for_company_and_user(company_id, user_id) do
    company_user = %{company_id: company_id, user_id: user_id}

    CompanyUser.changeset(%CompanyUser{}, company_user) 
    |> Repo.insert
  end

  defp insert_payment_of_month(start_access, company_id, recurrency_signature) do
    plan_id = recurrency_signature.plan_id

    monthly_payment = %{
      company_id: company_id,
      account_plan_id: plan_id,
      start_access: start_access,
      end_access: AccountDuration.get_end_access(plan_id, start_access),
      paid_value: recurrency_signature.value,
      pay_day: start_access
    }

    MonthlyCompanyPayment.changeset(%MonthlyCompanyPayment{}, monthly_payment)
    |> Repo.insert
  end

  defp delete_relation_for_company_and_user(company_id, user_id) do
    %CompanyUser{company_id: company_id, user_id: user_id}
    |> Repo.delete
  end

  defp delete_relation_for_company_and_user(company_id) do
    CompanyRepo.get_query_to_delete_all_users_of(company_id)
    |> Repo.delete_all
    |> check_result
  end

  defp check_result({deleted_lines, result}) when deleted_lines > 0, do: {:ok, result}
  defp check_result({deleted_lines, result}), do: {:error, result}

  defp create_new_tenant(user_id) do
    {status, tenant_name, _} = Apartmentex.new_tenant(Repo, Tenant.get_name(user_id, Repo))
    {status, tenant_name}
  end

  defp load_standards_procedures_of_this({:ok, tenant}), do: ProcedureRepo.insert_standard_procedures_of_this(tenant)
  defp load_standards_procedures_of_this(status), do: status

  defp delete_tenant(user_id) do
    {status, result} = Apartmentex.drop_tenant(Repo, Tenant.get_name(user_id, Repo))
    {status, result}
  end
end