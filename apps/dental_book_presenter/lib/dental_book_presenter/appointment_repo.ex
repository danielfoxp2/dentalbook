defmodule DentalBookPresenter.AppointmentRepo do
  import Ecto.Query
  alias DentalBookPresenter.{Repo, Appointment, Patient, AppointmentProcedure, Procedure, Income}

  def insert_appointment(tenant_name, appointment_params) do
    appointment = 
    build_appointment_patient_association(appointment_params)
    |> Appointment.changeset(appointment_params) 

    Apartmentex.insert(Repo, appointment, tenant_name)
  end

  def update_appointment(tenant_name, appointment_id, appointment_params) do
    appointment = 
    build_appointment_patient_association(appointment_params)
    |> set_appointment_id_in(appointment_id)
    |> Appointment.changeset(appointment_params) 

    Apartmentex.update(Repo, appointment, tenant_name)
  end

  def delete_appointment(appointment_id, tenant_name) do
    {result, _} = Apartmentex.delete_all(Repo, get_appointment_query(appointment_id), tenant_name)
    if result > 0 do
      {:ok, result}
    else
      {:error, result}
    end
  end

  def search_appointment(appointment_id) do
    from appointment in Appointment, 
    inner_join: patient in Patient,
    on: appointment.patient_id == patient.id,   
    inner_join: appointment_procedures in AppointmentProcedure,
    on: appointment.id == appointment_procedures.appointment_id,
    inner_join: procedures in Procedure,
    on: appointment_procedures.procedure_id == procedures.id,
    inner_join: incomes in Income,
    on: appointment_procedures.id == incomes.appointment_procedure_id,
    where: appointment.id == ^appointment_id,
    order_by: [appointment_procedures.attendance_date_at, appointment_procedures.attendance_hour],
    select: %{
      id: appointment.id,
      patient_id: patient.id,
      dentist_id: appointment.dentist_id,
      patient_name: patient.name,
      patient_cellphone: patient.cellphone,
      procedure_id: procedures.id,
      procedure_name: procedures.name,
      income_id: incomes.id,
      description: incomes.description,
      price: incomes.value,
      due_date_at: incomes.due_date_at,
      done: appointment_procedures.done,
      paid_at: incomes.paid_at,
      attendance_date_at: appointment_procedures.attendance_date_at,
      attendance_hour: appointment_procedures.attendance_hour,
      procedure_tooth: appointment_procedures.procedure_tooth,
      procedure_tooth_faces: appointment_procedures.procedure_tooth_faces
    }
  end

  def search_appointment_by(appointments_procedures_ids, tenant_name) do
    query = from a in Appointment,
    inner_join: p in Patient,
    on: a.patient_id == p.id,
    inner_join: ap in AppointmentProcedure,
    on: ap.appointment_id == a.id,
    where: ap.id in ^appointments_procedures_ids,
    select: %{ appointment_procedure_id: ap.id, patient_name: p.name }

    Apartmentex.all(Repo, query, tenant_name)
  end

  def search_appointment_only_with_patient(query_paginated) do
    from a in query_paginated,
    preload: [:patient]
  end

  def get_presentable_appointments(all_appointments, search_term) do
    from appointment in all_appointments, 
    inner_join: p in Patient, 
    on: appointment.patient_id == p.id,
    where: ilike(p.name, ^"%#{search_term}%"),
    select: appointment.id
  end

  def get_appointment_query(appointments_id, all_appointments) do
    from ap in all_appointments,
    where: ap.id in ^appointments_id
  end

  defp build_appointment_patient_association(appointment_params) do
    {patient_id_converted, _ } = Integer.parse(appointment_params["patient_id"])
    patient = %Patient{ id: patient_id_converted }
    Ecto.build_assoc(patient, :appointments)
  end

  defp set_appointment_id_in(appointment, appointment_id) do
    {parsed_appointment_id, _} = Integer.parse(appointment_id)
    put_in(appointment.id, parsed_appointment_id)
  end

  defp get_appointment_query(appointment_id) do
    from ap in Appointment,
    where: ap.id == ^appointment_id
  end


end