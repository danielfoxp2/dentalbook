defmodule DentalBookPresenter.Tenant do
  alias DentalBookPresenter.CompanyRepo

  def get_name(user_id, repo) do
  	user_id
    |> CompanyRepo.get_id(repo)
    |> mount_name
  end

  def get_full_name(user_id, repo) do
    "tenant_" <> get_name(user_id, repo)
  end

  defp mount_name(company_id) do
    company_id
    |> Integer.to_string
    |> String.pad_leading(5, "0")  
    |> concat_with_company
  end

  defp concat_with_company(formated_company_id) do
    "company_" <> formated_company_id
  end
end
  
