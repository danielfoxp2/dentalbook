defmodule DentalBookPresenter.EncodeParser do
    
  def get_string_from(encoded_data) do
    list_of_characters = String.codepoints(encoded_data)  
    val = Enum.reduce(list_of_characters, &concat_binary_with_strings(&1, &2))
  end

  defp concat_binary_with_strings(character, concatenated_characteres) do
    cond do
      String.valid?(character) -> 
        concatenated_characteres <> character
      true -> 
        << parsed_character :: 8>> = character 
        concatenated_characteres <> << parsed_character :: utf8 >>
    end
  end

end 