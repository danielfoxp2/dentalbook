defmodule DentalBookPresenter.PaymentStatusPlug do
    import Plug.Conn
    alias DentalBookPresenter.ClientAccountService
    alias DentalBookPresenter.UserRepo
    alias DentalBookPresenter.Repo
    
    def init(opts) do
      opts
    end
  
    def call(conn, opts) do
      conn |> manage_user_permission_on_login
      conn
    end

    defp manage_user_permission_on_login(%Plug.Conn{request_path: request_path} = conn) when request_path == "/sessions" do
      get_possible_user_emails_in(conn.params)
      |> UserRepo.get_user
      |> check_user_payment_status
      |> update_defaulting_user
    end
    defp manage_user_permission_on_login(conn), do: :nothing_to_do_here

    defp get_possible_user_emails_in(params) do
      inputed_email = params["session"]["email"]
      unpaid_email = "unpaid_#{inputed_email}"
      {[inputed_email, unpaid_email], Repo}
    end

    defp check_user_payment_status(user) when is_nil(user), do: :nothing_to_do_here
    defp check_user_payment_status(user), do: {ClientAccountService.payment_status(user), user}

    defp update_defaulting_user(:nothing_to_do_here), do: :nothing_to_do_here
    defp update_defaulting_user({:paid, user}) do 
      user_params = %{"email" => String.replace(user.email, "unpaid_", "")}
      UserRepo.update(user, user_params, Repo)
    end
    defp update_defaulting_user({:not_paid, user}) do
      user_email = String.replace(user.email, "unpaid_", "")
      user_params = %{"email" => "unpaid_#{user_email}"}
      UserRepo.update(user, user_params, Repo)
    end

  end