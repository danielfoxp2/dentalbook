defmodule DentalBookPresenter.AppointmentService do
  alias Ecto.Multi
  alias DentalBookPresenter.{Appointment, User, AppointmentRepo, ProcedureRepo, IncomeRepo, AppointmentObservationRepo, Repo}

  def get_appointments(query, tenant_name) do
    Apartmentex.all(Repo, query, tenant_name)
  end

  def insert_appointment_procedures_incomes(tenant_name, appointment_params) do
    case execute_insert_transaction(tenant_name, appointment_params) do
      {:ok, appointment_created} -> {:ok, appointment_created}
      {:error, _, reason, _} -> {:error, reason}
    end
  end

  def update_appointment_procedures_incomes(tenant_name, appointment_id, appointment_params) do
    case execute_update_transaction(tenant_name, appointment_id, appointment_params) do
      {:ok, appointment_updated} -> {:ok, appointment_updated}
      {:error, _, reason, _} -> {:error, reason}
    end
  end

  def delete_appointment_procedures_incomes(tenant_name, appointment_id) do
    case execute_delete_transaction(tenant_name, appointment_id) do
      {:ok, appointment_updated} -> {:ok, appointment_updated}
      {:error, _, reason, _} -> {:error, reason}
    end
  end

  def get_by(appointment_id, tenant_name) do
    appointments = Apartmentex.all(Repo, AppointmentRepo.search_appointment(appointment_id), tenant_name)
    appointment_observations = Apartmentex.all(Repo, AppointmentObservationRepo.search_observations_of(appointment_id), tenant_name)
    dentist = Repo.get(User, get_dentist_id_from(appointments))

    appointments_with_dentist = get_appointments_with_dentist_name(appointments, dentist)
    {appointments_with_dentist, appointment_observations}
  end

  def get_status_of_procedures(%Appointment{} = appointment, tenant_name) do
    procedures_status = Apartmentex.all(Repo, ProcedureRepo.search_appointment_procedure(appointment.id), tenant_name)
    {appointment, procedures_status}
  end

  def get_incomes_data({appointment, procedures_status} = params, tenant_name) do
    incomes = Apartmentex.all(Repo, IncomeRepo.search_appointment_incomes(appointment.id), tenant_name)  
    {appointment, procedures_status, incomes}
  end

  def get_query_of_appointment_and_patients(query_paginated) do
    AppointmentRepo.search_appointment_only_with_patient(query_paginated)
  end

  defp get_dentist_id_from(appointments) do
    appointment = List.first(appointments)
    appointment.dentist_id
  end

  defp get_appointments_with_dentist_name(appointments, dentist) do
    Enum.map appointments, fn appointment ->
      put_in(appointment, [:dentist_name], dentist.name)
    end
  end

  defp execute_insert_transaction(tenant_name, appointment_params) do
    Multi.new
    |> Multi.run(:appointment, fn _ -> AppointmentRepo.insert_appointment(tenant_name, appointment_params) end)
    |> Multi.run(:procedures, &ProcedureRepo.insert_procedures(&1.appointment, tenant_name, appointment_params))
    |> Multi.run(:incomes, &IncomeRepo.insert_incomes(&1.procedures, tenant_name, appointment_params))
    |> Multi.run(:observations, &AppointmentObservationRepo.insert_observations(&1.appointment, tenant_name, appointment_params))
    |> Repo.transaction
  end

  defp execute_update_transaction(tenant_name, appointment_id, appointment_params) do  
    Multi.new
    |> Multi.run(:appointment, fn _ -> AppointmentRepo.update_appointment(tenant_name, appointment_id, appointment_params) end)
    |> Multi.run(:delete_incomes, &IncomeRepo.delete_incomes_of(&1.appointment, tenant_name))
    |> Multi.run(:delete_procedures, &ProcedureRepo.delete_procedures_of(&1.appointment, tenant_name))
    |> Multi.run(:delete_observations, &AppointmentObservationRepo.delete_observations_of(&1.appointment, tenant_name))
    |> Multi.run(:procedures, &ProcedureRepo.insert_procedures(&1.appointment, tenant_name, appointment_params))
    |> Multi.run(:incomes, &IncomeRepo.insert_incomes(&1.procedures, tenant_name, appointment_params))
    |> Multi.run(:observations, &AppointmentObservationRepo.insert_observations(&1.appointment, tenant_name, appointment_params))
    |> Repo.transaction
  end

  defp execute_delete_transaction(tenant_name, appointment_id) do
    Multi.new
    |> Multi.run(:search_appointment, fn _ -> get_appointment_by(appointment_id, tenant_name) end)
    |> Multi.run(:delete_incomes, &IncomeRepo.delete_incomes_of(&1.search_appointment, tenant_name))
    |> Multi.run(:delete_procedures, fn _ -> ProcedureRepo.delete_procedures_of(appointment_id, tenant_name) end)
    |> Multi.run(:delete_observations, fn _ -> AppointmentObservationRepo.delete_observations_of(appointment_id, tenant_name) end)
    |> Multi.run(:delete_appointment, fn _ -> AppointmentRepo.delete_appointment(appointment_id, tenant_name) end)
    |> Repo.transaction
  end

  defp get_appointment_by(appointment_id, tenant_name) do
    appointment = get_by(appointment_id, tenant_name)
    if (appointment != nil) do 
      {:ok, appointment}
    else 
      {:error, appointment}
    end
  end

end