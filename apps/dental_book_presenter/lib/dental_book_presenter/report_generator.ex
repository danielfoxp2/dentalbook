defmodule DentalBookPresenter.ReportGenerator do
  
#   def create_pdf(file_name, template_path, template_params) do
#     priv_dir_path = :code.priv_dir(:dental_book_presenter)
#     template_file_path = Path.join(priv_dir_path, template_path)
#     html = EEx.eval_file(template_file_path, assigns: template_params)

#     {:ok, filename} = PdfGenerator.generate html, filename: file_name
#     {:ok, pdf_content} = File.read filename 
#     {filename, pdf_content}
#   end 

  def create_pdf(from_this_params) do
    params = get_map(from_this_params)

    template_file_path = full_file_path(params.template_path)

    full_template_params = include_css_dir_if_needed_in(params.template_params, params.partial_css_template_dir)

    html = EEx.eval_file(template_file_path, assigns: full_template_params)

    {:ok, filename} = PdfGenerator.generate html, filename: params.file_name
    {:ok, pdf_content} = File.read filename 
    {filename, pdf_content}
  end 

  defp get_map(from_this_params) do
    %{
      file_name: Keyword.get(from_this_params, :file_name, "relatorio_sem_nome"),
      template_path: Keyword.get(from_this_params, :template_path),
      partial_css_template_dir: Keyword.get(from_this_params, :partial_css_template_dir, ""), 
      template_params: Keyword.get(from_this_params, :template_params, [])
    }
  end

  defp full_file_path(partial_file_path) do
    priv_dir_path = :code.priv_dir(:dental_book_presenter)
    Path.join(priv_dir_path, partial_file_path)
  end

  defp include_css_dir_if_needed_in(template_params, partial_css_dir) when partial_css_dir == "", do: template_params
  defp include_css_dir_if_needed_in(template_params, partial_css_dir) do 
    full_css_dir = full_file_path(partial_css_dir)
    template_params = Keyword.put_new(template_params, :css_dir, full_css_dir)
    template_params
  end
    
end