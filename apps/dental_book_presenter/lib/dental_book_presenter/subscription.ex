defmodule DentalBookPresenter.Subscription do

  alias DentalBookPresenter.ClientAccountService
  alias DentalBookPresenter.CompanyRepo
  alias DentalBookPresenter.Repo
  alias DentalBookPresenter.MonthlyCompanyPayment
  alias DentalBookPresenter.DateFormat
  alias DentalBookPresenter.AccountDuration
  
  def update_client_subscription_if_needed(status, user_email) do
    get_payment(status)
    |> update_client_subscription_on_new_payment(user_email)
  end

  defp get_payment(status) when status == "paid", do: :paid
  defp get_payment(_status), do: :unpaid

  defp update_client_subscription_on_new_payment(:paid, user_email) do
    user = user_email |> ClientAccountService.get_user
    
    CompanyRepo.get_actual_payment(user.id, Repo)
    |> set(:start_date)
    |> set(:end_date)
    |> set(:payment_date)
    |> renew_account_access
  end
  defp update_client_subscription_on_new_payment(_status, _user_email), do: :unpaid_account

  defp set(params, :start_date) do
    end_access = params.end_access |> Calendar.DateTime.shift_zone!("America/Sao_Paulo")
    update_in(params.start_access, fn (start_access) -> start_access = DateFormat.add_one_day_to(end_access) end)
  end
  defp set(params, :end_date) do
    update_in(params.end_access, &AccountDuration.get_end_access(params.account_plan_id, &1))
  end 
  defp set(params, _) do
    update_in(params.pay_day, fn (pay_day) -> pay_day = Calendar.DateTime.now! "America/Sao_Paulo" end)
  end

  defp renew_account_access(new_payment) do
    monthly_payment = %{
      company_id: new_payment.company_id,
      account_plan_id: new_payment.account_plan_id,
      start_access: new_payment.start_access,
      end_access: new_payment.end_access,
      paid_value: new_payment.paid_value,
      pay_day: new_payment.pay_day
    }

    MonthlyCompanyPayment.changeset(%MonthlyCompanyPayment{}, monthly_payment)
    |> Repo.insert
  end      
end