defmodule DentalBookPresenter.AnamnesisIndicators do
 
  def get_question_rates_of(anamnesis_answers) do
    only_answers = Map.delete(anamnesis_answers.responses, "patient_id") |> Map.values
    total_number_of_questions = Enum.count only_answers
    number_of_not_answered_questions = get_total_of_not_answered_questions(only_answers)
    number_of_answered_questions = get_total_of_answered_questions(total_number_of_questions, number_of_not_answered_questions)
    {total_number_of_questions, number_of_answered_questions}
  end

  defp get_total_of_not_answered_questions(only_answers) do
    Enum.reduce only_answers, 0, fn(answer, counter) ->
      is_not_answered = answer |> Map.get("response") |> question_is_not_answered
      updated_counter(is_not_answered, counter)
    end
  end

  defp get_total_of_answered_questions(total_number_of_questions, number_of_not_answered_questions) do
    total_number_of_questions - number_of_not_answered_questions
  end

  defp question_is_not_answered(answer) do
    trimmed_answer = String.trim answer
    String.equivalent?(trimmed_answer, "Não respondida.") || String.equivalent?(trimmed_answer, "")
  end

  defp updated_counter(is_not_answered, counter) when is_not_answered == true, do: counter + 1
  defp updated_counter(_is_not_answered, counter), do: counter

end