defmodule DentalBookPresenter.DateFormat do
  import Ecto

  @one_day_in_seconds 86400
  @thirty_one_days_in_seconds 2678400
    
  def pre_cast_value_of(params) do
    params_with_created_at_updated = update_in(params["created_at"], &format(&1))
    params_with_due_date_at_updated = update_in(params_with_created_at_updated["due_date_at"], &format(&1))
    params_with_all_dates_updated = update_in(params_with_due_date_at_updated["paid_at"], &format(&1))
    params_with_all_dates_updated
  end

  def pre_cast_value_of(params, field) do
    update_in(params[field], &format(&1))
  end

  def ecto_date_to_string(date) when date == nil, do: date
  def ecto_date_to_string(date) do
    date_string = Ecto.Date.to_string(date)
    [year, month, day] = String.split(date_string, "-") 
    "#{day}/#{month}/#{year}"
  end

  def string_to_ecto_date(date) when date |> is_nil, do: date
  def string_to_ecto_date(date) when date == "", do: date
  def string_to_ecto_date(date) do
    date 
    |> format
    |> Ecto.Date.cast
    |> get_date
  end

  def naive_date_to_ecto_date(naive_date) do
    {naive_date.year, naive_date.month, naive_date.day}
    |> Ecto.Date.from_erl
  end

  defp get_date({_, date}), do: date

  def now() do
    {date, time} = :calendar.universal_time() |> :calendar.universal_time_to_local_time
    date |> Ecto.Date.from_erl
  end

  def seven_days_from_now() do
    today = now()
    days_to_add_on_today = 6
    
    :calendar.date_to_gregorian_days(today.year, today.month, today.day) 
    |> Kernel.+(days_to_add_on_today)
    |> :calendar.gregorian_days_to_date 
  end

  def add(days, to_date) do
    {_, new_date} = Date.new(to_date.year, to_date.month, to_date.day)
    last_second_of_day = ~T[02:59:59.999]
    {_, new_naive_date} = NaiveDateTime.new(new_date, last_second_of_day)

    days_in_second_to_be_added = get_seconds_with_timezone_handling_of(days)
    
    new_naive_date 
    |> NaiveDateTime.add(days_in_second_to_be_added, :second)
    |> DateTime.from_naive("Etc/UTC")
    |> get_added_days_from_tuple
    |> Calendar.DateTime.shift_zone!("America/Sao_Paulo")
  end

  defp get_added_days_from_tuple({_, of_naive_date_time}), do: of_naive_date_time

  defp get_seconds_with_timezone_handling_of(this_days) do
    seconds_to_deal_with_timezone = @one_day_in_seconds
    (this_days * @one_day_in_seconds) + seconds_to_deal_with_timezone
  end

  def add_one_day_to(date) do
    new_date = add_one_day_to(date.day, date.month, date.year)

    {_, date_plus_one_day} = new_date |> DateTime.from_naive("Etc/UTC")

    date_plus_one_day
    |> Calendar.DateTime.shift_zone!("America/Sao_Paulo")
  end

  def add_one_month_to(date) do
    new_date = add_one_month_to(date.day, date.month, date.year)

    {_, date_plus_one_month} = new_date |> DateTime.from_naive("Etc/UTC")

    date_plus_one_month
    |> Calendar.DateTime.shift_zone!("America/Sao_Paulo")
  end

  defp add_one_month_to(day, month, year) do
    {_, new_date} = Date.new(year, month, day)
    last_second_of_day = ~T[02:59:59.999]
    {_, new_date} = NaiveDateTime.new(new_date, last_second_of_day)
    new_date |> NaiveDateTime.add(@thirty_one_days_in_seconds, :second)
  end

  defp add_one_day_to(day, month, year) do
    {_, new_date_time} = Date.new(year, month, day)
    last_second_of_day = ~T[03:00:00.000]
    {_, new_naive_date_time} = NaiveDateTime.new(new_date_time, last_second_of_day)
    new_naive_date_time |> NaiveDateTime.add(@one_day_in_seconds, :second)
  end

  defp format(date) when date == nil, do: date 
  defp format(date) when date == "", do: date
  defp format(%Ecto.Date{} = date), do: date
  defp format(date) do
    [day, month, year] = String.split(date, "/")
    "#{year}-#{month}-#{day}"
  end
end