defmodule DentalBookPresenter.IncomeBudget do

  def get_total_of(appointment_incomes) do
    get_list_of_values_of(appointment_incomes)
    |> sum_all_of_them
    |> Money.to_string
  end

  def get_total_as_money_of(appointment_incomes) do
    get_list_of_values_of(appointment_incomes)
    |> sum_all_of_them
  end

  defp get_list_of_values_of(appointment_incomes) do
    Enum.map(appointment_incomes, fn income -> get_amount_of(income) end)
  end

  defp sum_all_of_them(values) do
    acumulator = Money.new(0)
    Enum.reduce(values, acumulator, fn(value, partial_total) -> Money.add(value, partial_total) end)
  end

  defp get_amount_of(%{price: price}), do: price
  defp get_amount_of(%{value: price}), do: price

end