defmodule DentalBookPresenter.Repo.Migrations.CreateAnamnesis do
  use Ecto.Migration

  def change do
    create table(:anamnesis) do
      add :responses, :map
      
      timestamps(type: :timestamptz)
    end
  end
end
