defmodule DentalBookPresenter.Repo.Migrations.CreateAppointmentObservations do
  use Ecto.Migration

  def change do
    create table(:appointment_observations, primary_key: false) do
      add :appointment_id, references(:appointments, on_delete: :nothing), primary_key: true
      add :observation_id, :bigint, primary_key: true
      add :observation, :string
      add :creation_date, :string
      add :creation_hour, :string
    end
    create index(:appointment_observations, [:appointment_id])
    create index(:appointment_observations, [:observation_id])
  end
end
