defmodule DentalBookPresenter.Repo.Migrations.CreateProcedure do
  use Ecto.Migration

  def change do
    create table(:procedures) do
      add :name, :string
      add :price, :integer

      timestamps(type: :timestamptz)
    end

  end
end
