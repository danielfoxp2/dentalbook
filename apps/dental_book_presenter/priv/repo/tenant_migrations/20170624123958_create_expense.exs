defmodule DentalBookPresenter.Repo.Migrations.CreateExpense do
  use Ecto.Migration

  def change do
    create table(:expenses) do
      add :description, :string
      add :value, :integer
      add :due_date_at, :date
      add :paid_at, :date

      timestamps(type: :timestamptz)
    end

  end
end
