defmodule DentalBookPresenter.Repo.Migrations.CreateSickNoteTemplate do
  use Ecto.Migration

  def change do
    create table(:sick_note_templates) do
      add :title, :string
      add :content, :text

      timestamps(type: :timestamptz)
    end

  end
end
