defmodule DentalBookPresenter.Repo.Migrations.CreatePrescription do
  use Ecto.Migration

  def change do
    create table(:prescriptions) do
      add :patient_id, :integer
      add :patient_name, :string
      add :dentist_name, :string
      add :dentist_cro, :string
      add :prescription_content, :map

      timestamps(type: :timestamptz)
    end

  end
end
