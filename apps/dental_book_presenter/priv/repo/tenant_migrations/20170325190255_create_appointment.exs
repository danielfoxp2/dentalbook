defmodule DentalBookPresenter.Repo.Migrations.CreateAppointment do
  use Ecto.Migration

  def change do
    create table(:appointments) do
      add :patient_id, references(:patients, on_delete: :nothing)
      add :dentist_id, :integer

      timestamps(type: :timestamptz)
    end
     create index(:appointments, [:patient_id, :dentist_id])
  end
end
