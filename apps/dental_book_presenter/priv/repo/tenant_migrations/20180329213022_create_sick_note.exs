defmodule DentalBookPresenter.Repo.Migrations.CreateSickNote do
  use Ecto.Migration

  def change do
    create table(:sick_notes) do
      add :patient_id, :integer, primary_key: true
      add :patient_name, :string
      add :dentist_name, :string
      add :dentist_cro, :string
      add :sick_note_content, :string

      timestamps(type: :timestamptz)
    end

  end
end
