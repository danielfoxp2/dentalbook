defmodule DentalBookPresenter.Repo.Migrations.CreateAppointmentProcedure do
  use Ecto.Migration

  def change do
    create table(:appointment_procedures) do
      add :appointment_id, references(:appointments, on_delete: :nothing)
      add :procedure_id, references(:procedures, on_delete: :nothing)
      add :attendance_date_at, :date
      add :attendance_hour, :time
      add :done, :boolean
      add :procedure_tooth, :string
      add :procedure_tooth_faces, :string

      timestamps(type: :timestamptz)
    end
    create index(:appointment_procedures, [:appointment_id])
    create index(:appointment_procedures, [:procedure_id])

  end
end
