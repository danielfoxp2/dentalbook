defmodule DentalBookPresenter.Repo.Migrations.CreateIncome do
  use Ecto.Migration

  def change do
    create table(:incomes) do
      add :description, :string
      add :created_at, :date
      add :due_date_at, :date
      add :paid_at, :date
      add :value, :integer
      add :appointment_procedure_id, :integer

      timestamps(type: :timestamptz)
    end
    create index(:incomes, [:appointment_procedure_id])

  end
end
