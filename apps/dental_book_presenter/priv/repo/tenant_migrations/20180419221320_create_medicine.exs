defmodule DentalBookPresenter.Repo.Migrations.CreateMedicine do
  use Ecto.Migration

  def change do
    create table(:medicines) do
      add :use_type, :string
      add :description, :string
      add :default_prescription, :string
      add :controlled, :boolean, default: false, null: false

      timestamps()
    end

  end
end
