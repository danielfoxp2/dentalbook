defmodule DentalBookPresenter.Repo.Migrations.CreatePatient do
  use Ecto.Migration

  def change do
    create table(:patients) do
      add :name, :string
      add :cellphone, :string
      add :gender, :string
      add :birth_date, :date
      add :age, :integer
      add :cpf, :string
      add :origin, :string

      timestamps(type: :timestamptz)
    end

  end
end
