defmodule DentalBookPresenter.Repo.Migrations.CreateMonthlyCompanyPayment do
  use Ecto.Migration

  def change do
    create table(:monthly_company_payments, primary_key: false) do
      add :company_id, references(:companies, on_delete: :nothing), primary_key: true
      add :account_plan_id, references(:account_plans, on_delete: :nothing), primary_key: true
      add :start_access, :timestamptz, primary_key: true
      add :end_access, :timestamptz
      add :paid_value, :integer
      add :pay_day, :timestamptz

      timestamps(type: :timestamptz)
    end
  end
end
