defmodule DentalBookPresenter.Repo.Migrations.AddCellphoneToCompanyOwner do
  use Ecto.Migration

  def change do
    alter table("users") do
      add :cellphone, :string
    end
  end
end
