defmodule DentalBookPresenter.Repo.Migrations.AddDurationInDaysToAccountPlan do
  use Ecto.Migration

  def change do
    alter table("account_plans") do
      add :duration, :integer
    end
  end
end
