defmodule DentalBookPresenter.Repo.Migrations.CreatePartnerProspect do
  use Ecto.Migration

  def change do
    create table(:partners_prospects, primary_key: false) do
      add :prospect_ip, :string, primary_key: true
      add :partner_key, :string, primary_key: true

      timestamps(type: :timestamptz)
    end  
    
    create index(:partners_prospects, [:prospect_ip])
  end
end
