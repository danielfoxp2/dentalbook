defmodule DentalBookPresenter.Repo.Migrations.CreateAccountPlans do
  use Ecto.Migration

  def change do
    create table(:account_plans) do
      add :description, :string
      add :type, :string
      add :price, :integer
      add :start_day, :timestamptz
      add :end_day, :timestamptz

      timestamps(type: :timestamptz)
    end
  end
end
