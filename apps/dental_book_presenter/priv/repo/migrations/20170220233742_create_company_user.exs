defmodule DentalBookPresenter.Repo.Migrations.CreateCompanyUser do
  use Ecto.Migration

  def change do
    create table(:company_users, primary_key: false) do
      add :company_id, references(:companies, on_delete: :nothing), primary_key: true
      add :user_id, references(:users, on_delete: :nothing), primary_key: true

      timestamps(type: :timestamptz)
    end

  end
end
