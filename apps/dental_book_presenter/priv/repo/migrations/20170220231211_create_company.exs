defmodule DentalBookPresenter.Repo.Migrations.CreateCompany do
  use Ecto.Migration

  def change do
    create table(:companies) do
      add :name, :string

      timestamps(type: :timestamptz)
    end

  end
end
