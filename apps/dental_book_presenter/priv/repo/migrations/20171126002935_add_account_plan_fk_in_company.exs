defmodule DentalBookPresenter.Repo.Migrations.AddAccountPlanFkInCompany do
  use Ecto.Migration

  def change do
    alter table("companies") do
      add :account_plan_id, references(:account_plans), null: false
    end
  end
end
