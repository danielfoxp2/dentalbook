defmodule DentalBookPresenter.Repo.Migrations.AddCroToTeam do
  use Ecto.Migration

  def change do
    alter table("users") do
      add :cro, :string
    end
  end
end
