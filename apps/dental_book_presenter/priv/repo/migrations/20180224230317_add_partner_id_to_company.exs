defmodule DentalBookPresenter.Repo.Migrations.AddPartnerIdToCompany do
  use Ecto.Migration

  def change do
    alter table("companies") do
      add :partner_id, :integer
    end
  end
end
