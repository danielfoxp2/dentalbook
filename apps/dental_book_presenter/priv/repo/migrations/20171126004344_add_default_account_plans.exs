defmodule DentalBookPresenter.Repo.Migrations.AddDefaultAccountPlans do
  use Ecto.Migration

  @insert_command "insert into account_plans (id, description, type, price, start_day, end_day, inserted_at, updated_at, duration)"
  @date_time_format "dd-mm-yyyy hh24:mi:ss"
  @now "current_timestamp"

  @incisivo_plan %{name: "Incisivo", type: :single_user, price: 3490}
  @canino_plan %{name: "Canino", type: :unlimited_users, price: 5990}
  @molar_plan %{name: "Molar", type: :unlimited_users, price: 29990}

  def change do
    execute "
      #{@insert_command} 
      values (290334, '#{@incisivo_plan.name}', '#{@incisivo_plan.type}', #{@incisivo_plan.price}, 
      to_timestamp('25-11-2017 00:00:00', '#{@date_time_format}'),
      to_timestamp('31-12-9999 23:59:59', '#{@date_time_format}'),
      #{@now},
      #{@now},
      30
    )"

    execute "
      #{@insert_command} 
      values (254811, '#{@canino_plan.name}', '#{@canino_plan.type}', #{@canino_plan.price}, 
      to_timestamp('25-11-2017 00:00:00', '#{@date_time_format}'),
      to_timestamp('31-12-9999 23:59:59', '#{@date_time_format}'),
      #{@now},
      #{@now},
      30
    )"

    execute "
      #{@insert_command} 
      values (290335, '#{@molar_plan.name}', '#{@molar_plan.type}', #{@molar_plan.price}, 
      to_timestamp('25-11-2017 00:00:00', '#{@date_time_format}'),
      to_timestamp('31-12-9999 23:59:59', '#{@date_time_format}'),
      #{@now},
      #{@now},
      180
    )"
  end
end
