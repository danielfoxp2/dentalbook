defmodule DentalBookPresenter.Repo.Migrations.CreateCoherenceUser do
  use Ecto.Migration
  def change do
    create table(:users) do
      add :name, :string
      add :email, :string
      add :admin, :boolean
      add :role, :string
      # trackable
      add :sign_in_count, :integer, default: 0
      add :current_sign_in_at, :datetime
      add :last_sign_in_at, :datetime
      add :current_sign_in_ip, :string
      add :last_sign_in_ip, :string
      # recoverable
      add :reset_password_token, :string
      add :reset_password_sent_at, :datetime
      # authenticatable
      add :password_hash, :string
      
      timestamps(type: :timestamptz)
    end
    create unique_index(:users, [:email])

  end
end
