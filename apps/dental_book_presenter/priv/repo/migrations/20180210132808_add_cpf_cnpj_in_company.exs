defmodule DentalBookPresenter.Repo.Migrations.AddCpfCnpjInCompany do
  use Ecto.Migration

  def change do
    alter table("companies") do
      add :cpf_cnpj, :string
    end
  end
end
