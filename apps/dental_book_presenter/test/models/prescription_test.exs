defmodule DentalBookPresenter.PrescriptionTest do
  use DentalBookPresenter.ModelCase

  alias DentalBookPresenter.Prescription

  @valid_attrs %{dentist_cro: "some content", dentist_name: "some content", patient_id: 42, patient_name: "some content", prescription_content: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Prescription.changeset(%Prescription{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Prescription.changeset(%Prescription{}, @invalid_attrs)
    refute changeset.valid?
  end
end
