defmodule DentalBookPresenter.AppointmentProcedureTest do
  use DentalBookPresenter.ModelCase

  alias DentalBookPresenter.AppointmentProcedure

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = AppointmentProcedure.changeset(%AppointmentProcedure{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = AppointmentProcedure.changeset(%AppointmentProcedure{}, @invalid_attrs)
    refute changeset.valid?
  end
end
