defmodule DentalBookPresenter.ExpenseTest do
  use DentalBookPresenter.ModelCase

  alias DentalBookPresenter.Expense

  @valid_attrs %{description: "some content", due_date_at: %{day: 17, month: 4, year: 2010}, value: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Expense.changeset(%Expense{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Expense.changeset(%Expense{}, @invalid_attrs)
    refute changeset.valid?
  end
end
