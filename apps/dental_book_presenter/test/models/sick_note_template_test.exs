defmodule DentalBookPresenter.SickNoteTemplateTest do
  use DentalBookPresenter.ModelCase

  alias DentalBookPresenter.SickNoteTemplate

  @valid_attrs %{content: "some content", id: 42, title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = SickNoteTemplate.changeset(%SickNoteTemplate{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = SickNoteTemplate.changeset(%SickNoteTemplate{}, @invalid_attrs)
    refute changeset.valid?
  end
end
