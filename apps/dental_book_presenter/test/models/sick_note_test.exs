defmodule DentalBookPresenter.SickNoteTest do
  use DentalBookPresenter.ModelCase

  alias DentalBookPresenter.SickNote

  @valid_attrs %{parameters: %{}, patient_id: 42, template_id: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = SickNote.changeset(%SickNote{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = SickNote.changeset(%SickNote{}, @invalid_attrs)
    refute changeset.valid?
  end
end
