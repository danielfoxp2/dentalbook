defmodule DentalBookPresenter.CompanyUserTest do
  use DentalBookPresenter.ModelCase

  alias DentalBookPresenter.CompanyUser

  @valid_attrs %{company_id: 42, user_id: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = CompanyUser.changeset(%CompanyUser{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = CompanyUser.changeset(%CompanyUser{}, @invalid_attrs)
    refute changeset.valid?
  end
end
