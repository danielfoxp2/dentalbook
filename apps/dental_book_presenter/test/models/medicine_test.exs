defmodule DentalBookPresenter.MedicineTest do
  use DentalBookPresenter.ModelCase

  alias DentalBookPresenter.Medicine

  @valid_attrs %{controlled?: true, default_prescription: "some content", description: "some content", use_type: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Medicine.changeset(%Medicine{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Medicine.changeset(%Medicine{}, @invalid_attrs)
    refute changeset.valid?
  end
end
