defmodule DentalBookPresenter.AppointmentTest do
  use DentalBookPresenter.ModelCase

  alias DentalBookPresenter.Appointment

  @valid_attrs %{appointment_date: %{day: 17, month: 4, year: 2010}, appointment_hour: %{hour: 14, min: 0, sec: 0}, patient_name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Appointment.changeset(%Appointment{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Appointment.changeset(%Appointment{}, @invalid_attrs)
    refute changeset.valid?
  end
end
