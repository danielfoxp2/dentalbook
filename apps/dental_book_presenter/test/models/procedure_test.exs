defmodule DentalBookPresenter.ProcedureTest do
  use DentalBookPresenter.ModelCase

  alias DentalBookPresenter.Procedure

  @valid_attrs %{name: "some content", price: "120.5"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Procedure.changeset(%Procedure{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Procedure.changeset(%Procedure{}, @invalid_attrs)
    refute changeset.valid?
  end
end
