defmodule DentalBookPresenter.IncomeTest do
  use DentalBookPresenter.ModelCase

  alias DentalBookPresenter.Income

  @valid_attrs %{created_at: %{day: 17, month: 4, year: 2010}, description: "some content", due_date_at: %{day: 17, month: 4, year: 2010}, paid_at: %{day: 17, month: 4, year: 2010}, value: "120.5"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Income.changeset(%Income{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Income.changeset(%Income{}, @invalid_attrs)
    refute changeset.valid?
  end
end
