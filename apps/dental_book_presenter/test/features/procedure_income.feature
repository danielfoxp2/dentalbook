Funcionalidade: Criação de receitas ao criar procedimentos
    Como secretária criando um procedimento no plano de tratamento
    Quero que o lançamento de receitas seja feito automaticamente junto da criação do procedimento
    Para evitar trabalho extra de ter que criar a receita manualmente depois 

    * Deve permitir o lançamento retroativo de receitas;
    * Não deve permitir data de pagamento maior que a data corrente;
    * Não deve permitir um procedimento sem data de agendamento;
    * Deve lançar a data de criação da receita igual a data atual caso a data de agendamento seja igual ou maior que a data corrente;
    * Deve lançar a data de criação da receita com a menor data entre a data de pagamento e agendamento caso pelo menos uma delas 
    seja menor que a data corrente;
    * Não deve considerar uma data em branco como sendo a menor data;

    Contexto:
        Dado que estou cadastrando um procedimento
        E que a data corrente é "20/08/2017"

    Cenário: Datas de pagamento e agendamento iguais a data corrente
        Dado que informo a data do agendamento como a data corrente
        E que informo a data do pagamento como a data corrente
        Quando confirmo a criação do procedimento
        Então a data de criação da receita é igual a "20/08/2017"

    Cenário: Data de agendamento maior que a data corrente
        Dado que informo a data do agendamento para "21/08/2017"
        Quando confirmo a criação do procedimento
        Então a data de criação da receita é igual a "20/08/2017"
    
    Cenário: Data de agendamento é menor que a data corrente e data de pagamento não existe
        Dado que informei a data de agendamento para "19/08/2017"
        E que não informei a data de pagamento
        Quando confirmo a criação do procedimento
        Então a data de criação da receita é igual a "19/08/2017"

    Cenário: Data de agendamento é menor que a data corrente e data de pagamento é igual a data corrente
        Dado que informei a data de agendamento para "19/08/2017"
        E que informei a data de pagamento para "20/08/2017"
        Quando confirmo a criação do procedimento
        Então a data de criação da receita é igual a "19/08/2017"

    Cenário: Data de pagamento é menor que a data corrente e data de agendamento é igual a data corrente
        Dado que informei a data de agendamento para "20/08/2017"
        E que informei a data de pagamento para "19/08/2017"
        Quando confirmo a criação do procedimento
        Então a data de criação da receita é igual a "19/08/2017"
        
    Cenário: Data de pagamento e agendamento são menores que a data corrente
        Dado que informei a data de agendamento para "17/08/2017"
        E que informei a data de pagamento para "15/08/2017"
        Quando confirmo a criação do procedimento
        Então a data de criação da receita é igual a "15/08/2017"

    Cenário: Data de pagamento maior que a data corrente
        Dado informei a data de pagamento para "21/08/2017"
        Quando confirmo a criação do procedimento
        Então sou informado que não é permitido que a data de pagamento seja maior que a data corrente
    
    Cenário: Data de agendamento não informada
        Dado que não informei a data de agendamento
        Quando confirmo a criação do procedimento
        Então sou informado que não é permitido que data de agendamento não seja informada




