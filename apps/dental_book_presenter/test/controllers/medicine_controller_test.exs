defmodule DentalBookPresenter.MedicineControllerTest do
  use DentalBookPresenter.ConnCase

  alias DentalBookPresenter.Medicine
  @valid_attrs %{controlled?: true, default_prescription: "some content", description: "some content", use_type: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, medicine_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing medicines"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, medicine_path(conn, :new)
    assert html_response(conn, 200) =~ "New medicine"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, medicine_path(conn, :create), medicine: @valid_attrs
    assert redirected_to(conn) == medicine_path(conn, :index)
    assert Repo.get_by(Medicine, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, medicine_path(conn, :create), medicine: @invalid_attrs
    assert html_response(conn, 200) =~ "New medicine"
  end

  test "shows chosen resource", %{conn: conn} do
    medicine = Repo.insert! %Medicine{}
    conn = get conn, medicine_path(conn, :show, medicine)
    assert html_response(conn, 200) =~ "Show medicine"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, medicine_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    medicine = Repo.insert! %Medicine{}
    conn = get conn, medicine_path(conn, :edit, medicine)
    assert html_response(conn, 200) =~ "Edit medicine"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    medicine = Repo.insert! %Medicine{}
    conn = put conn, medicine_path(conn, :update, medicine), medicine: @valid_attrs
    assert redirected_to(conn) == medicine_path(conn, :show, medicine)
    assert Repo.get_by(Medicine, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    medicine = Repo.insert! %Medicine{}
    conn = put conn, medicine_path(conn, :update, medicine), medicine: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit medicine"
  end

  test "deletes chosen resource", %{conn: conn} do
    medicine = Repo.insert! %Medicine{}
    conn = delete conn, medicine_path(conn, :delete, medicine)
    assert redirected_to(conn) == medicine_path(conn, :index)
    refute Repo.get(Medicine, medicine.id)
  end
end
