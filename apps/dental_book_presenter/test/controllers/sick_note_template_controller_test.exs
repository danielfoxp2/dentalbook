defmodule DentalBookPresenter.SickNoteTemplateControllerTest do
  use DentalBookPresenter.ConnCase

  alias DentalBookPresenter.SickNoteTemplate
  @valid_attrs %{content: "some content", id: 42, title: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, sick_note_template_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing sick note templates"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, sick_note_template_path(conn, :new)
    assert html_response(conn, 200) =~ "New sick note template"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, sick_note_template_path(conn, :create), sick_note_template: @valid_attrs
    assert redirected_to(conn) == sick_note_template_path(conn, :index)
    assert Repo.get_by(SickNoteTemplate, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, sick_note_template_path(conn, :create), sick_note_template: @invalid_attrs
    assert html_response(conn, 200) =~ "New sick note template"
  end

  test "shows chosen resource", %{conn: conn} do
    sick_note_template = Repo.insert! %SickNoteTemplate{}
    conn = get conn, sick_note_template_path(conn, :show, sick_note_template)
    assert html_response(conn, 200) =~ "Show sick note template"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, sick_note_template_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    sick_note_template = Repo.insert! %SickNoteTemplate{}
    conn = get conn, sick_note_template_path(conn, :edit, sick_note_template)
    assert html_response(conn, 200) =~ "Edit sick note template"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    sick_note_template = Repo.insert! %SickNoteTemplate{}
    conn = put conn, sick_note_template_path(conn, :update, sick_note_template), sick_note_template: @valid_attrs
    assert redirected_to(conn) == sick_note_template_path(conn, :show, sick_note_template)
    assert Repo.get_by(SickNoteTemplate, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    sick_note_template = Repo.insert! %SickNoteTemplate{}
    conn = put conn, sick_note_template_path(conn, :update, sick_note_template), sick_note_template: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit sick note template"
  end

  test "deletes chosen resource", %{conn: conn} do
    sick_note_template = Repo.insert! %SickNoteTemplate{}
    conn = delete conn, sick_note_template_path(conn, :delete, sick_note_template)
    assert redirected_to(conn) == sick_note_template_path(conn, :index)
    refute Repo.get(SickNoteTemplate, sick_note_template.id)
  end
end
