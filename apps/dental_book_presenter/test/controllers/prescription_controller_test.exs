defmodule DentalBookPresenter.PrescriptionControllerTest do
  use DentalBookPresenter.ConnCase

  alias DentalBookPresenter.Prescription
  @valid_attrs %{dentist_cro: "some content", dentist_name: "some content", patient_id: 42, patient_name: "some content", prescription_content: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, prescription_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing prescriptions"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, prescription_path(conn, :new)
    assert html_response(conn, 200) =~ "New prescription"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, prescription_path(conn, :create), prescription: @valid_attrs
    assert redirected_to(conn) == prescription_path(conn, :index)
    assert Repo.get_by(Prescription, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, prescription_path(conn, :create), prescription: @invalid_attrs
    assert html_response(conn, 200) =~ "New prescription"
  end

  test "shows chosen resource", %{conn: conn} do
    prescription = Repo.insert! %Prescription{}
    conn = get conn, prescription_path(conn, :show, prescription)
    assert html_response(conn, 200) =~ "Show prescription"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, prescription_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    prescription = Repo.insert! %Prescription{}
    conn = get conn, prescription_path(conn, :edit, prescription)
    assert html_response(conn, 200) =~ "Edit prescription"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    prescription = Repo.insert! %Prescription{}
    conn = put conn, prescription_path(conn, :update, prescription), prescription: @valid_attrs
    assert redirected_to(conn) == prescription_path(conn, :show, prescription)
    assert Repo.get_by(Prescription, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    prescription = Repo.insert! %Prescription{}
    conn = put conn, prescription_path(conn, :update, prescription), prescription: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit prescription"
  end

  test "deletes chosen resource", %{conn: conn} do
    prescription = Repo.insert! %Prescription{}
    conn = delete conn, prescription_path(conn, :delete, prescription)
    assert redirected_to(conn) == prescription_path(conn, :index)
    refute Repo.get(Prescription, prescription.id)
  end
end
