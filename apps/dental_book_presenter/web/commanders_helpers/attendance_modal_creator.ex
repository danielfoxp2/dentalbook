defmodule DentalBookPresenter.AttendanceModalCreator do
  use Drab.Commander
  use Phoenix.HTML 
  alias DentalBookPresenter.ProcedureAwesomeplete
  alias DentalBookPresenter.ProcedureToothChooser
  alias DentalBookPresenter.ProcedureDone
  alias DentalBookPresenter.ProcedureDate
  alias DentalBookPresenter.AttendanceHour
  alias DentalBookPresenter.ProcedurePrice
  alias DentalBookPresenter.ProcedureToothFaces
  alias DentalBookPresenter.ScriptModal
  alias DentalBookPresenter.DateFormat
  alias DentalBookPresenter.TimeFormat

  @empty_value ""

  def create_form(_socket) do
    [
      ProcedureAwesomeplete.make(),
      ProcedureToothChooser.make(),
      ProcedureToothFaces.make(),
      ProcedurePrice.make(@empty_value),
      ProcedureDone.make(),
      ProcedureDate.make("attendance_date_at", "Agendado para", today()),
      AttendanceHour.make(TimeFormat.now()),
      ProcedureDate.make("due_date_at", "Vencimento", today()), 
      ProcedureDate.make("paid_at", "Pagamento", @empty_value), 
      script_of_modal_components()
    ]
    |> Enum.join
  end 

  defp today(), do: DateFormat.now |> DateFormat.ecto_date_to_string

  defp script_of_modal_components() do
    "<script type=\"text/javascript\">
      #{ScriptModal.select2()}
    </script>
    
    <script type=\"text/javascript\">
      #{ScriptModal.awesomplete()}
    </script>
      
    <script type=\"text/javascript\">
      #{ScriptModal.done()}
    </script>

    <script type=\"text/javascript\">
       #{ScriptModal.clockpicker()}
    </script>

    <script type=\"text/javascript\">
       #{ScriptModal.datepicker_config()}
    </script>

    <script type=\"text/javascript\">
       #{ScriptModal.datepicker()}
    </script>
     "
  end

end