defmodule DentalBookPresenter.ProcedureToothChooser do
  use Phoenix.HTML

  def make() do
    content_tag :div do
    [
      create_tooth_label(),
      procedure_tooth()
    ]
    end
    |> safe_to_string
  end

  def make_update(value) do
   content_tag :div do
   [
     create_tooth_label(),
     procedure_tooth_updated(value)
   ]
   end
   |> safe_to_string
  end

  def make_error(value, error_message) do
    content_tag :div do
    [
      create_tooth_label(),
      create_tooth_label_error(error_message),
      procedure_tooth()
    ]
    end
    |> safe_to_string 
  end

  defp procedure_tooth() do
    content_tag :div do
    [  
      content_tag :select, 
        id: :appointment_ui_procedures_incomes_procedure_tooth, 
        name: "appointment_ui[procedures_incomes][procedure_tooth]",
        class: "select2 form-control select2-multiple",
        multiple: :multiple
      do
      [
          teeth_options(),
          content_tag :option, value: "Arcadas" do ["Arcadas"] end,
          content_tag :option, value: "Arcada Superior" do ["Arcada Superior"] end,
          content_tag :option, value: "Arcada Inferior" do ["Arcada Inferior"] end,
          content_tag :option, value: "Mandíbula" do ["Mandíbula"] end,
          content_tag :option, value: "Maxila" do ["Maxila"] end
      ]  
      end,
      tag(:hr, class: "no-margin color-line")
    ]
    end
  end

  defp teeth_options() do
    create_teeth_range()
    |> get_teeth_flattened_from()
    |> create_tooth_options
  end

  defp create_teeth_range(), do: [11..18, 21..28, 31..38, 41..48, 51..55, 61..65, 71..75, 81..85]

  defp get_teeth_flattened_from(multi_dimensional_range) do
    multi_dimensional_range |> Enum.map(fn elements -> elements |> Enum.map(fn element -> element end) end) 
    |> List.flatten
  end

  defp create_tooth_options(teeth) do
    Enum.map teeth, fn tooth -> get_option(tooth) end
  end

  defp get_option(tooth) do
    content_tag :option, value: "#{tooth}" do ["#{tooth}"] end
  end

  defp create_tooth_label() do
    label :appointment_ui_procedures_incomes, :procedure_tooth, class: "control-label make-it-50-50" do "Dente" end
  end

  defp procedure_tooth_updated(value) do
    chosen_teeth = value |> create_teeth_hidden_value
    teeth_first_span_classes = "select2 select2-container select2-container--default select2-container--above"
    teeth_third_span_classes = "select2-selection select2-selection--multiple"
    content_tag :div do
    [
      tag(:input, type: :hidden, class: "form-control", name: "appointment_ui[procedures_incomes][procedure_tooth]", value: chosen_teeth, disabled: :disabled),
      content_tag :span, class: teeth_first_span_classes do
      [
        content_tag :span, class: :selection do
        [
          content_tag :span, class: teeth_third_span_classes, style: "cursor: not-allowed;" do
          [
            content_tag :ul, class: "select2-selection__rendered" do
              Enum.map(value, fn tooth -> 
                content_tag :li, class: "select2-selection__choice", title: tooth, value: tooth do
                [
                    tooth 
                ]
                end
              end) 
            end
          ]
          end
        ]
        end
      ]
      end,
      tag(:hr, class: "no-margin color-line")
    ]
    end
  end

  defp create_teeth_hidden_value(teeth) when is_binary(teeth), do: teeth 
  defp create_teeth_hidden_value(teeth), do: teeth |> Enum.join(",")

  defp create_tooth_label_error(error_message) do
    content_tag :div, class: "text-right make-it-50-50" do
      [label :appointment_ui_procedures_incomes, :procedure_tooth, class: "control-label text-danger" do "#{error_message}" end]
    end
  end

  
end