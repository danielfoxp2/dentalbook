defmodule DentalBookPresenter.AttendanceModalUpdater do
  use Drab.Commander
  use Phoenix.HTML 
  alias DentalBookPresenter.ProcedureAwesomeplete
  alias DentalBookPresenter.ProcedureToothChooser
  alias DentalBookPresenter.ProcedureDone
  alias DentalBookPresenter.ProcedureDate
  alias DentalBookPresenter.AttendanceHour
  alias DentalBookPresenter.ProcedurePrice
  alias DentalBookPresenter.ProcedureToothFaces
  alias DentalBookPresenter.ScriptModal

  def create_form(parsed_params, optional_error_messages \\ []) do
    [
      create_procedure_name(parsed_params),
      create_tooth_chooser(parsed_params),
      create_tooth_faces_chooser(parsed_params),
      create_price_input(parsed_params),
      create_done_checkbox(parsed_params),
      create_attendace_date_datepicker(parsed_params),
      create_attendace_hour_hourpicker(parsed_params),
      create_due_date_datepicker(parsed_params),
      create_paid_date_datepicker(parsed_params),
      script_of_modal_components()
    ]
    |> Enum.join
  end

  defp create_procedure_name(parsed_params) do
    name = get_element_in(parsed_params, "procedure_name")
    procedure_id = get_element_in(parsed_params, "procedure_id")
    ProcedureAwesomeplete.make_update(name, procedure_id)
  end

  defp create_tooth_chooser(parsed_params) do
    chosen_tooth = get_element_in(parsed_params, "procedure_tooth")
    ProcedureToothChooser.make_update(chosen_tooth)
  end

  defp create_tooth_faces_chooser(parsed_params) do
    tooth_faces = get_element_in(parsed_params, "procedure_tooth_faces")
    ProcedureToothFaces.make_update(tooth_faces)
  end

  defp create_price_input(parsed_params) do
    price = get_element_in(parsed_params, "price")
    ProcedurePrice.make(price)
  end

  defp create_done_checkbox(parsed_params) do
    done = get_element_in(parsed_params, "done")
    ProcedureDone.make_update(done)
  end

  defp create_attendace_date_datepicker(parsed_params) do
    attendance_date = get_element_in(parsed_params, "attendance_date_at")
    ProcedureDate.make("attendance_date_at", "Agendado para", attendance_date)
  end

  defp create_attendace_hour_hourpicker(parsed_params) do
    attendance_hour = get_element_in(parsed_params, "attendance_hour")
    AttendanceHour.make(attendance_hour)
  end
  
  defp create_due_date_datepicker(parsed_params) do
    due_date = get_element_in(parsed_params, "due_date_at")
    ProcedureDate.make("due_date_at", "Vencimento", due_date)
  end

  defp create_paid_date_datepicker(parsed_params) do
    paid_at = get_element_in(parsed_params, "paid_at")
    ProcedureDate.make("paid_at", "Pagamento", paid_at)
  end

  defp get_element_in(parsed_params, element_to_be_returned) do
    parsed_params["appointment_ui[procedures_incomes][#{element_to_be_returned}]"]
  end

  defp script_of_modal_components() do
    "         
    <script type=\"text/javascript\">
      #{ScriptModal.done()}
    </script>

    <script type=\"text/javascript\">
       #{ScriptModal.clockpicker()}
    </script>

    <script type=\"text/javascript\">
       #{ScriptModal.datepicker_config()}
    </script>

    <script type=\"text/javascript\">
       #{ScriptModal.datepicker()}
    </script>
     "
  end
end