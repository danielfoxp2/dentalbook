defmodule DentalBookPresenter.AttendanceModalError do
  use Drab.Commander
  use Phoenix.HTML 
  alias DentalBookPresenter.ProcedureAwesomeplete
  alias DentalBookPresenter.ProcedureToothChooser
  alias DentalBookPresenter.ProcedureDone
  alias DentalBookPresenter.ProcedureDate
  alias DentalBookPresenter.AttendanceHour
  alias DentalBookPresenter.ProcedurePrice
  alias DentalBookPresenter.ProcedureToothFaces
  alias DentalBookPresenter.ScriptModal

  
  def create_form(parsed_params, optional_error_messages \\ []) do
    [
      parsed_params |> create_procedure_name(get_error_message(optional_error_messages, "procedure_id")),
      parsed_params |> create_tooth_chooser(get_error_message(optional_error_messages, "procedure_tooth")),
      parsed_params |> create_tooth_faces_chooser(),
      parsed_params |> create_price_input(get_error_message(optional_error_messages, "value")),
      parsed_params |> create_done_checkbox(),
      parsed_params |> create_attendace_date_datepicker(get_error_message(optional_error_messages, "attendance_date_at")),
      parsed_params |> create_attendace_hour_hourpicker(get_error_message(optional_error_messages, "attendance_hour")),
      parsed_params |> create_due_date_datepicker(get_error_message(optional_error_messages, "due_date_at")),
      parsed_params |> create_paid_date_datepicker(get_error_message(optional_error_messages, "paid_at")),
      parsed_params |> script_of_multi_select("procedure_tooth"),
      parsed_params |> script_of_multi_select("procedure_tooth_faces"),
      script_of_modal_components()
    ]
    |> Enum.join
  end

  defp create_procedure_name(parsed_params, error_message) do
    name = get_element_in(parsed_params, "procedure_name")
    procedure_id = get_element_in(parsed_params, "procedure_id")
    ProcedureAwesomeplete.make_error(name, procedure_id, error_message)
  end

  defp get_error_message(error_messages, field) do
    List.keyfind(error_messages, field, 0) 
    |> get_message()
  end

  defp get_message(value) when is_nil(value), do: ""
  defp get_message(value) do
    {_, message} = value
    message
  end

  defp create_tooth_chooser(parsed_params, error_message) do
    teeth = get_element_in(parsed_params, "procedure_tooth")
    make_teeth_input_from(teeth, error_message)
  end

  defp make_teeth_input_from(teeth, error_message) when teeth == [], do: ProcedureToothChooser.make_error(teeth, error_message)
  defp make_teeth_input_from(teeth, _error_message), do: ProcedureToothChooser.make_update(teeth)

  defp create_tooth_faces_chooser(parsed_params) do
    tooth_faces = get_element_in(parsed_params, "procedure_tooth_faces")
    make_input_of(tooth_faces)
  end

  defp make_input_of(tooth_faces) when tooth_faces == [], do: ProcedureToothFaces.make()
  defp make_input_of(tooth_faces), do: ProcedureToothFaces.make_update(tooth_faces)

  defp create_price_input(parsed_params, error_message) do
    price = get_element_in(parsed_params, "price")
    ProcedurePrice.make(price, error_message)
  end

  defp create_done_checkbox(parsed_params) do
    done = get_element_in(parsed_params, "done")
    ProcedureDone.make_update(done)
  end

  defp create_attendace_date_datepicker(parsed_params, error_message) do
    attendance_date = get_element_in(parsed_params, "attendance_date_at")
    ProcedureDate.make("attendance_date_at", "Agendado para", attendance_date, error_message)
  end

  defp create_attendace_hour_hourpicker(parsed_params, error_message) do
    attendance_hour = get_element_in(parsed_params, "attendance_hour")
    AttendanceHour.make(attendance_hour, error_message)
  end
  
  defp create_due_date_datepicker(parsed_params, error_message) do
    due_date = get_element_in(parsed_params, "due_date_at")
    ProcedureDate.make("due_date_at", "Vencimento", due_date, error_message)
  end

  defp create_paid_date_datepicker(parsed_params, error_message) do
    paid_at = get_element_in(parsed_params, "paid_at")
    ProcedureDate.make("paid_at", "Pagamento", paid_at, error_message)
  end

  defp script_of_multi_select(parsed_params, tooth_field) do
    tooth_id = "#appointment_ui_procedures_incomes_#{tooth_field}"
    tooth_data = get_element_in(parsed_params, tooth_field)
    script_for_create_tooth_inputs(tooth_data, tooth_id)
  end

  defp script_for_create_tooth_inputs(tooth_data, tooth_id) when is_list(tooth_data) do
    "<script type=\"text/javascript\">
      #{ScriptModal.select2(tooth_id)}
    </script>"
  end  
  defp script_for_create_tooth_inputs(_tooth_data, _tooth_id), do: ""

  defp get_element_in(parsed_params, element_to_be_returned) do
    parsed_params["appointment_ui[procedures_incomes][#{element_to_be_returned}]"]
  end

  defp script_of_modal_components() do
    "
    <script type=\"text/javascript\">
      #{ScriptModal.awesomplete()}
    </script>
      
    <script type=\"text/javascript\">
      #{ScriptModal.done()}
    </script>

    <script type=\"text/javascript\">
       #{ScriptModal.clockpicker()}
    </script>

    <script type=\"text/javascript\">
       #{ScriptModal.datepicker_config()}
    </script>

    <script type=\"text/javascript\">
       #{ScriptModal.datepicker()}
    </script>
     "
  end

end