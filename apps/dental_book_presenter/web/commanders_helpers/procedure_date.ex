defmodule DentalBookPresenter.ProcedureDate do
  use Phoenix.HTML 
  
  def make(input_attribute, input_label, value, error_message \\ "") do
    content_tag :div do
      [
        create_procedure_date_label(input_attribute, input_label),
        create_procedure_date_label_error(input_attribute, error_message),
        content_tag :div, class: "input-group col-md-12" do
          [
            tag(:input, 
            type: :text, 
            id: "appointment_ui_procedures_incomes_#{input_attribute}",
            name: "appointment_ui[procedures_incomes][#{input_attribute}]", 
            class: "form-control appointment_ui_date",
            placeholder: "dd/mm/aaaa",
            value: value)
          ]
        end
      ]
    end
     |> safe_to_string
  end

  defp create_procedure_date_label(input_attribute, input_label) do
    label :appointment_ui_procedures_incomes, "attendance_#{input_attribute}", class: "control-label make-it-50-50" do input_label end
  end

  defp create_procedure_date_label_error(input_attribute, error_message) do
    content_tag :div, class: "text-right make-it-50-50" do
      [label :appointment_ui_procedures_incomes, "attendance_#{input_attribute}", class: "control-label text-danger" do "#{error_message}" end]
    end
  end
 

end