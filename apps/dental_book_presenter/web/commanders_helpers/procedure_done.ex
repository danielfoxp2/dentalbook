defmodule DentalBookPresenter.ProcedureDone do
  use Phoenix.HTML
  alias DentalBookPresenter.TagAttributes

  def make() do
    "<div>
      <div>
        <label class=\"control-label\" for=\"appointment_ui_procedures_incomes_done\">Realizado?</label>
      </div>
      #{procedure_done()}
      <hr class=\"no-margin color-line\">      
    </div>"  
  end

  def make_update(value) do
    "<div>
      <div>
        <label class=\"control-label\" for=\"appointment_ui_procedures_incomes_done\">Realizado?</label>
      </div>
      #{procedure_done_update(value)}
      <hr class=\"no-margin color-line\">      
    </div>"  
  end

  defp procedure_done_update(value) do
    tag_attributes_hidden = %TagAttributes{consider_id: false, field_name: "done", field_type: "hidden", field_value: get_opposite_done_value_of(value)}
    tag_attributes_checkbox = %TagAttributes{field_name: "done", field_type: "checkbox", field_value: value}
  
    "<input #{configure_tag_with(tag_attributes_hidden)} >
     <input #{configure_tag_with(tag_attributes_checkbox)} class=\"done\" switch=\"info\">
     #{configure_tag_label(tag_attributes_checkbox)}"
  end

  defp get_opposite_done_value_of(value) when value == "true", do: "false"
  defp get_opposite_done_value_of(value) when value == "false", do: "true"

  defp procedure_done() do
    tag_attributes_hidden = %TagAttributes{consider_id: false, field_name: "done", field_type: "hidden", field_value: "true"}
    tag_attributes_checkbox = %TagAttributes{field_name: "done", field_type: "checkbox", field_value: "false"}
    
    "<input #{configure_tag_with(tag_attributes_hidden)} >
     <input #{configure_tag_with(tag_attributes_checkbox)} switch=\"info\">
     #{configure_tag_label(tag_attributes_checkbox)}"
  end

    defp configure_tag_label(tag_attributes_checkbox) do
    label_for = procedure_income_tag_id(tag_attributes_checkbox) |> String.replace("id=", "")
    {_, tag_label} = raw("<label for=#{label_for} data-on-label=\"Sim\" data-off-label=\"Não\" class=\"m-b-0 m-t-10\"></label>")

    tag_label
  end

  defp configure_tag_with(tag_attributes) do
    procedure_income_tag_id(tag_attributes) 
    |> procedure_income_tag_name(tag_attributes) 
    |> procedure_income_tag_type(tag_attributes)
    |> procedure_income_tag_value(tag_attributes)
    |> procedure_income_tag_class(tag_attributes)
  end

  defp procedure_income_tag_id(%TagAttributes{consider_id: false} = params), do: ""
  defp procedure_income_tag_id(%TagAttributes{consider_id: consider_id} = params) when consider_id == true do
    "id=\"appointment_ui_procedures_incomes_#{params.field_name}\""
  end

  defp procedure_income_tag_name(previous_attributes, %TagAttributes{} = params) do
    previous_attributes <> "name=\"appointment_ui[procedures_incomes][#{params.field_name}]\""
  end

  defp procedure_income_tag_type(previous_attributes, %TagAttributes{} = params) do
    previous_attributes <> "type=\"" <> params.field_type <> "\""
  end

  defp procedure_income_tag_value(previous_attributes, %TagAttributes{field_value: field_value} = params) 
  when field_value == nil do previous_attributes end

  defp procedure_income_tag_value(previous_attributes, %TagAttributes{field_value: field_value} = params) 
  when field_value != nil do
    previous_attributes <> "value=\"" <> field_value <> "\""
  end

  defp procedure_income_tag_class(previous_attributes, %TagAttributes{field_class: field_class} = params)
  when field_class == nil do previous_attributes end

  defp procedure_income_tag_class(previous_attributes, %TagAttributes{field_class: field_class} = params)
  when field_class != nil do
    previous_attributes <> "class=\"" <> field_class <> "\""
  end

end