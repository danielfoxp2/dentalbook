defmodule DentalBookPresenter.ProcedurePrice do
  use Phoenix.HTML
  
  def make(with_value, error_message \\ "") do
    content_tag :div do
    [
      create_procedure_price_label(),
      create_procedure_price_label_error(error_message),
      tag(:input, 
          type: :text, 
          id: :appointment_ui_procedures_incomes_price, 
          name: "appointment_ui[procedures_incomes][price]", 
          class: "form-control autonumber", 
          data: [a: [sep: "."]], 
          data: [a: [dec: ","]],
          value: with_value)
    ]
    end
    |> safe_to_string
  end

  defp create_procedure_price_label() do
    label :appointment_ui_procedures_incomes, :price, class: "control-label make-it-50-50" do "Valor" end
  end

  defp create_procedure_price_label_error(error_message) do
    content_tag :div, class: "text-right make-it-50-50" do
      [label :appointment_ui_procedures_incomes, :price, class: "control-label text-danger" do "#{error_message}" end]
    end
  end

end