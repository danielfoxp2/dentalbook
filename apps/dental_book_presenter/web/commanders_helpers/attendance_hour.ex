defmodule DentalBookPresenter.AttendanceHour do
  use Phoenix.HTML
  
  def make(with_value, error_message \\ "") do
    content_tag :div do
     [
       create_procedure_hour_label(),
       create_procedure_hour_label_error(error_message),
       content_tag :div, class: "input-group clockpicker col-xs-12" do
         [
           tag(:input, 
             type: :text, 
             id: :appointment_ui_procedures_incomes_attendance_hour,
             name: "appointment_ui[procedures_incomes][attendance_hour]", 
             class: "form-control",
             value: with_value
           )
         ]
       end
     ]
    end
    |> safe_to_string
  end

  defp create_procedure_hour_label() do
    label :appointment_ui_procedures_incomes, :attendance_hour, class: "control-label make-it-50-50" do "Hora" end
  end

  defp create_procedure_hour_label_error(error_message) do
    content_tag :div, class: "text-right make-it-50-50" do
      [label :appointment_ui_procedures_incomes, :attendance_hour, class: "control-label text-danger" do "#{error_message}" end]
    end
  end

end