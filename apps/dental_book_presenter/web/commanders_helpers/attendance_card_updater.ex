defmodule DentalBookPresenter.AttendanceCardUpdater do
  use Phoenix.HTML
  use Drab.Commander
  alias DentalBookPresenter.AppointmentCard
  alias DentalBookPresenter.AppointmentHiddenCard
  alias DentalBookPresenter.AppointmentUpdatableCard
  alias DentalBookPresenter.MoneyFormat

  def make(line_id, socket, params) do
    update_attendance_card(line_id, socket, params)
    update_attendance_hidden(line_id, socket, params)
    update_attendance_updatable(line_id, socket, params)
  end

  defp update_attendance_card(line_id, socket, params) do
    socket |> update(:html, set: mount_data_of(AppointmentCard, line_id, params), on: "#attendances-cards .#{line_id}")
  end

  defp update_attendance_hidden(line_id, socket, params) do
    socket |> update(:html, set: mount_data_of(AppointmentHiddenCard, line_id, params), on: "#attendances-data .#{line_id}")
  end

  defp update_attendance_updatable(line_id, socket, params) do
    socket |> update(:html, set: mount_updatable_data(params), on: "#" <> line_id)
  end

  defp mount_data_of(card, line_id, params) do
    line_id 
    |> String.last 
    |> mount_card_elements(params)
    |> card.make
    |> safe_to_string 
  end

  defp mount_updatable_data(params), do: AppointmentUpdatableCard.make(params)

  defp mount_card_elements(id, params) do
    %{
      id: "appointment_ui_procedures_incomes_#{id}",
      name: "appointment_ui[procedures_incomes][#{id}]",
      procedure_name: params[get(:procedure_name)],
      attendance_date_at: params[get(:attendance_date_at)],
      attendance_hour: params[get(:attendance_hour)],
      price: params[get(:price)] |> MoneyFormat.parse_value_to_money |> Money.to_string,
      done: params[get(:done)] |> parse_boolean,
      procedure_tooth: params[get(:procedure_tooth)],
      procedure_tooth_faces: params[get(:procedure_tooth_faces)],
      due_date_at: params[get(:due_date_at)],
      paid_at: params[get(:paid_at)],
      procedure_id: params[get(:procedure_id)]
    }
  end

  defp get(field), do: "appointment_ui[procedures_incomes][#{field}]"

  defp parse_boolean("true"), do: true
  defp parse_boolean("false"), do: false

end