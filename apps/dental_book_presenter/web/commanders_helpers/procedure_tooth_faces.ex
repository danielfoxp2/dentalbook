defmodule DentalBookPresenter.ProcedureToothFaces do
  use Phoenix.HTML

  def make() do
    content_tag :div do
    [  
      label :appointment_ui_procedures_incomes, :procedure_tooth_face, class: "control-label" do
      "Faces do dente"
      end,
      
      content_tag :select, 
      id: :appointment_ui_procedures_incomes_procedure_tooth_faces, 
      name: "appointment_ui[procedures_incomes][procedure_tooth_faces]",
      class: "select2 form-control select2-multiple",
      multiple: :multiple
      do
      [
          content_tag :option, value: "Vestibular" do ["Vestibular"] end,
          content_tag :option, value: "Mesial" do ["Mesial"] end,
          content_tag :option, value: "Distal" do ["Distal"] end,
          content_tag :option, value: "Oclusal" do ["Oclusal"] end,
          content_tag :option, value: "Lingual/Palatal" do ["Lingual/Palatal"] end
      ]  
      end,
      
      tag(:hr, class: "no-margin color-line")
    ]
    end
    |> safe_to_string
  end

  def make_update(value) do
    chosen_tooth_faces = value |> create_tooth_faces_hidden_value
    tooth_faces_first_span_classes = "select2 select2-container select2-container--default select2-container--above"
    tooth_faces_third_span_classes = "select2-selection select2-selection--multiple"
    content_tag :div do
    [
      label :appointment_ui_procedures_incomes, :procedure_tooth_face, class: "control-label" do "Faces do dente" end,
      tag(:input, type: :hidden, class: "form-control", name: "appointment_ui[procedures_incomes][procedure_tooth_faces]", value: chosen_tooth_faces, disabled: :disabled),
      content_tag :span, class: tooth_faces_first_span_classes do
      [
        content_tag :span, class: :selection do
        [
          content_tag :span, class: tooth_faces_third_span_classes, style: "cursor: not-allowed;" do
          [
            content_tag :ul, class: "select2-selection__rendered" do
              Enum.map(value, fn tooth_face -> 
                content_tag :li, class: "select2-selection__choice", title: tooth_face, value: tooth_face do
                [
                    tooth_face 
                ]
                end
              end) 
            end
          ]
          end
        ]
        end
      ]
      end,
      tag(:hr, class: "no-margin color-line")
    ]
    end
    |> safe_to_string
  end
  
  defp create_tooth_faces_hidden_value(tooth_faces) when is_binary(tooth_faces), do: tooth_faces 
  defp create_tooth_faces_hidden_value(tooth_faces), do: tooth_faces |> Enum.join(",")
end