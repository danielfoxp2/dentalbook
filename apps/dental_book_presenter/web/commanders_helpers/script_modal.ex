defmodule DentalBookPresenter.ScriptModal do

  def select2(selector \\ ".select2"), do: "$(\"#{selector}\").select2();"

  def awesomplete() do 
    "#{awesomplete("procedure_price", "appointment_ui_procedures_incomes_price")}

     #{awesomplete("procedure_id", "appointment_ui_procedures_incomes_procedure_id")}"
  end

  def done() do
    "$(function(){
        if($(\'.done\').val() === \"true\"){
            $(\'.done\').click();
        }

        document.getElementById(\'appointment_ui_procedures_incomes_done\').addEventListener(\'click\', function() {
            var checkBoxStatus = $(\'#appointment_ui_procedures_incomes_done\').val();
            if (checkBoxStatus === \"true\"){
                $(\'#appointment_ui_procedures_incomes_done\').val(false);
            }
            else{
                $(\'#appointment_ui_procedures_incomes_done\').val(true);
            }
        });
    });"
  end

  def clockpicker() do
    "$(function(){
        $(\'#appointment_ui_procedures_incomes_attendance_hour\').clockpicker({
            placement: \'top\',
            align: \'right\',
            autoclose: true,
            default: \'now\'
        });
     });"
  end

  def datepicker_config() do
    "//É preciso colocar esse javascript aqui para poder
    //utilizar os controles externos de datepicker e clockpicker
    //Os imports ficaram no layout e o uso ficou no ready da última partial filha

    /*Datepicker Functions */

    function hasCharacter(value){
      return isNaN(value);
    }

    function clearDatePickerIfIsAnInvalidYear(datePicker, dateAsString){
      [day, month, year] = $(dateAsString).val().split(\"/\");
      if(hasCharacter(day)) { datePicker.clear(); }
      if(hasCharacter(month)) { datePicker.clear(); }
      if(hasCharacter(year)) { datePicker.clear(); }
      if(year < 100) { datePicker.clear(); }
    }

    function returnToSelectedDate(datePickerShown, dateAsString){
      var selectedDateString = $(dateAsString).val();
      if (selectedDateString == \"\") { return; }
      
      [day, month, year] = selectedDateString.split(\"/\");

      var selectedDate = new Date(year, month -1, day);

      datePickerShown.date = selectedDate;
      datePickerShown.selectDate(selectedDate);
    }

    function createDatePicker(inputId){
      var datePicker = $(inputId).datepicker({
        language: \"pt-BR\",
        position: \'top right\',
        clearButton: true,
        todayButton: new Date(),
        dateFormat : \'dd/mm/yyyy\',
        maxDate: new Date(\"2100\", \"01\", \"01\"),
        onHide: function (datePickerShown, animationCompleted){ clearDatePickerIfIsAnInvalidYear(datePickerShown, inputId); },
        onShow: function(datePickerShown, animationCompleted){ returnToSelectedDate(datePickerShown, inputId); }
      }).data(\'datepicker\');

      return datePicker;
    }

    function isNotAValidDay(day, month){
        var february = 2;
        var lastValidDayInFebruary = 29;

        if(hasCharacter(day)) return true;
        
        if(month == february && day > lastValidDayInFebruary) { return true; }

        if (day < 1 || day > 31) { return true; }
        debugger;
        return false;
    }

    function isNotAValidMonth(month){
      if(hasCharacter(month)) return true;

      return (month < 1 || month > 12);
    }


    function hasAtLeastTwoCaractersIn(year){
      return year.length >= 2;
    }
    
    function isAValidYear(year){
      return year > 99;
    }

    function selectingDateManually(partialDateSelected, datePicker){
      var dateElements = partialDateSelected.val().split(\'/\');
      
      if(dateElements.length != 3){ return; }
      debugger;
      [day, month, year] = dateElements;

      if(isNotAValidDay(day, month) || isNotAValidMonth(month)){
        datePicker.clear();
        return;
      }

      if(hasAtLeastTwoCaractersIn(year) && isAValidYear(year)){
        var newSelectedDate = new Date(year, month - 1, day);
        datePicker.date = newSelectedDate;
        datePicker.selectDate(newSelectedDate);
      }
    
    }

    function datePickerInitialize(inputId, index, array) {
      var datePicker = createDatePicker(inputId);

      $(inputId).on(\'keyup\', function(){
        selectingDateManually($(this), datePicker);
      });
    }

    function datePickerInit(inputsIds){
      for(var element = 0; element < inputsIds.length; element++){
        datePickerInitialize(inputsIds[element])
      }
    }"
  end

  def datepicker() do
    "$(function(){
        datePickerInit([\'#appointment_ui_procedures_incomes_attendance_date_at\', 
                        \'#appointment_ui_procedures_incomes_due_date_at\', 
                        \'#appointment_ui_procedures_incomes_paid_at\']);
    });"
  end

  defp awesomplete(field, destination) do
    "AwesompleteUtil.startCopy(\'#appointment_ui_procedures_incomes_procedure_name\', 
                               \'#{field}\',
                               \'##{destination}\');"   
  end
end