defmodule DentalBookPresenter.AttendanceCardCreator do
  use Phoenix.HTML
  use Drab.Commander
  alias DentalBookPresenter.AppointmentCard
  alias DentalBookPresenter.AppointmentHiddenCard
  alias DentalBookPresenter.AppointmentUpdatableCard
  alias DentalBookPresenter.MoneyFormat
  
  def make(id, socket, params) do
    create_attendance_card(id, socket, params)
    create_attendance_hidden(id, socket, params)
    create_attendance_updatable(id, socket, params)
  end

  defp create_attendance_card(id, socket, params) do
    socket |> insert(mount_card_content(params, id), append: "#attendances-cards")
  end

  defp create_attendance_hidden(id, socket, params) do
    socket |> insert(mount_hidden_data(params, id), append: "#attendances-data")
  end

  defp create_attendance_updatable(id, socket, params) do
    socket |> insert(mount_updatable_data(params, id), append: "#attendances-updatable")
  end

  defp mount_card_elements(id, params) do
    %{
      id: "#{get_identifier(id)}",
      name: "appointment_ui[procedures_incomes][#{id}]",
      procedure_name: params[get(:procedure_name)],
      attendance_date_at: params[get(:attendance_date_at)],
      attendance_hour: params[get(:attendance_hour)],
      price: params[get(:price)] |> MoneyFormat.parse_value_to_money |> Money.to_string,
      done: params[get(:done)] |> parse_boolean,
      procedure_tooth: params[get(:procedure_tooth)],
      procedure_tooth_faces: params[get(:procedure_tooth_faces)],
      due_date_at: params[get(:due_date_at)],
      paid_at: params[get(:paid_at)],
      procedure_id: params[get(:procedure_id)]
    }
  end

  defp mount_card_content(params, id) do
    content_tag :div, class: "#{get_identifier(id)} col-md-4" do
      [mount_card_elements(id, params) |> AppointmentCard.make]
    end
    |> safe_to_string
  end

  defp mount_hidden_data(params, id) do
    content_tag :div, class: "#{get_identifier(id)}" do
       mount_card_elements(id, params) |> AppointmentHiddenCard.make
    end
    |> safe_to_string
  end

  defp mount_updatable_data(params, id) do
    content_tag :div, id: "#{get_identifier(id)}", class: "#{get_identifier(id)} updatable" do
      [AppointmentUpdatableCard.make(params)]
    end
    |> safe_to_string
  end

  defp get(field), do: "appointment_ui[procedures_incomes][#{field}]"

  defp parse_boolean("true"), do: true
  defp parse_boolean("false"), do: false

  defp get_identifier(id), do: "appointment_ui_procedures_incomes_#{id}"

end