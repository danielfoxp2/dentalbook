defmodule DentalBookPresenter.ProcedureAwesomeplete do
  use Phoenix.HTML

  def make() do
    content_tag :div do
    [
      create_procedure_label(),
      procedure_name(),
      procedure_id()
    ]
    end
    |> safe_to_string
  end

  def make_update(name, procedure_id) do
    content_tag :div do
    [
      create_procedure_label(),
      procedure_name_updated(name),
      procedure_id_value(procedure_id)
    ]
    end
    |> safe_to_string
  end

  def make_error(name, procedure_id, error_message) do
    content_tag :div, class: "text-left" do
    [
      create_procedure_label(),
      create_procedure_label_error(error_message), 
      procedure_name(name),
      procedure_id_value(procedure_id)
    ]
    end
    |> safe_to_string
  end

  defp procedure_name(value \\ "") do
   raw("<div class=\"awesomplete\" style=\"z-index:3000;\">
     <input 
      class=\"form-control\"
      id=\"appointment_ui_procedures_incomes_procedure_name\" 
     name=\"appointment_ui[procedures_incomes][procedure_name]\" 
     value=\"#{value}\"
     type=\"text\" 
     autocomplete=\"off\" 
     aria-autocomplete=\"list\">
     <ul hidden=\"\"></ul>
     <span class=\"visually-hidden\" 
       role=\"status\" 
       aria-live=\"assertive\" 
       aria-relevant=\"additions\">
     </span>
    </div>
    <script>
     var awe_appointment_ui_procedures_incomes_procedure_name=AwesompleteUtil.start('#appointment_ui_procedures_incomes_procedure_name', {url: '/obtemProcedimentos/?query=%25', urlEnd: '%25'}, {minChars: 2, data: function(rec, input) { return { label: (rec['label_of_procedure_name'] || '').replace('<p>', '<p >')+'<p>'+(rec['procedure_price'] || ''), value: rec['procedure_name'] }; }, filter: AwesompleteUtil.filterContains});
    </script>")
  end

  defp procedure_name_updated(value) do
    tag(:input, type: :text, class: "form-control", name: "appointment_ui[procedures_incomes][procedure_name]", value: value, disabled: :disabled)
  end

  defp create_procedure_label_error(error_message) do
    content_tag :div, class: "text-right make-it-50-50" do
      [label :appointment_ui_procedures_incomes, :procedure_name, class: "control-label text-danger" do "#{error_message}" end]
    end
  end

  defp create_procedure_label() do
    label :appointment_ui_procedures_incomes, :procedure_name, class: "control-label make-it-50-50" do "Procedimento" end
  end

  defp procedure_id() do
    content_tag :div do
      [
        tag(:input, type: :hidden, id: "appointment_ui_procedures_incomes_procedure_id", 
        name: "appointment_ui[procedures_incomes][procedure_id]")
      ]
    end
  end

  defp procedure_id_value(value) do
    content_tag :div do
    [
      tag(:input, type: :hidden, id: "appointment_ui_procedures_incomes_procedure_id", 
      name: "appointment_ui[procedures_incomes][procedure_id]",
      value: value)
    ]
    end
  end

end