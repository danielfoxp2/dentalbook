defmodule DentalBookPresenter.Router do
  use DentalBookPresenter.Web, :router
  use Coherence.Router 

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug DentalBookPresenter.PaymentStatusPlug
    plug Coherence.Authentication.Session
  end

  pipeline :protected do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Coherence.Authentication.Session, protected: true 
  end

  pipeline :zircos_layout do
    plug :put_layout, {DentalBookPresenter.ZircosLayoutView, "zircos_layout.html"}
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/" do
    pipe_through :browser
    coherence_routes()
  end

  scope "/" do
    pipe_through :protected
    coherence_routes :protected
  end


  scope "/", DentalBookPresenter do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/pagamentodeplano", PaymentController, :index
    get "/redirect", PaymentController, :go_to_login_page
  end

  scope "/", DentalBookPresenter do
    pipe_through [:protected, :zircos_layout] # Use the default browser stack

    get "/login", PageController, :index
    get "/agendamentos", AttendanceController, :index
    resources "/planosdetratamento", AppointmentController do
      post "/orcamento", AppointmentController, :print_dental_invoice, as: :dental_invoice
    end
    resources "/pacientes", PatientController do
      get "/anamnese", PatientController, :new_anamnesis, as: :anamnesis
      get "/anamneserespondida/:anamnesis_id", PatientController, :show_anamnesis, as: :anamnesis
      post "/anamnese", PatientController, :save_anamnesis, as: :anamnesis
      post "/emitiranamnese", PatientController, :print_anamnesis, as: :anamnesis
      get "/atestado", SickNoteController, :index
      get "/visualizaratestado", SickNoteController, :show_sick_note
      post "/atestado", SickNoteController, :create_sick_note
      get "/receituario", PrescriptionController, :index
      get "/visualizarreceituario", PrescriptionController, :show_prescription
      post "/receituario", PrescriptionController, :create_prescription
    end
    resources "/procedimentos", ProcedureController
    resources "/receitas", IncomeController
    resources "/despesas", ExpenseController
    resources "/colaboradores", TeamController
    resources "/modelodeatestado", SickNoteTemplateController
    resources "/medicamentos", MedicineController
    get "/fluxodecaixa", FinancialController, :index
    post "/fluxodecaixa", FinancialController, :get_custom_financial
    get "/obtemPacientes", PatientController, :get_patients
    get "/obtemProcedimentos", ProcedureController, :get_procedures
    get "/obtemProfissionaisDeSaude", TeamController, :get_dentists
    get "/receitasporprocedimentos", GroupedIncomeController, :index
    post "/receitasporprocedimentosfiltrados", GroupedIncomeController, :filter
    get "/filtramedicamentos", MedicineController, :get_medicines
   end

  # Other scopes may use custom stacks.
  scope "/api", DentalBookPresenter do
    pipe_through :api

    post "/confirmarassinatura", PaymentController, :signature_confirmation
    post "/atualizarassinatura", PaymentController, :signature_update
  end
end
