defmodule DentalBookPresenter.AppointmentObservation do
  use DentalBookPresenter.Web, :model

  @primary_key false
  schema "appointment_observations" do
    field :appointment_id, :integer, primary_key: true
    field :observation_id, :integer, primary_key: true
    field :observation, :string
    field :creation_date, :string
    field :creation_hour, :string
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:appointment_id, :observation_id, :observation, :creation_date, :creation_hour])
    |> validate_required([:appointment_id, :observation_id, :observation, :creation_date, :creation_hour])
  end

end