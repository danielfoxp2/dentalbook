defmodule DentalBookPresenter.Appointment do
  use DentalBookPresenter.Web, :model

  schema "appointments" do
  
    belongs_to :patient, DentalBookPresenter.Patient
    many_to_many :procedures, DentalBookPresenter.Procedure, join_through: DentalBookPresenter.AppointmentProcedure
    field :dentist_id, :integer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do

    struct
    |> cast(params, [:dentist_id])
  end
end
