defmodule DentalBookPresenter.AccountPlan do
  use DentalBookPresenter.Web, :model

  schema "account_plans" do
    field :description, :string
    field :type, :string
    field :price, Money.Ecto.Type
    field :start_day, :utc_datetime
    field :end_day, :utc_datetime
    field :duration, :integer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:description, :type, :price, :start_day, :end_day])
  end

  def without_support() do
    [147504, 147506, 290334]
  end
end