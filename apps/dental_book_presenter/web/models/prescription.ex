defmodule DentalBookPresenter.Prescription do
  use DentalBookPresenter.Web, :model

  schema "prescriptions" do
    field :patient_id, :integer
    field :patient_name, :string
    field :dentist_name, :string
    field :dentist_cro, :string
    field :prescription_content, :map

    timestamps(type: :utc_datetime)
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:patient_id, :patient_name, :dentist_name, :dentist_cro, :prescription_content])
    |> validate_required([:patient_id, :patient_name, :dentist_name, :dentist_cro, :prescription_content])
  end
end
