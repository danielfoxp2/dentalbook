defmodule DentalBookPresenter.Company do
  use DentalBookPresenter.Web, :model

  schema "companies" do
    field :name, :string
    field :account_plan_id, :integer
    field :cpf_cnpj, :string
    field :partner_id, :integer
    
    many_to_many :users, DentalBookPresenter.User, join_through: DentalBookPresenter.CompanyUsers

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :account_plan_id, :cpf_cnpj, :partner_id])
    |> validate_required([:name, :account_plan_id])
  end
end
