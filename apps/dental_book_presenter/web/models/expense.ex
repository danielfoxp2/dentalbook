defmodule DentalBookPresenter.Expense do
  use DentalBookPresenter.Web, :model
  alias DentalBookPresenter.DateFormat

  schema "expenses" do
    field :description, :string
    field :value, Money.Ecto.Type
    field :due_date_at, Ecto.Date
    field :paid_at, Ecto.Date

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    params = format_values_of(params)

    struct
    |> cast(params, [:description, :value, :due_date_at, :paid_at])
    |> validate_required([:description, :value, :due_date_at])
    |> validate_paid_at(:paid_at)
  end

  defp format_values_of(params) do
    params 
    |> DateFormat.pre_cast_value_of("due_date_at")
    |> DateFormat.pre_cast_value_of("paid_at")
  end

  defp validate_paid_at(changeset, field) do
    paid_at = changeset.changes[field]

    compare_date_with_current_date(paid_at)
    |> deal_with_changeset(changeset)
  end

  defp compare_date_with_current_date(paid_at) when is_nil(paid_at), do: :blank
  defp compare_date_with_current_date(paid_at) do
    today = DateFormat.now
    Ecto.Date.compare(paid_at, today)
  end

  defp deal_with_changeset(:gt, %{errors: errors} = changeset) do
    new_errors = [paid_at: {"Não pode ser maior que a data atual", [validation: :validate_paid_at]}]
    %{changeset | errors: new_errors ++ errors, valid?: false}
  end
  defp deal_with_changeset(_, changeset), do: changeset

end
