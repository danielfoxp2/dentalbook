defmodule DentalBookPresenter.Anamnesis do
  use DentalBookPresenter.Web, :model
  alias DentalBookPresenter.DateFormat

  schema "anamnesis" do
    field :responses, :map

    timestamps(type: :utc_datetime)
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  fragment("responses ->> patient_id @> ?, ^patient_id)
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:responses])
  end
end