defmodule DentalBookPresenter.Medicine do
  use DentalBookPresenter.Web, :model

  schema "medicines" do
    field :use_type, :string
    field :description, :string
    field :default_prescription, :string
    field :controlled, :boolean, default: false

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:use_type, :description, :default_prescription, :controlled])
    |> validate_required([:use_type, :description, :default_prescription, :controlled])
  end
end
