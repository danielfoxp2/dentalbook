defmodule DentalBookPresenter.Procedure do
  use DentalBookPresenter.Web, :model
  
  schema "procedures" do
    field :name, :string
    field :price, Money.Ecto.Type

    many_to_many :appointments, DentalBookPresenter.Appointment, join_through: DentalBookPresenter.AppointmentProcedure

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :price])
    |> validate_required([:name, :price])
  end
end
