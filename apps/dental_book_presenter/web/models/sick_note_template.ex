defmodule DentalBookPresenter.SickNoteTemplate do
  use DentalBookPresenter.Web, :model

  schema "sick_note_templates" do
    field :title, :string
    field :content, :string

    timestamps(type: :utc_datetime)
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :content])
    |> validate_required([:title, :content])
  end
end
