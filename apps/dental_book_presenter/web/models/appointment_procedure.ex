defmodule DentalBookPresenter.AppointmentProcedure do
  use DentalBookPresenter.Web, :model
  alias DentalBookPresenter.DateFormat
  alias DentalBookPresenter.TimeFormat

  schema "appointment_procedures" do
    belongs_to :appointment, DentalBookPresenter.Appointment
    belongs_to :procedure, DentalBookPresenter.Procedure
    field :attendance_date_at, Ecto.Date
    field :attendance_hour, Ecto.Time
    field :done, :boolean
    field :procedure_tooth, :string
    field :procedure_tooth_faces, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    params = DateFormat.pre_cast_value_of(params, :attendance_date_at) |> TimeFormat.pre_cast_value_of

    struct
    |> cast(params, [:appointment_id, :procedure_id, :attendance_date_at, :attendance_hour, :done, :procedure_tooth, :procedure_tooth_faces])
    |> validate_required([:appointment_id, :procedure_id, :procedure_tooth, :attendance_date_at, :attendance_hour])
  end

  def modal_changeset(struct, params \\ %{}) do
    params = DateFormat.pre_cast_value_of(params, :attendance_date_at) |> TimeFormat.pre_cast_value_of

    struct
    |> cast(params, [:procedure_id, :procedure_tooth, :attendance_date_at, :attendance_hour])
    |> validate_required([:procedure_id, :procedure_tooth, :attendance_date_at, :attendance_hour])
    |> validate_procedure_tooth
  end

  def validate_procedure_tooth(changeset) do
    {_, procedure_tooth} = fetch_field(changeset, :procedure_tooth)

    exists(procedure_tooth)
    |> mount_changeset_errors(changeset)
  end

  defp exists(procedure_tooth) when procedure_tooth == "", do: false
  defp exists(_procedure_tooth), do: true

  defp mount_changeset_errors(is_valid, and_this_changeset) when is_valid == true, do: and_this_changeset
  defp mount_changeset_errors(is_valid, and_this_changeset) do
    %{errors: concatenating_this_errors} = and_this_changeset
    new_errors = [procedure_tooth: {"Dente inválido", [validation: "validate_procedure_tooth"]}]
    %{and_this_changeset | errors: new_errors ++ concatenating_this_errors, valid?: false}
  end

end
