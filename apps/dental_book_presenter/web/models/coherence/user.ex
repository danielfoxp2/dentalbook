defmodule DentalBookPresenter.User do
  use DentalBookPresenter.Web, :model
  use Coherence.Schema

  schema "users" do
    field :name, :string
    field :email, :string
    field :admin, :boolean, default: true
    field :role, :string, default: "Profissional de Saúde"
    field :cro, :string
    field :cellphone, :string

    coherence_schema()

    many_to_many :companies, DentalBookPresenter.Company, join_through: DentalBookPresenter.CompanyUser
    timestamps()
  end

  def changeset(model, params \\ %{}) do
    model
    |> cast(params, [:name, :email, :admin, :role, :cellphone, :cro] ++ coherence_fields())
    |> validate_required([:name, :email, :role])
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
    |> validate_coherence(params)
  end
end
