defmodule DentalBookPresenter.Patient do
  use DentalBookPresenter.Web, :model
  alias DentalBookPresenter.DateFormat

  schema "patients" do
    field :name, :string
    field :cellphone, :string
    field :gender, :string
    field :birth_date, Ecto.Date
    field :age, :integer
    field :cpf, :string
    field :origin, :string

    has_many :appointments, DentalBookPresenter.Appointment
    
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    params = format_values_of(params)

    struct
    |> cast(params, [:name, :cellphone, :gender, :birth_date, :age, :cpf, :origin])
    |> validate_required([:name, :cellphone, :gender, :birth_date])
    |> validate_birth_date
  end

  defp format_values_of(params) do
    params |> DateFormat.pre_cast_value_of("birth_date")
  end

  defp validate_birth_date(changeset) do
    birth_date = changeset.changes[:birth_date]

    compare_date_with_current_date(birth_date)
    |> deal_with_changeset_of_dates(changeset, :birth_date)
  end

  defp compare_date_with_current_date(birth_date) when is_nil(birth_date), do: :blank
  defp compare_date_with_current_date(birth_date) do
    today = DateFormat.now
    Ecto.Date.compare(birth_date, today)
  end

  defp deal_with_changeset_of_dates(:gt, changeset, field), do: mount_changeset_errors_of_the(field, changeset)
  defp deal_with_changeset_of_dates(_, changeset, _), do: changeset

  defp mount_changeset_errors_of_the(field, and_this_changeset) do
    %{errors: concatenating_this_errors} = and_this_changeset
    new_errors = ["#{field}": {"Não pode ser maior que a data atual", [validation: "validate_#{field}"]}]
    %{and_this_changeset | errors: new_errors ++ concatenating_this_errors, valid?: false}
  end

end
