defmodule DentalBookPresenter.PartnerProspect do
  use DentalBookPresenter.Web, :model

  @primary_key false
  schema "partners_prospects" do
    field :prospect_ip, :string
    field :partner_key, :string

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:prospect_ip, :partner_key])
    |> validate_required([:prospect_ip, :partner_key])
  end

end