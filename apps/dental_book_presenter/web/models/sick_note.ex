defmodule DentalBookPresenter.SickNote do
  use DentalBookPresenter.Web, :model

  schema "sick_notes" do
    field :patient_id, :integer, primary_key: true
    field :patient_name, :string
    field :dentist_name, :string
    field :dentist_cro, :string
    field :sick_note_content, :string

    timestamps(type: :utc_datetime)
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:patient_id, :patient_name, :dentist_name, :dentist_cro, :sick_note_content])
    |> validate_required([:patient_id, :patient_name, :dentist_name, :dentist_cro, :sick_note_content])
  end
end
