defmodule DentalBookPresenter.Financial do
  use DentalBookPresenter.Web, :model
  alias DentalBookPresenter.DateFormat

  schema "financials" do
    field :custom_begin_date, Ecto.Date
    field :custom_end_date, Ecto.Date
  end

  def changeset(struct, params \\ %{}) do
    params = format_values_of(params)

    struct
    |> cast(params, [:custom_begin_date, :custom_end_date])
    |> validate_required([:custom_begin_date, :custom_end_date])
    |> validate_interval_of_period
  end

  defp format_values_of(params) do
    params
    |> DateFormat.pre_cast_value_of("custom_begin_date")
    |> DateFormat.pre_cast_value_of("custom_end_date")
  end

  defp validate_interval_of_period(%Ecto.Changeset{valid?: is_valid} = changeset) when is_valid == false do
    "A data inicial e a data final devem ser informadas"
  end 

  defp validate_interval_of_period(changeset) do
    {_, begin_date} = fetch_field(changeset, :custom_begin_date)
    {_, end_date} = fetch_field(changeset, :custom_end_date)

    compare_dates(begin_date, end_date)
    |> get_error_message
    |> deal_with_changeset_of_period(:custom_begin_date, changeset)
  end

  defp compare_dates(first_date, second_date) do
    Ecto.Date.compare(first_date, second_date)  
  end

  defp get_error_message(:gt), do: "A data inicial não pode ser maior que a data final"
  defp get_error_message(_), do: :valid

  defp deal_with_changeset_of_period(:valid, _field, changeset) do
    {_, begin_date} = fetch_field(changeset, :custom_begin_date)
    {_, end_date} = fetch_field(changeset, :custom_end_date)

    {begin_date, end_date}
  end 
  defp deal_with_changeset_of_period(error_message, field, changeset), do: error_message

end