defmodule DentalBookPresenter.MonthlyCompanyPayment do
  use DentalBookPresenter.Web, :model
  
  @primary_key false
  schema "monthly_company_payments" do
    field :company_id, :integer
    field :account_plan_id, :integer
    field :start_access, :utc_datetime
    field :end_access, :utc_datetime
    field :paid_value, Money.Ecto.Type
    field :pay_day, :utc_datetime

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:company_id, :account_plan_id, :start_access, :end_access, :paid_value, :pay_day])
    |> validate_required([:company_id, :account_plan_id, :start_access, :end_access, :paid_value, :pay_day])
    |> foreign_key_constraint(:account_plan_id)
  end

end