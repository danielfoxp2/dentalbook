defmodule DentalBookPresenter.Income do
  use DentalBookPresenter.Web, :model
  alias DentalBookPresenter.DateFormat

  schema "incomes" do
    field :description, :string
    field :created_at, Ecto.Date
    field :due_date_at, Ecto.Date
    field :paid_at, Ecto.Date
    field :value, Money.Ecto.Type
    field :appointment_procedure_id, :integer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    params = format_values_of(params)
    
    struct
    |> cast(params, [:description, :created_at, :due_date_at, :paid_at, :value, :appointment_procedure_id])
    |> validate_required([:description, :created_at, :due_date_at, :value])
    |> validate_paid_at(:paid_at)
    |> dates_cannot_be_lesser_than_creation_date_of_income(:due_date_at)
    |> dates_cannot_be_lesser_than_creation_date_of_income(:paid_at)
  end

  def modal_changeset(struct, params \\ %{}) do
    params = format_values_of(params)
    
    struct
    |> cast(params, [:created_at, :due_date_at, :paid_at, :value])
    |> validate_required([:created_at, :due_date_at, :value])
    |> validate_paid_at(:paid_at)
    |> dates_cannot_be_lesser_than_creation_date_of_income(:due_date_at)
    |> dates_cannot_be_lesser_than_creation_date_of_income(:paid_at)
  end

  defp format_values_of(params) do 
    params
    |> DateFormat.pre_cast_value_of("created_at")
    |> DateFormat.pre_cast_value_of("due_date_at")
    |> DateFormat.pre_cast_value_of("paid_at")
  end

  defp validate_paid_at(changeset, field) do
    paid_at = changeset.changes[field]

    compare_date_with_current_date(paid_at)
    |> mount_error_atom
    |> deal_with_changeset_of_dates(changeset, field)
  end

  defp mount_error_atom(:gt), do: :invalid
  defp mount_error_atom(_), do: :ok

  defp compare_date_with_current_date(paid_at) when is_nil(paid_at), do: :blank
  defp compare_date_with_current_date(paid_at) do
    today = DateFormat.now
    Ecto.Date.compare(paid_at, today)
  end

  defp dates_cannot_be_lesser_than_creation_date_of_income(changeset, field) do
    {_, created_at} = fetch_field(changeset, :created_at)
    {_, date_to_compare} = fetch_field(changeset, field)
    compare_date_with_created_date(date_to_compare, created_at)
    |> deal_with_changeset_of_dates(changeset, field)
  end

  defp compare_date_with_created_date(date_to_compare, created_at) when is_nil(date_to_compare) or is_nil(created_at), do: :blank
  defp compare_date_with_created_date(date_to_compare, created_at) do
    Ecto.Date.compare(date_to_compare, created_at)
  end

  defp deal_with_changeset_of_dates(:invalid, changeset, _), do: mount_changeset_errors_of_the(:paid_at, :gt, changeset)
  defp deal_with_changeset_of_dates(:lt, changeset, field), do: mount_changeset_errors_of_the(field, :lt, changeset)
  defp deal_with_changeset_of_dates(_, changeset, _), do: changeset

  defp mount_changeset_errors_of_the(field, for_this_error_type, and_this_changeset) do
    %{errors: concatenating_this_errors} = and_this_changeset
    new_errors = ["#{field}": {get_error_message(for_this_error_type), [validation: "validate_#{field}"]}]
    %{and_this_changeset | errors: new_errors ++ concatenating_this_errors, valid?: false}
  end

  defp get_error_message(:gt), do: "Não pode ser maior que a data atual"
  defp get_error_message(:lt), do: "Não pode ser menor que a data de criação"
  
end
