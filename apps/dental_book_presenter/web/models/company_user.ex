defmodule DentalBookPresenter.CompanyUser do
  use DentalBookPresenter.Web, :model

  @primary_key false
  schema "company_users" do
    belongs_to :user, DentalBookPresenter.User, primary_key: true
    belongs_to :company, DentalBookPresenter.Company, primary_key: true
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:company_id, :user_id])
    |> validate_required([:company_id, :user_id])
  end
end
