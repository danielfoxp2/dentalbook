defmodule DentalBookPresenter.FinancialView do
  use DentalBookPresenter.Web, :view

  defdelegate get_title(), to: DentalBookPresenter.OnboardMessages
  defdelegate get_use_instructions(), to: DentalBookPresenter.OnboardMessages, as: :get_cash_flow_instructions
  defdelegate get_icon_type(), to: DentalBookPresenter.OnboardMessages

  def create_date_picker(form, field) do
    content_tag :div, class: "input-group col-lg-12" do
    [
      text_input(form, field, class: "form-control financial_date", placeholder: "dd/mm/aaaa")
    ]
    end
  end

  def status_of(result) do
    result_is_negative = result |> String.contains?("-")
    if (result_is_negative) do
      "tilebox-danger"
    else
      "tilebox-primary"
    end
  end

end