defmodule DentalBookPresenter.TeamView do
  use DentalBookPresenter.Web, :view
  use Rummage.Phoenix.View
  alias DentalBookPresenter.ValidationI18n
  alias DentalBookPresenter.TagAttributes
  alias DentalBookPresenter.PageTitle

  def create_admin_field(team_form, current_user) do
    get_admin_value(team_form.data.id, team_form.data.admin)
    |> admin_field(current_user)
  end

  def get_label_from(action_type), do: PageTitle.get_label_from(action_type)

  defdelegate not_exists?(team), to: DentalBookPresenter.OnboardValidation
  defdelegate get_title(), to: DentalBookPresenter.OnboardMessages
  defdelegate get_use_instructions(), to: DentalBookPresenter.OnboardMessages, as: :get_team_instructions
  defdelegate get_icon_type(), to: DentalBookPresenter.OnboardMessages

  defp get_admin_value(id, _value) when id == nil, do: "false"
  defp get_admin_value(id, value), do: value
    
  defp admin_field(value, current_user) do
    tag_attributes_hidden = %TagAttributes{consider_id: false, field_name: "admin", field_type: "hidden", field_value: "false"}
    tag_attributes_checkbox = %TagAttributes{field_name: "admin", field_type: "checkbox", field_value: value}
  
    raw(
    "<input #{configure_tag_with(tag_attributes_hidden)} >
     <input #{configure_tag_with(tag_attributes_checkbox)} #{has_permission?(current_user.admin)} class=\"done\" switch=\"info\">
     #{configure_tag_label(tag_attributes_checkbox)}")
  end

  defp has_permission?(is_admin) when is_admin == true, do: ""
  defp has_permission?(_is_admin), do: "disabled=\"disabled\""

  defp configure_tag_label(tag_attributes_checkbox) do
    label_for = procedure_income_tag_id(tag_attributes_checkbox) |> String.replace("id=", "")
    {_, tag_label} = raw("<label for=#{label_for} data-on-label=\"Sim\" data-off-label=\"Não\" class=\"m-b-0 c-m-t\"></label>")

    tag_label
  end

  defp configure_tag_with(tag_attributes) do
    procedure_income_tag_id(tag_attributes) 
    |> procedure_income_tag_name(tag_attributes) 
    |> procedure_income_tag_type(tag_attributes)
    |> procedure_income_tag_value(tag_attributes)
    |> procedure_income_tag_class(tag_attributes)
  end

  defp procedure_income_tag_id(%TagAttributes{consider_id: false} = params), do: ""
  defp procedure_income_tag_id(%TagAttributes{consider_id: consider_id} = params) when consider_id == true do
    "id=\"registration_#{params.field_name}\""
  end

  defp procedure_income_tag_name(previous_attributes, %TagAttributes{} = params) do
    previous_attributes <> "name=\"registration[#{params.field_name}]\""
  end

  defp procedure_income_tag_type(previous_attributes, %TagAttributes{} = params) do
    previous_attributes <> "type=\"" <> params.field_type <> "\""
  end

  defp procedure_income_tag_value(previous_attributes, %TagAttributes{field_value: field_value} = params) 
  when field_value == nil do previous_attributes end

  defp procedure_income_tag_value(previous_attributes, %TagAttributes{field_value: field_value} = params) 
  when field_value != nil do
    "#{previous_attributes}value=\"#{field_value}\""
  end

  defp procedure_income_tag_class(previous_attributes, %TagAttributes{field_class: field_class} = params)
  when field_class == nil do previous_attributes end

  defp procedure_income_tag_class(previous_attributes, %TagAttributes{field_class: field_class} = params)
  when field_class != nil do
    previous_attributes <> "class=\"" <> field_class <> "\""
  end

end