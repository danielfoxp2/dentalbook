defmodule DentalBookPresenter.AttendanceView do
  use DentalBookPresenter.Web, :view

  defdelegate not_exists?(attendances), to: DentalBookPresenter.OnboardValidation
  defdelegate get_title(), to: DentalBookPresenter.OnboardMessages
  defdelegate get_use_instructions(), to: DentalBookPresenter.OnboardMessages, as: :get_attendance_instructions
  defdelegate get_icon_type(), to: DentalBookPresenter.OnboardMessages
  
  def create_scheduled_attendances(scheduled_patients, conn, put_date? \\ true)
  def create_scheduled_attendances([_|_] = scheduled_patients, conn, put_date?) do
    Enum.map scheduled_patients, fn scheduled_attendance ->
      content_tag :a, href: appointment_path(conn, :edit, scheduled_attendance.appointment_id) do
        get_attendance_body(scheduled_attendance, put_date?)
      end
    end
  end
  def create_scheduled_attendances([], _conn, _put_date?) do
    content_tag :div, style: "text-align:center" do
    [
      content_tag(:div,"", class: "swal-icon swal-icon--info"),
      content_tag(:div, "Não existem pacientes agendados", class: "swal-title"),
      content_tag(:div, "Mais informações no botão \"Instruções de uso\".", class: "swal-text")
    ]
    end
  end

  defp get_attendance_body(scheduled_attendance, put_date?) do
    content_tag :div, class: "inbox-item" do
    [
      content_tag(:p, scheduled_attendance.patient_name, class: "inbox-item-author"),
      content_tag(:p, get_attendance_date_with_hour(scheduled_attendance, put_date?), class: "inbox-item-text"),
      content_tag(:p, scheduled_attendance.patient_cellphone, class: "inbox-item-date")
    ]
    end
  end

  defp get_attendance_date_with_hour(scheduled_attendance, put_date?) when put_date? == false, do: "#{scheduled_attendance.attendance_hour}"
  defp get_attendance_date_with_hour(scheduled_attendance, _put_date?) do
    "#{scheduled_attendance.attendance_date} - #{scheduled_attendance.attendance_hour}"
  end

end