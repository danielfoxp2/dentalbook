defmodule DentalBookPresenter.PaymentView do
  use DentalBookPresenter.Coherence.Web, :view
  
  def get_encryption_key(), do: System.get_env("EK_LIVE")
end