defmodule Coherence.SessionView do
  use DentalBookPresenter.Coherence.Web, :view
  alias DentalBookPresenter.ValidationI18n

  def show_failed_auth_if_needed(message) do
    get_translated(message)
  end

  def get_translated(message) when message == "", do: ""
  def get_translated(message) when is_nil(message), do: ""
  def get_translated(message), do: Gettext.dgettext(DentalBookPresenter.Gettext, "coherence", message)

end
