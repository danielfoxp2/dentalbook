defmodule DentalBookPresenter.SickNoteView do
  use DentalBookPresenter.Web, :view

  alias DentalBookPresenter.DentistSearch

  defdelegate create_dentist_name_from(dentist, form, conn), to: DentistSearch, as: :make

  def create_template_select(form, sick_notes_templates) do
    sick_notes_identifiers = get_only_indetifiers_of(sick_notes_templates)
    select form, :template_id, sick_notes_identifiers, class: "form-control selectpicker show-tick"
  end

  defp get_only_indetifiers_of(sick_notes_templates) do
    sick_notes_templates
    |> Enum.map(&get_indentifiers_of(&1))
    |> Enum.into([[value: "0", key: "Selecione um modelo de atestado"]])
  end

  defp get_indentifiers_of(sick_note_template) do
    [value: sick_note_template.id, key: sick_note_template.title]
  end
  
end