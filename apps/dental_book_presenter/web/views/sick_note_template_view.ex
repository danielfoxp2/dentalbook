defmodule DentalBookPresenter.SickNoteTemplateView do
  use DentalBookPresenter.Web, :view

  defdelegate not_exists?(sick_note_templates), to: DentalBookPresenter.OnboardValidation
  defdelegate get_title(), to: DentalBookPresenter.OnboardMessages
  defdelegate get_use_instructions(), to: DentalBookPresenter.OnboardMessages, as: :get_sick_note_template_instructions
  defdelegate get_icon_type(), to: DentalBookPresenter.OnboardMessages

end
