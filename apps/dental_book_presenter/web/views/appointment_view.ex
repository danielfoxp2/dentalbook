defmodule DentalBookPresenter.AppointmentView do
  use DentalBookPresenter.Web, :view
  use Rummage.Phoenix.View
  alias DentalBookPresenter.AppointmentCard
  alias DentalBookPresenter.AppointmentHiddenCard
  alias DentalBookPresenter.AppointmentUpdatableCard
  alias DentalBookPresenter.AppointmentObservationPanel
  alias DentalBookPresenter.DentistSearch

  def create_procedure_incomes(appointment_form) do
    inputs_for appointment_form, :procedures_incomes, fn ps ->
      content_tag :div, class: "#{ps.id} col-md-4" do
        [mount_card_elements(ps) |> AppointmentCard.make]
      end
    end
  end

  def create_hidden_procedures_incomes(appointment_form) do
    inputs_for appointment_form, :procedures_incomes, fn ps ->
      content_tag :div, class: "#{ps.id}" do
        [mount_card_elements(ps) |> AppointmentHiddenCard.make]
      end
    end
  end

  def create_updatable_procedures_incomes(appointment_form) do
    inputs_for appointment_form, :procedures_incomes, fn ps ->
      content_tag :div, id: ps.id, class: "#{ps.id} updatable" do
        [AppointmentUpdatableCard.make(ps)]
      end
    end
  end

  def configure_id_of(%{patient_id: patient_id}, dentist, form) do
    [hidden_input(form, :patient_id, value: patient_id), hidden_input(form, :dentist_id, value: get_dentist_id(dentist))]
  end

  def configure_id_of(_, dentist, form) do
    [hidden_input(form, :patient_id), hidden_input(form, :dentist_id, value: get_dentist_id(dentist))]
  end

  def create_patient_name_from(%{patient_name: patient_name}, form, conn) do
    text_input(form, :patient_name, class: "form-control", disabled: :disabled, value: patient_name)
  end

  def create_patient_name_from(_, form, conn) do
    awesomplete_parameters = get_awesomplete_parameters(conn)
    awesomplete(form, :patient_name, [class: "form-control"], awesomplete_parameters)
  end

  def create_patient_cellphone_from(%{patient_cellphone: patient_cellphone}, form) do
    create_patient_cellphone_with(patient_cellphone, form)
  end

  def create_patient_cellphone_from(_, form) do
    create_patient_cellphone_with("", form)
  end

  def create_patient_cellphone_with(patient_cellphone, form) do
    text_input(form, :patient_cellphone, class: "form-control", disabled: :disabled, value: patient_cellphone) 
  end

  defdelegate create_dentist_name_from(dentist, form, conn), to: DentistSearch, as: :make

  def create_observation_field() do
    content_tag :div do
      content_tag(:textarea, "", class: "form-control", id: "observation_field", rows: "2", style: "min-height: auto;margin-top: 30px;", placeholder: "Insira suas observações aqui")
    end
  end

  def show_existing_observations(appointment_form) do
    inputs_for appointment_form, :observations, fn obs ->
      mount_observation_elements(obs) |> AppointmentObservationPanel.make
    end
  end

  def show_that_appointment_is_empty() do
    content_tag :div, style: "text-align:center" do
    [
      content_tag(:div,"", class: "swal-icon swal-icon--info"),
      content_tag(:div, "Ainda não existem planos de tratamentos cadastrados", class: "swal-title"),
      content_tag(:div, "Mais informações no botão \"Instruções de uso\".", class: "swal-text")
    ]
    end
  end

  defdelegate not_exists?(appointments), to: DentalBookPresenter.OnboardValidation
  defdelegate get_title(), to: DentalBookPresenter.OnboardMessages
  defdelegate get_use_instructions(), to: DentalBookPresenter.OnboardMessages, as: :get_appointment_instructions
  defdelegate get_creation_appointment_instructions(), to: DentalBookPresenter.OnboardMessages
  defdelegate get_icon_type(), to: DentalBookPresenter.OnboardMessages

  defp get_dentist_id(dentist) when is_nil(dentist), do: ""
  defp get_dentist_id(dentist), do: dentist.id

  defp mount_card_elements(ps) do
    %{
      id: ps.id,
      name: ps.name,
      procedure_name: ps.data.procedure_name,
      attendance_date_at: ps.data.attendance_date_at,
      attendance_hour: ps.data.attendance_hour,
      price: ps.data.price,
      done: ps.data.done,
      procedure_tooth: ps.data.procedure_tooth,
      procedure_tooth_faces: ps.data.procedure_tooth_faces,
      due_date_at: ps.data.due_date_at,
      paid_at: ps.data.paid_at,
      procedure_id: ps.data.procedure_id
    }
  end

  defp mount_observation_elements(obs) do
    %{
      observation_id: obs.data.observation_id,
      observation: obs.data.observation,
      creation_date: obs.data.creation_date,
      creation_hour: obs.data.creation_hour
    }
  end

  defp get_awesomplete_parameters(conn) do
    %{
      url: patient_path(conn, :get_patients) <> "/?query=%25",
      urlEnd: "%25",
      filter: "AwesompleteUtil.filterContains",
      minChars: 2,
      label: "label_of_patient_name",
      value: "patient_name",
      assign: true,
      descr: "patient_cellphone"
    }
  end
end


