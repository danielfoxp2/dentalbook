defmodule DentalBookPresenter.ExpenseView do
  use DentalBookPresenter.Web, :view
  use Rummage.Phoenix.View
  alias DentalBookPresenter.PageTitle

  def create_date_picker(form, field) do
    content_tag :div, class: "input-group" do
      text_input(form, field, class: "form-control expense_ui_date", placeholder: "dd/mm/aaaa")
    end
  end

  def create_value(form, field) do
    text_input form, field, class: "form-control"
  end

  def get_label_from(action_type), do: PageTitle.get_label_from(action_type)

  defdelegate not_exists?(expenses), to: DentalBookPresenter.OnboardValidation
  defdelegate get_title(), to: DentalBookPresenter.OnboardMessages
  defdelegate get_use_instructions(), to: DentalBookPresenter.OnboardMessages, as: :get_expenses_instructions
  defdelegate get_icon_type(), to: DentalBookPresenter.OnboardMessages

  def show_that_expense_is_empty() do
    content_tag :div, style: "text-align:center" do
    [
      content_tag(:div,"", class: "swal-icon swal-icon--info"),
      content_tag(:div, "Ainda não existem despesas cadastradas", class: "swal-title"),
      content_tag(:div, "Mais informações no botão \"Instruções de uso\".", class: "swal-text")
    ]
    end
  end

end
