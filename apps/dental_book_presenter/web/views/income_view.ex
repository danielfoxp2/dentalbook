defmodule DentalBookPresenter.IncomeView do
  use DentalBookPresenter.Web, :view
  use Rummage.Phoenix.View
  alias Plug.Conn
  alias DentalBookPresenter.PageTitle

  def create_date_picker(form, field) do
    content_tag :div, class: "input-group" do
    [
      text_input(form, field, class: "form-control appointment_ui_date", placeholder: "dd/mm/aaaa")
    ]
    end
  end

  def create_value(form, field) do
    text_input form, field, class: "form-control autonumber", data: [a: [sep: "."]], data: [a: [dec: ","]] 
  end

  def get_label_from(action_type), do: PageTitle.get_label_from(action_type)

  defdelegate not_exists?(expenses), to: DentalBookPresenter.OnboardValidation
  defdelegate get_title(), to: DentalBookPresenter.OnboardMessages
  defdelegate get_use_instructions(), to: DentalBookPresenter.OnboardMessages, as: :get_income_instructions
  defdelegate get_icon_type(), to: DentalBookPresenter.OnboardMessages
  
  def get_patient_name_filter(conn), do: get("patient_name", conn)
  def get_income_description_filter(conn), do: get("income_description", conn)
  def get_start_date_filter(conn), do: get("start_date", conn)
  def get_end_date_filter(conn), do: get("end_date", conn)

  def get_search_option_status(field, conn) do
    get(field, conn) |> mount_filter_status
  end

  def alert_if_have_selected_filters(conn) do
    """
      #{get_patient_name_filter(conn)}
      #{get_income_description_filter(conn)}
      #{get_start_date_filter(conn)}
      #{get_end_date_filter(conn)}
      #{get_search_option_status("paid", conn)}
      #{get_search_option_status("not_paid", conn)}
      #{get_search_option_status("overdue", conn)}
    """
    |> String.trim
    |> get_alert_of
  end

  def show_that_income_is_empty() do
    content_tag :div, style: "text-align:center" do
      [
        content_tag(:div,"", class: "swal-icon swal-icon--info"),
        content_tag(:div, "Ainda não existem receitas cadastradas", class: "swal-title"),
        content_tag(:div, "Mais informações no botão \"Instruções de uso\".", class: "swal-text")
      ]
      end
  end

  defp get(filter, conn) do
    or_return_nothing = ""
    Conn.get_session(conn, :search_params)
    |> Map.get(filter, or_return_nothing)
  end

  defp mount_filter_status(field_content) when is_nil(field_content), do: ""
  defp mount_filter_status(field_content) when field_content == "", do: ""
  defp mount_filter_status(_field_content), do: "checked"

  defp get_alert_of(filters_selected) when filters_selected == "", do: ""
  defp get_alert_of(_filters_selected), do: "bounce"
end
