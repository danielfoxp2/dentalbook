defmodule DentalBookPresenter.MedicineView do
  use DentalBookPresenter.Web, :view
  use Rummage.Phoenix.View

  alias DentalBookPresenter.PageTitle
  alias DentalBookPresenter.SwitchButton
  alias DentalBookPresenter.TagAttributes

  defdelegate not_exists?(expenses), to: DentalBookPresenter.OnboardValidation
  defdelegate get_title(), to: DentalBookPresenter.OnboardMessages
  defdelegate get_use_instructions(), to: DentalBookPresenter.OnboardMessages, as: :get_medicines_instructions
  defdelegate get_icon_type(), to: DentalBookPresenter.OnboardMessages

  def get_label_from(action_type), do: PageTitle.get_label_from(action_type)

  def show_that_medicines_is_empty() do
    content_tag :div, style: "text-align:center" do
    [
      content_tag(:div,"", class: "swal-icon swal-icon--info"),
      content_tag(:div, "Ainda não existem medicamentos cadastrados", class: "swal-title"),
      content_tag(:div, "Mais informações no botão \"Instruções de uso\".", class: "swal-text")
    ]
    end
  end

  def create_use_type_field(form, input_name) do
    name = "medicine[#{input_name}]"
    yes_id = "medicine_#{input_name}_yes"
    no_id = "medicine_#{input_name}_no"

    content_tag :section do
      [
        create_option_question(form, name, yes_id, no_id)
      ]
    end
  end

  def get_controlled_value(true), do: "Sim"
  def get_controlled_value(false), do: "Não"

  def create_controlled_medicine_field(form, input_name) do
    value = form.data.controlled
    id = "medicine_#{input_name}"
    name = "medicine[#{input_name}]"

    tag_attributes_hidden = %TagAttributes{consider_id: false, field_name: name, field_type: "hidden", field_value: "false"}
    tag_attributes_checkbox = %TagAttributes{field_id: id, field_name: name, field_type: "checkbox", field_value: value, field_class: "done"}

    {tag_attributes_hidden, tag_attributes_checkbox}
    |> SwitchButton.make
  end 

  defp create_option_question(form, name, yes_id, no_id) do
    intern_value = "Uso Interno"
    extern_value = "Uso Externo"
    checked_value = get_checked_value_for(form.data.use_type, extern_value)

    [
      create_option(intern_value, name, yes_id, intern_value, !checked_value),
      create_option(extern_value, name, no_id, extern_value, checked_value)
    ]
  end

  defp get_checked_value_for(use_type, extern_value), do: use_type == extern_value 

  defp create_option(label_value, name, id, input_value, checked) do
    content_tag :div, class: "radio radio-success radio-inline" do
    [
      content_tag(:input, "", type: "radio", name: name, id: id, value: input_value, checked: checked),
      content_tag(:label, label_value, for: id)
    ]
    end
  end

end
