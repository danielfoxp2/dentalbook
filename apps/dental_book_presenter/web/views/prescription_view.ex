defmodule DentalBookPresenter.PrescriptionView do
  use DentalBookPresenter.Web, :view

  alias DentalBookPresenter.MedicineSearch
  alias DentalBookPresenter.StringToMap

  defdelegate create_medicine_searcher(form), to: MedicineSearch, as: :make

  def get_medicine_measures() do
    [
      "Frasco(s)",
      "Comprimido(s)", 
      "Ampola(s)", 
      "Caixa(s)",
      "Pacote(s)",
      "Tubo(s)",
      "Cápsula(s)"
    ]
  end

end
