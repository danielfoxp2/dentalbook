defmodule DentalBookPresenter.OnboardValidation do
  def not_exists?(expenses) when is_nil(expenses), do: true
  def not_exists?(expenses) when expenses == [], do: true
  def not_exists?(expenses), do: false
end