defmodule DentalBookPresenter.PageTitle do
  
  def get_label_from(:new), do: "Adicionar"
  def get_label_from(:edit), do: "Editar"    
end