defmodule DentalBookPresenter.OnboardMessages do
  use Phoenix.HTML

  def get_title(), do: "Instruções de uso"
  def get_use_instructions() do
    """
    Esta é uma instrução de uso sdfajkjk \
    kasdfjakjkkd sdfasdfsssssssssssssdsd \
    sdfasdfasdfa ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd fffffffffffffffffffffffffffffffffffffffffffff \
    """
  end
  def get_icon_type(), do: "success"

  def get_attendance_instructions() do
    """
    Esta funcionalidade apresenta os pacientes que tem planos de tratamento com procedimentos que \
    foram agendados para o dia de hoje e para os próximos 6 dias.\\n\\n \
    Supondo que hoje seja quarta-feira, e que existam planos de tratamento com agendamentos para quarta-feira, \
    quinta-feira, sexta-feira e terça-feira da próxima semana.\\n\\n \
    Na lista de agendamentos de hoje serão apresentados os agendamentos de quarta-feira e na lista dos próximos 6 dias \
    serão apresentados os agendamentos de quinta-feira, sexta-feira e terça-feira (da semana seguinte). \
    """
  end

  def get_appointment_instructions() do
    """
    Um plano de tratamento é onde se encontram todos os procedimentos realizados ou a realizar no tratamento de um paciente.\\n\\n \
    Serve para planejar e registrar os atendimentos feitos em cada paciente.\\n\\n \
    Para que um plano de tratamento possa ser criado é necessário que exista no mínimo um paciente e um procedimento cadastrados. \
    """
  end

  def get_creation_appointment_instructions() do
    """
    Um plano de tratamento precisa essencialmente ter o paciente, o dentista e pelo menos um agendamento associado. \\n\\n \
    Para associar um agendamento basta clicar no botão Novo Procedimento e preencher as informações solicitadas. \\n\\n \
    Após a inclusão dos procedimentos é possível emitir para o paciente um orçamento com a listagem do serviços \
    que serão realizados. \\n\\n \
    O dentista responsável pela realização do plano de tratamento pode fazer a inclusão de observações, na guia observações, \
    caso julgue necessário. \
    """
  end

  def get_patient_instructions() do
    """
    O cadastro de paciente é feito aqui. \
    Porém, muitas outras coisas são possíveis por meio dessa funcionalidade ao acessar o Histórico do paciente. \\n\\n \
    Histórico Financeiro: Pode-se ver todas as receitas pagas, em aberto e em atraso do paciente; \\n\\n \
    Histórico de Procedimentos: Com todos os procedimentos realizados até o momento; \\n\\n \
    Histórico de Anamnese: Onde é possível visualizar e criar anamneses para o paciente; \\n\\n \
    Os dados históricos, excetuando-se anamneses, são gerados por meio das informações dos planos de tratamento do paciente.\
    """
  end

  def get_patient_history_instructions() do
    """
    O histórico do paciente é composto por informações clínicas e financeiras.\\n\\n\
    No contexo clínico são apresentados todos os procedimentos que o paciente realizou, sendo possível acessar o plano \
    de tratamento vinculado.\\n\\n\
    Ainda no histórico clínico visualizam-se as anamneses realizadas no paciente e pode-se criar novas anamneses. \\n\\n\
    Já no contexto financeiro é discriminado todo o faturamento dos procedimentos que foram realizados ou pagos pelo paciente, \
    apresentando inclusive os débitos em atraso.\
    """
  end

  def get_procedures_instructions() do
    """
    Os procedimentos que são praticados pelo profissional de saúde são gerenciados nesta funcionalidade.\\n\\n\
    Aqui informam-se o nome do procedimento e o seu valor padrão.\\n\\n Esse valor é um valor de referência inicial e \
    pode ser alterado para cada procedimento agendado no momento da criação de um Plano de Tratamento, sem que o valor padrão \
    cadastrado aqui seja modificado.\
    """
  end

  def get_expenses_instructions() do
    """
    Em Despesas é possível gerenciar os custos operacionais do seu negócio. \\n\\n\
    Os lançamentos feitos aqui influenciam os demonstrativos financeiros do Fluxo de Caixa. \\n\\nSendo importante seu correto \
    preenchimento para que seja possível obter informações confiáveis sobre a saúde financeira do negócio. \
    """
  end

  def get_team_instructions() do
    """
    Os usuários do sistema são cadastrados nessa funcionalidade. Existem, até o momento, dois perfis profissionais: \
    Profissional de Saúde e Administrativo, por exemplo:\\n\\n\
    Profissional de saúde (Dentista, Técnico em Saúde Bucal etc.); \\n\\n\
    Administrativo (Secretária, Telefonista etc.); \\n\\n\
    Cada perfil pode ser configurado para ter privilégio de administrador ou não do sistema.\\n\\n \
    Um usuário admnistrador do sistema poderá visualizar e manipular os cadastros dos demais usuários sem restrições.\\n\\n \
    Mas um usuário sem privilégios de administrador pode visualizar e manipular seu próprio cadastro \
    e criar somente novos usuários não administradores. \
    """
  end

  def get_cash_flow_instructions() do
    """
    Aqui você visualiza os resultados financeiros do negócio de forma consolidada.\\n\\n\
    O fluxo de caixa apresenta somente o que de fato foi pago pelos clientes do negócio.\\n\\n\
    O faturamento apresentado aqui é afetado diretamente pelo que existe cadastrado nas \
    funcionalidades Receitas e Despesas. Sendo importante o preenchimento correto dessas funcionalidades \
    para que seja possível obter informações confiáveis sobre a saúde financeira do negócio. \
    """
  end

  def get_income_instructions() do
    """
    Aqui são lançadas receitas de dois tipos: Vinculada à um plano de tratamento e Avulsas.\\n\\n\
    Receitas vinculadas à um plano de tratamento são lançadas de forma automática nesta listagem sempre \
    que um procedimento é realizado ou pago na execução de um plano de tratamento.\\n\\n\
    As receitas avulsas são lançadas de forma manual e nunca são relacionadas, \
    via sistema, com um plano de tratamento.\\n\\n\
    É possível filtrar receitas por diversos critérios de busca diferentes, usando a opção Filtrar receitas. \\n\\n\
    Sempre que existir um critério de busca selecionado e aplicado, o botão Filtrar receitas sinalizará ao usuário do sistema \
    com um efeito de movimentação.\
    """
  end

  def get_sick_note_template_instructions() do
    """
    Aqui você poderá gerenciar modelos de emissão dos atestados de seus pacientes.\\n\\n\
    Com os modelos você cria um conteúdo padrão, não precisando escrever o mesmo texto toda vez \
    que entregar um atestado. \
    Contudo, o atestado sairá personalizado de acordo com os parâmetros informados no modelo.\\n\\n\
    Imagine que seu modelo esteja escrito: \\n\\n\
    Atesto que @paciente necessita de @dias de afastamento. \\n\\n\
    Então quando você emitir um atestado de 3 dias para o paciente Gilberto Veloso, o documento apresentará o seguinte texto:\\n\\n\
    Atesto que Gilberto Veloso necessita de 3 (três) dias de afastamento. \
    """
  end

  def get_medicines_instructions() do
    """
    Os medicamentos que são prescritos pelo profissional de saúde são gerenciados nesta funcionalidade. \\n\\n\
    Aqui informam-se a descrição do medicamento, sua posologia e demais características. \
    """
  end
end