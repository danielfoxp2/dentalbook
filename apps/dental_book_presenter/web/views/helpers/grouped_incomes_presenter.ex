defmodule DentalBookPresenter.GroupedIncomesPresenter do
  use Phoenix.HTML

  def show(grouped_incomes) do
    content_tag :div, class: "col-xs-12 col-md-8" do
      mount_panel(grouped_incomes)
    end
  end

  defp mount_panel({grouped_total_amount, grouped_incomes}) do
    content_tag :div, class: "panel panel-border panel-primary" do
    [
      mount_total_of(grouped_total_amount),
      mount_panel_body(grouped_incomes)
    ]
    end
  end

  defp mount_total_of(grouped_incomes_amount) do
    content_tag :div, class: "panel-heading" do
      content_tag(:h3, "Total: #{grouped_incomes_amount}", class: "panel-title")
    end
  end

  defp mount_panel_body(grouped_incomes) do
    content_tag :div, class: "panel-body slimscroll-alt", style: "min-height: 395px;" do
      mount_data_of(grouped_incomes)
    end
  end

  defp mount_data_of(grouped_incomes) do
    content_tag :div, class: "table-responsive" do
      content_tag :table, class: "table table-hover m-0" do
      [
        mount_table_head(),
        mount_table_body_with(grouped_incomes)
      ]
      end
    end
  end

  defp mount_table_head() do
    content_tag :thead do
      content_tag :tr do
      [
        content_tag(:th, ""),
        content_tag(:th, ""),
        content_tag(:th, "")
      ]
      end
    end
  end

  defp mount_table_body_with(grouped_incomes) do
    content_tag :tbody do
      Enum.map(grouped_incomes, fn grouped_income -> 
        mount(grouped_income)
      end)
    end
  end

  defp mount(grouped_income) do
    percentage_of_the_total = grouped_income.percentage_of_the_total |> format_percentage
    
    content_tag :tr do
      [
        content_tag(:td, grouped_income.procedure_name),
        content_tag(:td, grouped_income.incomes_amount, style: "text-align: right; color: #337ab7;"),
        content_tag(:td, "#{percentage_of_the_total}%", style: "text-align: right; font-weight: bold;")
      ]
    end
  end

  defp format_percentage(value) do
    splited_values = Float.to_string(value) |> String.split(".") 
    first_term = get_first_term_of(splited_values)
    second_term = get_second_term_of(splited_values)

    "#{first_term}.#{second_term}"
  end

  defp get_first_term_of(values), do: List.first(values)
  defp get_second_term_of(values), do: List.last(values) |> String.pad_trailing(2, "0")

end