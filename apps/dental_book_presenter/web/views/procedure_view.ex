defmodule DentalBookPresenter.ProcedureView do
  use DentalBookPresenter.Web, :view
  use Rummage.Phoenix.View
  alias DentalBookPresenter.PageTitle

  def get_label_from(action_type), do: PageTitle.get_label_from(action_type)

  defdelegate not_exists?(procedures), to: DentalBookPresenter.OnboardValidation
  defdelegate get_title(), to: DentalBookPresenter.OnboardMessages
  defdelegate get_use_instructions(), to: DentalBookPresenter.OnboardMessages, as: :get_procedures_instructions
  defdelegate get_icon_type(), to: DentalBookPresenter.OnboardMessages

  def show_that_procedure_is_empty() do
    content_tag :div, style: "text-align:center" do
      [
        content_tag(:div,"", class: "swal-icon swal-icon--info"),
        content_tag(:div, "Ainda não existem procedimentos cadastrados", class: "swal-title"),
        content_tag(:div, "Mais informações no botão \"Instruções de uso\".", class: "swal-text")
      ]
      end
  end
end
