defmodule DentalBookPresenter.FinancialHistoryView do
  use DentalBookPresenter.Web, :view
  alias DentalBookPresenter.DateFormat
  alias DentalBookPresenter.TimeFormat
  alias DentalBookPresenter.Income

  def create_patient_avatar() do
    content_tag :div, class: "thumb-xl member-thumb m-b-10 center-block" do
      content_tag :div, class: "text-center" do
        content_tag(:i, "", class: "mdi mdi-account-circle", style: "font-size:120px;")
      end
    end 
  end

  def create_identifiers_of(patient) do
    content_tag :div do
    [
      content_tag(:h4, patient.name, class: "m-b-5"),
      content_tag(:p, content_tag(:span, patient.cellphone))
    ]
    end
  end

  def create_button_to(:appointment_related_to, patient, conn) do
    classes = "btn btn-success btn-sm w-sm waves-effect m-t-10 waves-light"
    path = get_appointment_path_of(patient, conn)
    content_tag(:a, "Novo Plano", href: path, class: classes)
  end

  def create_button_to(:edit_patient, patient, conn) do
    classes = "btn btn-primary btn-sm w-sm waves-effect m-t-10 waves-light"
    path = patient_path(conn, :edit, patient)
    content_tag(:a, "Editar", href: path, class: classes)
  end

  def create_details_of(patient) do
    content_tag :div, class: "text-left" do
    [
      create_detail_to("Sexo:", patient.gender),
      create_detail_to("Idade:", "#{patient.age} anos"),
      create_detail_to("CPF:", patient.cpf),
      create_detail_to("Email:", "awesometeeth@dentalbook.com")
    ]
    end
  end

  def create_financial_general_resume(patient_history) do
    content_tag :div, id: "custom_results_card" do
      content_tag :div, class: "text-center" do
        [
          content_tag(:h4, "Resumo Geral", class: "header-title m-t-0 m-b-30"),
          content_tag(:span, "Total Pago", class: "text-muted m-t-30"),
          content_tag(:h1, patient_history.financial_history.total_paid_bills, class: "m-t-5", id: "custom_results"),
          content_tag :div, class: "detalhes" do
          [
            create_incomes_section(patient_history),
            create_expenses_section(patient_history)
          ]
          end
        ]
      end
    end
  end

  def create_financial_table_of(:paid_bills, financial_history, conn) do
    %{
      table_id: "paid-bills",
      total_paid_bills: financial_history.total_paid_bills,
      table_header: get_table_header_of_bills(:paid),
      table_content: get_detailed_data_from(financial_history.all_paid_bills, :paid, conn)
    } 
    |> create_table
  end
  
  def create_financial_table_of(:unpaid_bills_not_overdue, financial_history, conn) do
    %{
      table_id: "unpaid-bills-not-overdue",
      total_paid_bills: financial_history.total_unpaid_bills_without_overdue,
      table_header: get_table_header_of_bills(:unpaid_bills_not_overdue),
      table_content: get_detailed_data_from(financial_history.all_unpaid_bills_without_overdue, :unpaid_bills_not_overdue, conn)
    } 
    |> create_table
  end

  def create_financial_table_of(:unpaid_bills_overdue, financial_history, conn) do
    %{
      table_id: "unpaid-bills-overdue",
      total_paid_bills: financial_history.total_overdue_bills,
      table_header: get_table_header_of_bills(:unpaid_bills_overdue),
      table_content: get_detailed_data_from(financial_history.all_overdue_unpaid_bills, :unpaid_bills_overdue, conn)
    } 
    |> create_table
  end

  defp get_table_header_of_bills(:paid) do
    [
      content_tag(:th, "Procedimento", class: "col-md-6"),
      content_tag(:th, "Pagamento", class: "col-md-2"),
      content_tag(:th, "Vencimento", class: "col-md-2"),
      content_tag(:th, "Valor", class: "col-md-2")
    ]
  end

  defp get_table_header_of_bills(_) do
    [
      content_tag(:th, "Procedimento", class: "col-md-8"),
      content_tag(:th, "Vencimento", class: "col-md-2"),
      content_tag(:th, "Valor", class: "col-md-2")
    ]
  end

  defp get_detailed_data_from(patient_paid_bills_history, :paid, conn) do
    Enum.map(patient_paid_bills_history, fn clinical_procedure ->
      content_tag :tr do
      [
        content_tag :td, class: "col-md-6" do
          content_tag :a, href: income_path(conn, :edit, %Income{id: clinical_procedure.income_id}) do 
          [
            content_tag(:h5, get_procedure_status_label_of(clinical_procedure.done), class: "m-0"),
            content_tag :p, class: "m-0 text-muted font-13" do
              content_tag(:small, clinical_procedure.procedure_name)
            end
          ]
          end
        end,
        content_tag :td do
          content_tag :a, href: income_path(conn, :edit, %Income{id: clinical_procedure.income_id}) do 
            clinical_procedure.paid_at |> DateFormat.ecto_date_to_string
          end
        end,
        content_tag :td do
          content_tag :a, href: income_path(conn, :edit, %Income{id: clinical_procedure.income_id}) do 
            clinical_procedure.due_date_at |> DateFormat.ecto_date_to_string
          end
        end,
        content_tag :td do
          content_tag :a, href: income_path(conn, :edit, %Income{id: clinical_procedure.income_id}) do 
            clinical_procedure.price
          end
        end
      ]
      end
    end)
  end

  defp get_detailed_data_from(patient_paid_bills_history, _, conn) do
    Enum.map(patient_paid_bills_history, fn clinical_procedure ->
      content_tag :tr do
      [
        content_tag(:td, class: "col-md-6") do
          content_tag :a, href: income_path(conn, :edit, %Income{id: clinical_procedure.income_id}) do 
          [
            content_tag(:h5, get_procedure_status_label_of(clinical_procedure.done), class: "m-0"),
            content_tag(:p, class: "m-0 text-muted font-13") do
              content_tag(:small, clinical_procedure.procedure_name)
            end
          ]
          end
        end,
        content_tag :td do
          content_tag :a, href: income_path(conn, :edit, %Income{id: clinical_procedure.income_id}) do 
            clinical_procedure.due_date_at |> DateFormat.ecto_date_to_string
          end
        end,
        content_tag :td do
          content_tag :a, href: income_path(conn, :edit, %Income{id: clinical_procedure.income_id}) do 
            clinical_procedure.price
          end
        end
      ]
      end
    end)
  end

  defp get_procedure_status_label_of(status) when status == true, do: "Realizado"
  defp get_procedure_status_label_of(_status), do: "Não Realizado"

  defp create_table(params) do
    content_tag :div, id: params.table_id, style: "display:none;" do
      content_tag :div, class: "col-md-8", style: "padding-left:0;padding-right:0;" do
        content_tag :div, class: "panel panel-border panel-primary" do
        [
          content_tag :div, class: "panel-heading" do
            content_tag(:h3, "Total: #{params.total_paid_bills}", class: "panel-title")
          end,

          content_tag :div, class: "panel-body slimscroll-alt", style: "min-height: 395px;" do
            content_tag :div, class: "table-responsive" do
              content_tag :table, class: "table table-hover m-0" do
              [ 
                content_tag(:thead,  content_tag(:tr, params.table_header)),
                content_tag(:tbody, params.table_content)
              ]  
              end
            end
          end
        ]
        end
      end
    end
  end

  defp create_incomes_section(patient_history) do
    content_tag :div, class: "receitas" do
    [
      content_tag(:span, "", class: "mdi mdi-arrow-up text-primary icone-seta"),
      content_tag :div do
      [
        content_tag(:span, "Total a pagar", class: "text-muted"),
        content_tag(:p, patient_history.financial_history.total_unpaid_bills_without_overdue, class: "valor text-primary", id: "custom_income")
      ]
      end
    ]
    end
  end

  defp create_expenses_section(patient_history) do
    content_tag :div, class: "despesas" do
    [
      content_tag(:span, "", class: "mdi mdi-arrow-down text-danger icone-seta"),
      content_tag :div do
      [
        content_tag(:span, "Total em atraso", class: "text-muted"),
        content_tag(:p, patient_history.financial_history.total_overdue_bills, class: "valor text-danger", id: "custom_expense")
      ]
      end
    ]
    end
  end

  defp create_detail_to(field, with_this_value) do
    content_tag :p, class: "text-muted font-13" do
    [
      content_tag(:strong, field),
      content_tag(:span, with_this_value, class: "pull-right")
    ]
    end
  end

  defp get_appointment_path_of(patient, conn) do
    appointment_path(conn, :new)
    |> concat("?")
    |> concat(get(:id, patient))
    |> concat("&")
    |> concat(get(:name, patient)) 
    |> concat("&")
    |> concat(get(:cellphone, patient))
  end

  defp concat(path, element), do: "#{path}#{element}"

  defp get(:id, patient), do: "patient_id=#{patient.id}"
  defp get(:name, patient), do: "patient_name=#{patient.name}"
  defp get(:cellphone, patient), do: "patient_cellphone=#{patient.cellphone}"
end
