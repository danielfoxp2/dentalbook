defmodule DentalBookPresenter.HistoryTimelineView do
  use DentalBookPresenter.Web, :view
  alias DentalBookPresenter.DateFormat
  alias DentalBookPresenter.TimeFormat
  alias DentalBookPresenter.AnamnesisIndicators

  def create(:procedures, procedures_history, patient, conn) do
    content_tag :div, class: "col-sm-12", id: "procedures-history" do
      content_tag :div, class: "timeline timeline-left" do
      [
        create_timeline_button("#", get_patient_name_from(procedures_history)),
        create_timeline_content(:procedures, procedures_history, conn, patient),
        create_timeline_button("/pacientes", "Voltar para lista de pacientes")
      ]
      end
    end
  end

  def create(:anamnesis, anamnesis_history, patient, conn) do
    patient_path = patient_anamnesis_path(conn, :new_anamnesis, patient)
    content_tag :div, class: "col-sm-12", id: "anamnesis-history", style: "display:none;" do
      content_tag :div, class: "timeline timeline-left" do
      [
        create_timeline_button(patient_path, "Nova Anamnese"),
        create_timeline_content(:anamnesis, anamnesis_history, conn, patient),
        create_timeline_button("/pacientes", "Voltar para lista de pacientes")
      ]
      end
    end
  end

  def create(:sick_notes, sick_notes_history, patient, conn) do
    sick_note_path = patient_sick_note_path(conn, :index, patient)
    content_tag :div, class: "col-sm-12", id: "sick-notes-history", style: "display:none;" do
      content_tag :div, class: "timeline timeline-left" do
      [
        create_timeline_button(sick_note_path, "Novo Atestado"),
        create_timeline_content(:sick_notes, sick_notes_history, conn, patient),
        create_timeline_button("/pacientes", "Voltar para lista de pacientes")
      ]
      end
    end
  end

  def create(:prescriptions, prescriptions_history, patient, conn) do
    prescription_path = patient_prescription_path(conn, :index, patient)
    content_tag :div, class: "col-sm-12", id: "prescriptions-history", style: "display:none;" do
      content_tag :div, class: "timeline timeline-left" do
      [
        create_timeline_button(prescription_path, "Novo Receituário"),
        create_timeline_content(:prescriptions, prescriptions_history, conn, patient),
        create_timeline_button("/pacientes", "Voltar para lista de pacientes")
      ]
      end
    end
  end

  defp create_timeline_button(href, label) do
    content_tag :article, class: "timeline-item alt" do
      content_tag :div, class: "text-left" do
        content_tag :div, class: "time-show first", style: "margin-right:0;" do
          content_tag(:a, label, class: "btn btn-purple w-lg", href: href)
        end
      end
    end
  end

  defp get_patient_name_from(procedures_history) do
    history_item = List.first(procedures_history)
    get_name_in(history_item)
  end

  defp get_name_in(history_item) when is_nil(history_item), do: "Paciente não possui histórico"
  defp get_name_in(history_item) do
    history_item.patient_name
  end

  defp create_timeline_content(timeline_type, history, conn, patient) do
    Enum.map(history, fn history_item ->
      content_tag :a, href: get_href(timeline_type, history_item, conn, patient) do
        mount(timeline_type, history_item)
      end
    end)
  end

  defp get_href(:anamnesis, history_item, conn, patient), do: patient_anamnesis_path(conn, :show_anamnesis, patient, history_item.id)
  defp get_href(:procedures, history_item, conn, _patient), do: appointment_path(conn, :edit, history_item.appointment_id)
  defp get_href(:sick_notes, history_item, conn, patient), do: patient_sick_note_path(conn, :show_sick_note, patient, sick_note_to_show_from(history_item))
  defp get_href(:prescriptions, history_item, conn, patient), do: patient_prescription_path(conn, :show_prescription, patient, prescription_to_show_from(history_item))

  defp sick_note_to_show_from(history) do
    %{
      "patient_id" => history.patient_id,
      "dentist_name" => history.dentist_name,
      "dentist_cro" => history.dentist_cro,
      "sick_note_content" => history.sick_note_content
    }
  end

  defp prescription_to_show_from(history_item) do
    %{
      "patient_id" => history_item.patient_id,
      "patient_name" => history_item.patient_name,
      "dentist_name" => history_item.dentist_name,
      "dentist_cro" => history_item.dentist_cro,
      "emission_date" => get_emission_date(history_item),
      "prescription_content" => history_item.prescription_content |> Kernel.inspect
    }
  end

  defp get_emission_date(history_item) do
    history_item.inserted_at 
    |> DateFormat.naive_date_to_ecto_date 
    |> DateFormat.ecto_date_to_string
  end

  defp mount(timeline_type, history_item) do
    content_tag :article, class: "timeline-item" do
      content_tag :div, class: "timeline-desk" do
        content_tag :div, class: "panel" do
          content_tag :div, class: "timeline-box" do
            timeline_card(timeline_type, history_item)
          end
        end
      end
    end
  end

  defp timeline_card(:procedures, history_item) do
    [
      create_span_arrow(),
      create_bullet_point(),
      create_attendance_date_from(history_item),
      create_attendance_hour_from(history_item),
      create_procedure_name_from(history_item)
    ]
  end

  defp timeline_card(:anamnesis, history_item) do
    creation_date =
    DateFormat.naive_date_to_ecto_date(history_item.inserted_at)
    |> DateFormat.ecto_date_to_string

    [
      create_span_arrow(),
      create_bullet_point(),
      content_tag(:h4, creation_date, class: "text-purple"),
      create_hour_of(history_item),
      create_answers_counter_of(history_item)
    ]
  end

  defp timeline_card(:sick_notes, history_item), do: create_timeline_content(history_item)
  defp timeline_card(:prescriptions, history_item), do: create_timeline_content(history_item)

  defp create_hour_of(anamnesis) do
    creation_time = TimeFormat.to_string(anamnesis.inserted_at)
    content_tag(:p, class: "timeline-date text-muted") do
      content_tag(:small, creation_time)
    end
  end

  defp create_answers_counter_of(anamnesis_answers) do
    {total_number_of_questions, number_of_answered_questions} = AnamnesisIndicators.get_question_rates_of(anamnesis_answers)
    questions = get_plural_or_singular_word_of(number_of_answered_questions)
    content_tag :p do
      "#{number_of_answered_questions} #{questions} do total de #{total_number_of_questions}"
    end
  end

  defp get_plural_or_singular_word_of(number_of_answered_questions) when number_of_answered_questions == 1, do: "questão respondida"
  defp get_plural_or_singular_word_of(_number_of_answered_questions), do: "questões respondidas"

  defp create_timeline_content(history_item) do
    creation_date =
    DateFormat.naive_date_to_ecto_date(history_item.inserted_at)
    |> DateFormat.ecto_date_to_string

    [
      create_span_arrow(),
      create_bullet_point(),
      content_tag(:h4, creation_date, class: "text-purple"),
      create_hour_of(history_item),
      create_dentist_from(history_item)
    ]
  end

  defp create_span_arrow() do
    content_tag(:span, "", class: "arrow")
  end

  defp create_bullet_point() do
    content_tag(:span, class: "timeline-icon bg-purple") do
      content_tag(:i, "", class: "mdi mdi-checkbox-blank-circle-outline")
    end
  end

  defp create_attendance_date_from(history_item) do
    content_tag(:h4, DateFormat.ecto_date_to_string(history_item.attendance_date_at), class: "text-purple")
  end

  defp create_attendance_hour_from(history_item) do
    content_tag(:p, class: "timeline-date text-muted") do
      content_tag(:small, TimeFormat.to_string(history_item.attendance_hour))
    end
  end

  defp create_procedure_name_from(history_item) do
    content_tag(:p, history_item.procedure_name)
  end

  defp create_dentist_from(history_item) do
    content_tag :p do
      "#{history_item.dentist_name} - #{history_item.dentist_cro}"
    end
  end
end