defmodule DentalBookPresenter.HistoryNavView do
  use DentalBookPresenter.Web, :view

  def create_tab(href, aria_expanded, title, class \\ "") do
    content_tag :li, class: class do
      content_tag :a, href: href, data: [toggle: "tab"], aria: [expanded: aria_expanded] do
      [
        content_tag(:span, content_tag(:i, "", class: "fa fa-home"), class: "visible-xs"),
        content_tag(:span, title, class: "hidden-xs")
      ]
      end
    end
  end

  def create_nav_buttons(:history) do
    procedure_button = create_button("Procedimentos", "btn-all-procedures-history")
    anamnesis_button = create_button("Anamneses", "btn-all-anamnesis-history")
    sick_notes_button = create_button("Atestados", "btn-all-sick-notes-history")
    prescriptions_button = create_button("Receituários", "btn-all-prescriptions-history")

    [procedure_button, anamnesis_button, sick_notes_button, prescriptions_button]
    |> group
  end

  def create_nav_buttons(:financial) do
    all_bills_button = create_button("Geral", "btn-all-summated-bills")
    paid_bills = create_button("Pagas", "btn-paid-bills")
    unpaid_not_overdue = create_button("A pagar", "btn-unpaid-not-overdue")
    unpaid_overdue = create_button("Em atraso", "btn-unpaid-overdue")

    [all_bills_button, paid_bills, unpaid_not_overdue, unpaid_overdue]
    |> group
  end

  defp create_button(label, id) do
    content_tag :a, id: id, class: "btn btn-default waves-effect", role: "button" do
      label        
    end
  end

  defp group(buttons) do
    content_tag :div, class: "btn-group btn-group-justified m-b-10" do
      buttons
    end
  end
end