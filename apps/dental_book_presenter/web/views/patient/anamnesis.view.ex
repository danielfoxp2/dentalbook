defmodule DentalBookPresenter.AnamnesisView do
  use DentalBookPresenter.Web, :view

  def create_question(question_description, :free_text, input_name, _explanation_description) do
    name = "anamnesis[#{input_name}][response]"
    question_description_name = "anamnesis[#{input_name}][question_description]"
    question_title_class = "header-title m-t-20 m-b-10"

    content_tag :section do
    [
      create_question_title_with_this(question_description, question_title_class),
      create_text_question_with(name),
      create_hidden(question_description, question_description_name)
    ]
    end
  end

  def create_question(question_description, :single_choice, input_name, _explanation_description) do
    yes_id = "anamnesis_#{input_name}_yes"
    no_id = "anamnesis_#{input_name}_no"
    no_response_id = "anamnesis_#{input_name}_no_response"    
    name = "anamnesis[#{input_name}][response]"
    question_description_name = "anamnesis[#{input_name}][question_description]"

    content_tag :section do
    [
      create_option_question(question_description, name, yes_id, no_id, no_response_id),
      create_hidden(question_description, question_description_name)
    ]
    end
  end

  def create_question(question_description, :single_choice_with_free_text, input_name, explanation_description) do
    yes_id = "anamnesis_#{input_name}_yes"
    no_id = "anamnesis_#{input_name}_no"
    no_response_id = "anamnesis_#{input_name}_no_response"
    name = "anamnesis[#{input_name}][response]"
    question_description_name = "anamnesis[#{input_name}][question_description]"
    explanation_name = "anamnesis[#{input_name}][explanation_response]"
    explanation_question_name = "anamnesis[#{input_name}][explanation_question]"
    question_title_class = "control-label"
    question_tag = :label
    
    content_tag :section do
    [
      create_option_question(question_description, name, yes_id, no_id, no_response_id),
      create_question_title_with_this(explanation_description, question_title_class, question_tag),
      create_hidden(question_description, question_description_name),
      create_text_question_with(explanation_name),
      create_hidden(explanation_description, explanation_question_name)      
    ]
    end
  end

  defp create_question_title_with_this(question_description, question_title_class \\ "header-title m-t-20 m-b-20", question_tag \\ :h4) do 
    content_tag(question_tag, question_description, class: question_title_class)
  end

  defp create_text_question_with(name) do
    content_tag :div do
      content_tag(:textarea, "", class: "form-control", name: name, rows: "1", style: "min-height: auto;")
    end
  end

  defp create_hidden(question_description, input_name) do
    content_tag(:input, "", name: input_name, value: question_description, type: "hidden")  
  end

  defp create_option_question(question_description, name, yes_id, no_id, no_response_id) do
    yes_value = "Sim."
    no_value = "Não."
    without_answer_value = "Não respondida."
    checked = true
    [
      create_question_title_with_this(question_description),
      create_option("Sim", name, yes_id, yes_value),
      create_option("Não", name, no_id, no_value),  
      create_option("Sem resposta", name, no_response_id, without_answer_value, checked)  
    ]
  end
  
  defp create_option(label_value, name, id, input_value, checked \\ false) do
    content_tag :div, class: "radio radio-success" do
    [
      content_tag(:input, "", type: "radio", name: name, id: id, value: input_value, checked: checked),
      content_tag(:label, label_value, for: id)
    ]
    end
  end

end