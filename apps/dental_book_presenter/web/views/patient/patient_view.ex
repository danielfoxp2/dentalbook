defmodule DentalBookPresenter.PatientView do
  use DentalBookPresenter.Web, :view
  use Rummage.Phoenix.View
  alias DentalBookPresenter.HistoryNavView
  alias DentalBookPresenter.HistoryTimelineView
  alias DentalBookPresenter.FinancialHistoryView
  alias DentalBookPresenter.AnamnesisView
  alias DentalBookPresenter.ShowingAnamnesisView
  alias DentalBookPresenter.PageTitle

  def create_date_picker(form, field) do
    content_tag :div, class: "input-group" do
      text_input(form, field, class: "form-control patient_birth_date", placeholder: "dd/mm/aaaa")
    end
  end

  def get_hidden_default_age_from(patient_age) do
    patient_age
    |> Kernel.inspect
    |> String.replace("anos", "") 
    |> String.replace("\"", "") 
    |> String.trim
  end
  
  def get_patient_name_from(patient_history) do
    history_item = List.first(patient_history)
    get_name_in(history_item)    
  end

  def create_tab(href, aria_expanded, title, class \\ ""), do: HistoryNavView.create_tab(href, aria_expanded, title, class)
  def create_nav_buttons(of_this_component), do: HistoryNavView.create_nav_buttons(of_this_component)
  def create_timeline_with(procedure_history, patient, conn), do: HistoryTimelineView.create(:procedures, procedure_history, patient, conn)
  def create_timeline_with_anamnesis(anamnesis_history, patient, conn), do: HistoryTimelineView.create(:anamnesis, anamnesis_history, patient, conn)
  def create_timeline_with_sick_notes(sick_notes_history, patient, conn), do: HistoryTimelineView.create(:sick_notes, sick_notes_history, patient, conn)
  def create_timeline_with_prescriptions(prescriptions_history, patient, conn), do: HistoryTimelineView.create(:prescriptions, prescriptions_history, patient, conn)
  def create_patient_avatar(), do: FinancialHistoryView.create_patient_avatar()
  def create_identifiers_of(patient), do: FinancialHistoryView.create_identifiers_of(patient)
  def create_button_to(path_to_go, patient, conn), do: FinancialHistoryView.create_button_to(path_to_go, patient, conn)
  def create_details_of(patient), do: FinancialHistoryView.create_details_of(patient)
  def create_financial_general_resume(patient_history), do: FinancialHistoryView.create_financial_general_resume(patient_history)
  def create_financial_table_of(table_type, financial_history, conn), do: FinancialHistoryView.create_financial_table_of(table_type, financial_history, conn)
  def create_question(question_description, question_type, input_name, explanation_description \\ "") do
    AnamnesisView.create_question(question_description, question_type, input_name, explanation_description)
  end

  def show_question(with_this_name, from_this_map), do: ShowingAnamnesisView.show_question(with_this_name, from_this_map)

  def get_label_from(action_type), do: PageTitle.get_label_from(action_type)

  defdelegate not_exists?(patients), to: DentalBookPresenter.OnboardValidation
  defdelegate get_title(), to: DentalBookPresenter.OnboardMessages
  defdelegate get_use_instructions(), to: DentalBookPresenter.OnboardMessages, as: :get_patient_instructions
  defdelegate get_patient_history_instructions(), to: DentalBookPresenter.OnboardMessages
  defdelegate get_icon_type(), to: DentalBookPresenter.OnboardMessages

  def show_that_patient_list_is_empty() do
    content_tag :div, style: "text-align:center" do
    [
      content_tag(:div,"", class: "swal-icon swal-icon--info"),
      content_tag(:div, "Ainda não existem pacientes cadastrados", class: "swal-title"),
      content_tag(:div, "Mais informações no botão \"Instruções de uso\".", class: "swal-text")
    ]
    end
  end

  defp get_name_in(history_item) when is_nil(history_item), do: "Paciente não possui histórico"
  defp get_name_in(history_item) do
    history_item.patient_name
  end
end
