defmodule DentalBookPresenter.ShowingAnamnesisView do
  use DentalBookPresenter.Web, :view

  def show_question(of_this_type, from_this_map) do
    question_description = from_this_map.responses[of_this_type]["question_description"]
    response = from_this_map.responses[of_this_type]["response"] |> show_this
    inner_question = from_this_map.responses[of_this_type]["explanation_question"]
    inner_question_response = from_this_map.responses[of_this_type]["explanation_response"] |> show_this
    content_tag :section do
    [
      content_tag(:h4, question_description, style: "margin-bottom: 5px;"),
      content_tag(:p, response, style: "margin-top: 0;"),
      create(inner_question, inner_question_response),
      tag(:hr)
    ]     
    end 
  end

  defp show_this(anamnesis_response) do
    has_answer = anamnesis_response |> trimmed |> is_answered?
    get(anamnesis_response, has_answer)
  end

  defp trimmed(anamnesis_response) when is_nil(anamnesis_response), do: ""
  defp trimmed(anamnesis_response), do: String.trim(anamnesis_response, " ")

  defp is_answered?(anamnesis_response) when anamnesis_response == "", do: :no
  defp is_answered?(_anamnesis_response), do: :yes

  defp get(anamnesis_response, :yes), do: anamnesis_response
  defp get(anamnesis_response, :no), do: "Não respondida."

  defp create(inner_question, _inner_question_response) when is_nil(inner_question), do: ""
  defp create(inner_question, inner_question_response) do
    [content_tag(:h5, inner_question, style: "margin-bottom: 5px;"), content_tag(:p, inner_question_response, style: "margin-top: 0;")]
  end

end