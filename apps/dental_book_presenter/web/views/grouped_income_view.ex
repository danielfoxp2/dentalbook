defmodule DentalBookPresenter.GroupedIncomeView do
  use DentalBookPresenter.Web, :view

  defdelegate show(grouped_incomes), to: DentalBookPresenter.GroupedIncomesPresenter

  def create_date_picker(form, field) do
    content_tag :div, class: "input-group col-xs-12" do
      text_input(form, field, class: "form-control", placeholder: "dd/mm/aaaa")
    end
  end

  def make_filter_of(procedures, within_this_form) do
    content_tag :div do
    [
      create_label(),
      filter_of(procedures, within_this_form)
    ]
    end
  end

  defp create_label() do
    label :grouped_income_ui_procedures, :procedure_id, class: "control-label" do "Procedimentos" end
  end

  defp filter_of(procedures, within_this_form) do
    content_tag :div do
    [  
      mount_multi_select_of(procedures, within_this_form),
      tag(:hr, class: "no-margin color-line")
    ]
    end
  end

  defp mount_multi_select_of(procedures, within_this_form) do
    options_content = mount_options_for_selection_from(procedures) 
    multi_select_style = "select2 form-control select2-multiple"
    multiple_select(within_this_form, :procedures, options_content , class: multi_select_style)
  end

  defp mount_options_for_selection_from(procedures) do
    Enum.map(procedures, fn procedure ->
      {procedure.name, procedure.id}
    end)
  end
end