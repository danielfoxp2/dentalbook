defmodule DentalBookPresenter.FinancialCommander do
  use Drab.Commander
  alias DentalBookPresenter.FinancialRepo
  alias DentalBookPresenter.Financial
 
  def get_billing_of_period(socket, dom_sender) do
    %{"data" => %{"currentUserId" => current_user_id}, "val" => financial_period} = dom_sender
   
    financial_period_params = get_period_params(financial_period) 

    Financial.changeset(%Financial{}, financial_period_params) 
    |> search_financial_results(socket, current_user_id)
  end

  defp get_period_params(financial_period) when financial_period == "", do: %{"custom_begin_date" => "", "custom_end_date" => ""}
  defp get_period_params(financial_period) do
     [custom_begin_date, custom_end_date] = financial_period |> String.split(";")
     %{"custom_begin_date" => custom_begin_date, "custom_end_date" => custom_end_date}
  end

  defp search_financial_results({_, _} = period, socket, current_user_id) do
    {custom_income, custom_expense, custom_results} = current_user_id
    |> FinancialRepo.get_billing_of_period(period)

    set_status_class_of(custom_results, socket)
    socket |> update(:html, set: custom_results, on: "#custom_results")
    socket |> update(:html, set: custom_income, on: "#custom_income")
    socket |> update(:html, set: custom_expense, on: "#custom_expense")
    socket |> delete("#interval-error-message") 
  end

  defp search_financial_results(error_message, socket, _) do
    socket |> update(:html, set: get_message_alert(error_message), on: "#error-message")
  end

  defp set_status_class_of(custom_results, socket) do
    is_negative?(custom_results)
    |> update_class(socket)
  end

  defp is_negative?(custom_results), do: custom_results |> String.contains?("-")

  defp update_class(result_is_negative, socket) when result_is_negative == true do
    socket |> update(class: "tilebox-primary", set: "tilebox-danger", on: "#custom_results_card")
  end
  defp update_class(_result_is_negative, socket) do
    socket |> update(class: "tilebox-danger", set: "tilebox-primary", on: "#custom_results_card")
  end

  defp get_message_alert(error_message) do
    "
    <div id=\"interval-error-message\" class=\"alert alert-danger alert-dismissible fade in alert-icon\" role=\"alert\">
      <button class=\"close\" type=\"button\" data-dismiss=\"alert\" aria-label=\"Close\">
        <span aria-hidden=\"true\">×</span>
      </button>
      <i class=\"mdi mdi-alert\"></i>
        #{error_message}
    </div>
    "
  end

end
