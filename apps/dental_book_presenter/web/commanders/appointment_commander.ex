defmodule DentalBookPresenter.AppointmentCommander do
  use Drab.Commander, onload: :page_loaded
  use Phoenix.HTML 
  alias Drab.Query, as: Drab
  alias DentalBookPresenter.AttendanceModalCreator
  alias DentalBookPresenter.AttendanceModalUpdater
  alias DentalBookPresenter.AppointmentCard
  alias DentalBookPresenter.AppointmentHiddenCard
  alias DentalBookPresenter.AppointmentUpdatableCard
  alias DentalBookPresenter.AttendanceCardCreator
  alias DentalBookPresenter.AttendanceCardUpdater
  alias DentalBookPresenter.AppointmentProcedure
  alias DentalBookPresenter.Income
  alias DentalBookPresenter.AttendanceModalError
  alias DentalBookPresenter.IncomeCreationDate
  alias DentalBookPresenter.MoneyFormat
  alias DentalBookPresenter.StringToMap


  @procedure_price "appointment_ui[procedures_incomes][price]"

  def add_procedure(socket, dom_sender) do
    %{"data" => %{"proceduresCount" => procedures_count, "invoiceValue" => invoice_value}} = dom_sender
    socket |> launch_modal(procedures_count)
  end

  def delete_procedure(socket, dom_sender) do
    %{"data" => %{"deleteLine" => delete_line, "valueOfDeletedProcedure" => value_of_deleted_procedure }} = dom_sender
    is_negative = true
    socket |> update_total_value(value_of_deleted_procedure, is_negative)
    socket |> delete("." <> delete_line)
  end

  def edit_procedure(socket, dom_sender) do
    %{"data" => %{"lineId" => line_id}} = dom_sender
    params = socket |> Drab.select(:html, from: "#" <> line_id)
    
    socket |> launch_modal_edit(params, line_id)
  end
  
  def page_loaded(%Phoenix.Socket{assigns: %{__action: action}} = socket) 
  when action == :new do
    socket |> put_store(:counter, 0)
    socket |> put_store(:total, Money.new(0))
  end

  def page_loaded(%Phoenix.Socket{assigns: %{__action: action}} = socket) 
  when action == :edit do
    invoice_value = socket 
    |> Drab.select(:html, from: ".total-money")
    |> MoneyFormat.parse_value_to_money

    socket |> put_store(:total, invoice_value)
  end

  def page_loaded(_socket), do: nil

  defp launch_modal(socket, procedures_count) do
    case socket |> alert("Adicionar Procedimento", AttendanceModalCreator.create_form(socket), buttons: [ok: "OK", cancel: "Cancelar"]) do
      { :ok, params } ->  
        new_id = get_new_id(socket, procedures_count)
        consistent_params = socket |> get_consistent_params(new_id, params)
        attendance_params = [socket: socket, new_id: new_id, original_price: "0", module: AttendanceCardCreator]
        {consistent_params, attendance_params, :new} |> build_attendance_with

      { :cancel, _ }  -> :canceled
    end  
  end

  defp launch_modal_edit(socket, params, line_id) do
    parsed_params = params |> StringToMap.convert
    original_price = parsed_params[@procedure_price]

    name = case socket |> alert("Adicionar Procedimento", AttendanceModalUpdater.create_form(parsed_params), buttons: [ok: "OK", cancel: "Cancelar"]) do
      { :ok, params } ->  
        consistent_params = socket |> get_consistent_params(line_id, params)
        attendance_params = [socket: socket, new_id: line_id, original_price: original_price, module: AttendanceCardUpdater]
        {consistent_params, attendance_params, :edit} |> build_attendance_with

      { :cancel, _ } -> :canceled
    end  
  end

  defp build_attendance_with({:canceled, _, _} ), do: :canceled
  defp build_attendance_with({card_params, params, procedure_life_status}) do
    socket = params |> Keyword.fetch!(:socket)
    new_id = params |> Keyword.fetch!(:new_id)
    module = params |> Keyword.fetch!(:module)

    original_price = params |> Keyword.fetch!(:original_price)
    new_price = card_params[@procedure_price]
    
    socket |> mount_card(new_id, card_params, module)
    {procedure_life_status, socket, new_price, original_price} |> proccess_total_value
  end

  defp proccess_total_value({:new, socket, price_edited_by_user, _}) do
    socket |> update_total_value(price_edited_by_user)
  end

  defp proccess_total_value({_, socket, price_edited_by_user, original_price}) do
    final_price = calculate_attendance_total(price_edited_by_user, original_price)
    socket |> update_total_value(final_price)
  end

  defp calculate_attendance_total(new_price, original_price) do
    new_price_money = new_price |> MoneyFormat.parse_value_to_money
    original_price_money = original_price |> MoneyFormat.parse_value_to_money

    new_price_money
    |> Money.subtract(original_price_money) 
    |> Money.to_string
  end

  defp mount_card(_, _, :canceled, _), do: :canceled
  defp mount_card(socket, new_id, consistent_params, attendance_card) do
    attendance_card.make(new_id, socket, consistent_params)
  end

  defp get_consistent_params(socket, new_id, params) do
    app_procedure = get_appointment_procedure_map_from(params)
    app_income = get_income_map_from(params) 

    app_procedure_changeset = AppointmentProcedure.modal_changeset(%AppointmentProcedure{}, app_procedure)
    app_income_changeset = Income.modal_changeset(%Income{}, app_income)

    consistent_params = params

    if app_procedure_changeset.valid? == false || app_income_changeset.valid? == false do
      error_messages = get_messages_of(app_procedure_changeset)
      error_messages = error_messages ++ get_messages_of(app_income_changeset)
    
      params_modal_edit = AppointmentUpdatableCard.make(params)

      params_modal_edit_parsed = params_modal_edit |> StringToMap.convert

      consistent_params = case socket |> alert("Adicionar Procedimento", AttendanceModalError.create_form(params_modal_edit_parsed, error_messages), buttons: [ok: "OK", cancel: "Cancelar"]) do
        { :ok, params } ->  
          socket |> remove_modal
          consistent_params = get_consistent_params(socket, new_id, params)
        { :cancel, _ }  -> 
          socket |> remove_modal
          :canceled
      end  
     
    end
    consistent_params
  end

  defp remove_modal(socket) do
    socket |> delete(".modal-backdrop")
    socket |> delete("#_drab_modal")
  end

  defp get_appointment_procedure_map_from(params) do
    %{
      procedure_id: params["appointment_ui[procedures_incomes][procedure_id]"],
      procedure_tooth: params["appointment_ui[procedures_incomes][procedure_tooth]"] |> parse_to_string,
      attendance_date_at: params["appointment_ui[procedures_incomes][attendance_date_at]"], 
      attendance_hour: params["appointment_ui[procedures_incomes][attendance_hour]"]
    }
  end

  defp parse_to_string(teeth) when is_nil(teeth), do: ""
  defp parse_to_string(teeth) when is_list(teeth), do: Enum.join(teeth, ",")
  defp parse_to_string(teeth), do: teeth

  defp get_income_map_from(params) do
    attendance_date = params["appointment_ui[procedures_incomes][attendance_date_at]"]
    paid_at = params["appointment_ui[procedures_incomes][paid_at]"]
    
    %{
      "value" => params["appointment_ui[procedures_incomes][price]"],
      "created_at" => IncomeCreationDate.create(attendance_date, paid_at), 
      "due_date_at" => params["appointment_ui[procedures_incomes][due_date_at]"],
      "paid_at" => params["appointment_ui[procedures_incomes][paid_at]"]
    }
  end

  defp get_messages_of(changeset) do
    errors = Map.fetch!(changeset, :errors)

    Enum.map(errors, fn {key, value} ->
      {message, _} = value
      {"#{key}", Gettext.dgettext(DentalBookPresenter.Gettext, "errors", message)}
    end)
  end

  defp update_total_value(socket, value, is_negative \\ false) do
    new_total = socket |> get_new_total(value, is_negative)
    
    socket |> update(:html, set: Money.to_string(new_total), on: ".total-money")
    socket |> put_store(:total, new_total)
  end

  defp get_new_total(socket, value, is_negative) do
    get_store(socket, :total)
    |> Money.add(MoneyFormat.parse_value_to_money(value, is_negative))
  end

  defp get_new_id(socket, procedures_count) do
    new_id = get_procedure_id(socket) + procedures_count
  end

  defp get_procedure_id(socket) do
    socket
    |> get_new_procedure_id
    |> store_new_procedure_id(socket)
   
    get_store(socket, :counter)
  end

  defp get_new_procedure_id(socket) do
    get_store(socket, :counter) + 1
  end

  defp store_new_procedure_id(new_id, socket) do
    socket |> put_store(:counter, new_id)
  end
end