defmodule DentalBookPresenter.AppointmentCard do
  use Phoenix.HTML
  alias DentalBookPresenter.AppointmentCardHeader
  alias DentalBookPresenter.AppointmentCardBody
  alias DentalBookPresenter.AppointmentCardFooter

  def make(elements) do
    content_tag :div, class: "panel panel-border #{get_class_color_panel(elements.done)}" do
    [
        AppointmentCardHeader.make(elements),
        content_tag :div, class: "panel-body no-padding-top" do 
        [
            AppointmentCardBody.make(elements),
            AppointmentCardFooter.make(elements)
        ]
        end
    ]
    end
  end

  defp get_class_color_panel(done) when done == true, do: "panel-success"
  defp get_class_color_panel(done) when done == false, do: "panel-danger"

end