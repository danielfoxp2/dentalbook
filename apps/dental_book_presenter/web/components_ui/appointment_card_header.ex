defmodule DentalBookPresenter.AppointmentCardHeader do
  use Phoenix.HTML
  
  def make(elements) do
    header_fields = [content_tag(:h4, elements.procedure_name), create_date_and_hour(elements)]

    content_tag :div, class: "panel-heading" do
      [content_tag(:div, header_fields)]
    end
  end

  defp create_date_and_hour(elements) do
    content_tag :span, class: "text-muted" do
      [
        "Marcado para: ",
        elements.attendance_date_at,
        content_tag(:span, " - "),
        content_tag(:span, elements.attendance_hour)
      ]
    end
  end
end