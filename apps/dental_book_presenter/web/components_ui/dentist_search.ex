defmodule DentalBookPresenter.DentistSearch do
  import PhoenixFormAwesomplete
  alias DentalBookPresenter.Router
  alias DentalBookPresenter.Endpoint

  def make(dentist, form, conn) do
    awesomplete_parameters = get_awesomplete_dentist_parameters(conn)
    awesomplete(form, :dentist_name, [class: "form-control", value: get_dentist_name(dentist)], awesomplete_parameters)
  end

  defp get_awesomplete_dentist_parameters(conn) do
    %{
      url: Router.Helpers.team_url(Endpoint, :get_dentists) <> "/?query=%25",
      urlEnd: "%25",
      filter: "AwesompleteUtil.filterContains",
      minChars: 2,
      label: "label_of_dentist_name",
      value: "dentist_name",
      assign: true
    }
  end

  defp get_dentist_name(dentist) when is_nil(dentist), do: ""
  defp get_dentist_name(dentist), do: dentist.name

end