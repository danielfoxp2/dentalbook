defmodule DentalBookPresenter.AppointmentCardFooter do
  use Phoenix.HTML

  def make(elements) do
    footer_fields = [create_delete_procedure_button(elements.id, elements.price), create_edit_procedure_button(elements.id)]
    content_tag(:div, footer_fields, class: "m-t-15")
  end

  defp create_delete_procedure_button(line_id, price_to_be_deleted) do
    #Está sendo utilizado o raw html porque não é possível criar o atributo drab-click com o content tag
    raw("<button drab-click=\"delete_procedure\" 
                 type=\"button\" 
                 data-delete-line=\"#{line_id}\" 
                 data-value-of-deleted-procedure=\"#{price_to_be_deleted}\"
                 class=\"btn btn-default\" 
                 drab-event=\"click\" 
                 drab-handler=\"delete_procedure\">
                 <i class=\"fa fa-trash m-r-5\"></i>
                 Remover
        </button> "
    )
  end

  defp create_edit_procedure_button(line_id) do
    #Está sendo utilizado o raw html porque não é possível criar o atributo drab-click com o content tag    
    raw("<button drab-click=\"edit_procedure\" 
                 type=\"button\" 
                 data-line-id=\"#{line_id}\" 
                 class=\"btn btn-default\" 
                 drab-event=\"click\" 
                 drab-handler=\"edit_procedure\">
                 <i class=\"fa fa-pencil m-r-5\"></i>
                 Editar
        </button>"
    )    
  end

end