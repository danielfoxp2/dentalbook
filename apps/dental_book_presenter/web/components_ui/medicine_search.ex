defmodule DentalBookPresenter.MedicineSearch do
  import PhoenixFormAwesomplete
  alias DentalBookPresenter.Router
  alias DentalBookPresenter.Endpoint

  def make(form) do
    awesomplete_parameters = get_awesomplete_medicine_parameters()
    awesomplete(form, :medicine_description, [class: "form-control"], awesomplete_parameters)
  end

  defp get_awesomplete_medicine_parameters() do
    %{
      url: Router.Helpers.medicine_url(Endpoint, :get_medicines) <> "/?query=%25",
      urlEnd: "%25",
      filter: "AwesompleteUtil.filterContains",
      minChars: 2,
      label: "label_of_medicine_description",
      value: "medicine_description",
      assign: true,
      descr: "medicine_use_type"
    }
  end
end