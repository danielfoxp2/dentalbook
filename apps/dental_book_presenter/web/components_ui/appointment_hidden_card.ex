defmodule DentalBookPresenter.AppointmentHiddenCard do
  use Phoenix.HTML

  def make(elements) do
    content_tag :div do
    [
      tag(:input, name: "#{elements.name}[procedure_name]", type: "hidden", value: elements.procedure_name),
      tag(:input, name: "#{elements.name}[procedure_tooth]", type: "hidden", value: get(elements.procedure_tooth)),
      tag(:input, name: "#{elements.name}[procedure_tooth_faces]", type: "hidden", value: get(elements.procedure_tooth_faces)),
      tag(:input, name: "#{elements.name}[price]", type: "hidden", value: elements.price),
      tag(:input, name: "#{elements.name}[done]", type: "hidden", value: elements.done |> Kernel.inspect),
      tag(:input, name: "#{elements.name}[attendance_date_at]", type: "hidden", value: elements.attendance_date_at),
      tag(:input, name: "#{elements.name}[attendance_hour]", type: "hidden", value: elements.attendance_hour),
      tag(:input, name: "#{elements.name}[due_date_at]", type: "hidden", value: elements.due_date_at),
      tag(:input, name: "#{elements.name}[paid_at]", type: "hidden", value: elements.paid_at),
      tag(:input, name: "#{elements.name}[procedure_id]", type: "hidden", value: elements.procedure_id)
    ]
    end
  end

  defp get(tooth_data) when tooth_data |> is_nil, do: []
  defp get(tooth_data) when tooth_data |> is_binary, do: tooth_data
  defp get(tooth_data), do: Enum.join(tooth_data, ",")
end