defmodule DentalBookPresenter.SwitchButton do
  use Phoenix.HTML

  alias DentalBookPresenter.TagAttributes
  
  def make({tag_attributes_hidden, tag_attributes_checkbox}) do
    raw(
    "<input #{configure_tag_with(tag_attributes_hidden)} >
     <input #{configure_tag_with(tag_attributes_checkbox)} #{has_permission?(tag_attributes_checkbox.field_enabled?)} switch=\"info\">
     #{configure_tag_label(tag_attributes_checkbox)}")
  end

  defp configure_tag_with(tag_attributes) do
    tag_id(tag_attributes) 
    |> tag_name(tag_attributes) 
    |> tag_type(tag_attributes)
    |> tag_value(tag_attributes)
    |> tag_class(tag_attributes)
  end

  defp has_permission?(true), do: ""
  defp has_permission?(false), do: "disabled=\"disabled\""

  defp configure_tag_label(tag_attributes_checkbox) do
    label_for = tag_id(tag_attributes_checkbox) |> String.replace("id=", "")
    {_, tag_label} = raw("<label for=#{label_for} data-on-label=\"Sim\" data-off-label=\"Não\" class=\"m-b-0 c-m-t\"></label>")

    tag_label
  end

  defp tag_id(%TagAttributes{field_id: field_id} = params) when is_nil(field_id), do: ""
  defp tag_id(params), do: "id=\"#{params.field_id}\""

  defp tag_name(previous_attributes, %TagAttributes{} = params) do
    previous_attributes <> "name=\"#{params.field_name}\""
  end

  defp tag_type(previous_attributes, %TagAttributes{} = params) do
    previous_attributes <> "type=\"" <> params.field_type <> "\""
  end

  defp tag_value(previous_attributes, %TagAttributes{field_value: field_value} = params) 
  when field_value == nil do previous_attributes end

  defp tag_value(previous_attributes, %TagAttributes{field_value: field_value} = params) 
  when field_value != nil do
    "#{previous_attributes}value=\"#{field_value}\""
  end

  defp tag_class(previous_attributes, %TagAttributes{field_class: field_class} = params)
  when field_class == nil do previous_attributes end

  defp tag_class(previous_attributes, %TagAttributes{field_class: field_class} = params)
  when field_class != nil do
    previous_attributes <> "class=\"" <> field_class <> "\""
  end

end