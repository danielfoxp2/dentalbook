defmodule DentalBookPresenter.AppointmentCardBody do
  use Phoenix.HTML

  def make(elements) do
    [
      create_price(elements),
      create_procedure_income_details(elements)
    ]
  end

  defp create_price(elements) do
    content_tag :div do
      content_tag(:h2, elements.price, class: "text-muted")
    end
  end

  defp create_procedure_income_details(elements) do
    content_tag :div do
      [
        create_procedures_status(elements.done),
        content_tag(:div, get_tooth_label(elements.procedure_tooth)),
        content_tag(:div, get_date_label("Vencimento", elements.due_date_at)),
        content_tag(:div, get_date_label("Pagamento", elements.paid_at))
      ]
    end
  end

  defp create_procedures_status(it_was_done) when it_was_done == true do
    content_tag(:span, "Procedimento já realizado", class: "text-success")
  end

  defp create_procedures_status(it_was_done) when it_was_done == false do
    content_tag(:span, "Procedimento não realizado", class: "text-danger")
  end

  defp get_tooth_label(tooth) when tooth == "", do: "Dente: Não informado"
  defp get_tooth_label(tooth) when is_list(tooth), do: "Dente: #{Enum.join(tooth, ", ")}"
  defp get_tooth_label(tooth), do: "Dente: #{tooth |> String.replace(",", ", ")}"

  defp get_date_label(label, date) when date == "" or date == nil, do: "#{label}: Não informado"  
  defp get_date_label(label, date) when date != "", do: "#{label}: #{date}" 

end