defmodule DentalBookPresenter.AppointmentUpdatableCard do
  use Phoenix.HTML
  alias DentalBookPresenter.MoneyFormat

  @tooth_key "appointment_ui[procedures_incomes][procedure_tooth]"
  @tooth_faces_key "appointment_ui[procedures_incomes][procedure_tooth_faces]"
  @price_key "appointment_ui[procedures_incomes][price]"

  def make(%Phoenix.HTML.Form{} = params) do
    %{
      "#{get_form_name(params)}[procedure_name]" => params.data.procedure_name,
      "#{get_form_name(params)}[procedure_tooth]" => get_value_of(params.data.procedure_tooth),
      "#{get_form_name(params)}[procedure_tooth_faces]" => get_value_of(params.data.procedure_tooth_faces),
      "#{get_form_name(params)}[price]" => Money.to_string(params.data.price),
      "#{get_form_name(params)}[done]" => Kernel.inspect(params.data.done),
      "#{get_form_name(params)}[attendance_date_at]" => params.data.attendance_date_at,
      "#{get_form_name(params)}[attendance_hour]" => params.data.attendance_hour,
      "#{get_form_name(params)}[due_date_at]" => params.data.due_date_at,
      "#{get_form_name(params)}[paid_at]" => get_value_in(params.data.paid_at),
      "#{get_form_name(params)}[procedure_id]" =>  params.data.procedure_id
    }
    |> Kernel.inspect
  end

  def make(params) do
    params 
    |> make_tooth
    |> make_tooth_faces
    |> make_procedure_price   
    |> Kernel.inspect
  end

  defp make_tooth(%{@tooth_key => tooth} = params) when is_nil(tooth) do
    params_without_nil_tooth = update_in(params[@tooth_key], &get_value_of(&1))
    params_without_nil_tooth
  end

  defp make_tooth(%{@tooth_key => tooth} = params) when is_binary(tooth) do
    params_with_tooth_as_list = update_in(params[@tooth_key],  &String.split(&1, ","))
    params_with_tooth_as_list
  end

  defp make_tooth(params), do: params

  defp make_tooth_faces(%{@tooth_faces_key => tooth_faces} = params) when is_binary(tooth_faces) do
    params_with_tooth_faces_as_list = update_in(params[@tooth_faces_key], &String.split(&1, ","))
    params_with_tooth_faces_as_list 
  end

  defp make_tooth_faces(%{@tooth_faces_key => tooth_faces} = params) when is_nil(tooth_faces) do
    params_without_nil_tooth_faces = update_in(params[@tooth_faces_key], &get_value_of(&1))
    params_without_nil_tooth_faces 
  end

  defp make_tooth_faces(params), do: params

  defp make_procedure_price(params) do
    params_without_nil_procedure_price = update_in(params[@price_key], &format(&1))
    params_without_nil_procedure_price
  end

  defp get_value_of(tooth_data) when is_nil(tooth_data), do: []
  defp get_value_of(tooth_data), do: tooth_data

  defp get_value_in(paid_at) when is_nil(paid_at), do: ""
  defp get_value_in(paid_at), do: paid_at

  defp get_form_name(procedure_form) do
    remove_procedure_index_from(procedure_form.name)
  end

  defp remove_procedure_index_from(form_name) do
    form_name |> String.replace(~r/\[[0-9]+\]/, "")
  end

  defp format(price) when is_nil(price), do: ""
  defp format(price) when price == "", do: price
  defp format(price) do
    MoneyFormat.parse_value_to_money(price) 
    |> Money.to_string
  end

end
