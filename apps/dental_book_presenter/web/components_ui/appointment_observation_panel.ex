defmodule DentalBookPresenter.AppointmentObservationPanel do
  use Phoenix.HTML
  
  def make(elements) do
    content_tag :div, class: "col-sm-12" do
    [
      content_tag :article, class: "panel panel-border panel-teal panel-observation" do 
      [
        make_panel_header(elements),
        make_panel_body(elements),
        make_hidden_inputs(elements)
      ] 
      end
    ] 
    end
  end

  defp make_panel_header(elements) do
    content_tag :div, class: "panel-heading" do
    [
      content_tag :div, class: "col-xs-6 col-sm-6", style: "padding:0;" do
      [
        content_tag(:h4, elements.creation_date, style: "margin:0;"),
        content_tag :p, class: "timeline-date text-muted", style: "margin:0;" do
          content_tag(:small, elements.creation_hour)
        end
      ]
      end,

      content_tag :div, class: "col-xs-6 col-sm-6", style: "text-align:right;padding:0;" do
      [
        content_tag :button, class: "close", type: "button", onClick: "removeObservation(this)" do
          content_tag(:i, "", class: "ion-close-round")
        end
      ]
      end
    ]
    end
  end

  defp make_panel_body(elements) do
    raw_observation = elements.observation |> raw
    content_tag :div, class: "panel-body", style: "padding-left:0; margin-left:10px;" do
      content_tag(:p, raw_observation, class: "col-xs-12 col-sm-12", style: "overflow-wrap:break-word;")
    end
  end

  defp make_hidden_inputs(elements) do
    content_tag :div, class: "observation_to_be_sent" do
    [
      tag(:input, type: "hidden", name: "appointment_ui[observations][#{elements.observation_id}][observation_id]", value: "#{elements.observation_id}"),
      tag(:input, type: "hidden", name: "appointment_ui[observations][#{elements.observation_id}][creation_date]", value: "#{elements.creation_date}"),
      tag(:input, type: "hidden", name: "appointment_ui[observations][#{elements.observation_id}][creation_hour]", value: "#{elements.creation_hour}"),
      tag(:input, type: "hidden", name: "appointment_ui[observations][#{elements.observation_id}][observation]", value: "#{elements.observation}")
    ] 
    end
  end

end
