defmodule DentalBookPresenter.IncomeController do
  use DentalBookPresenter.Web, :controller
  use Rummage.Phoenix.Controller
  
  alias DentalBookPresenter.Income
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.RummageConfig
  alias DentalBookPresenter.IncomeUI
  alias DentalBookPresenter.AppointmentProcedure
  alias DentalBookPresenter.AppointmentRepo
  alias DentalBookPresenter.IncomeSearchFilters
  alias DentalBookPresenter.Appointment
  alias DentalBookPresenter.Patient
  alias DentalBookPresenter.IncomeBudget
  alias Rummage.Ecto

  def index(conn, params) do
    user_id = conn.assigns.current_user.id

    results_per_page = "10"

    rummage_default =  RummageConfig.get_default_params_from(params, results_per_page)

    income_tenant =  RummageConfig.get_rummage_query_with_tenant_name(Income, user_id)

    search_params = get_filters_in(params, conn, params["clear_filters"])

    query_get_presentable_incomes = get_presentable_incomes(income_tenant, search_params)
    
    tenant_name = Tenant.get_name(user_id, Repo)

    all_incomes_query =
    Apartmentex.all(Repo, query_get_presentable_incomes, tenant_name)
    |> get_income_query(income_tenant)

    searched_total_income_budget = 
    Apartmentex.all(Repo, all_incomes_query, tenant_name)
    |> IncomeBudget.get_total_of

    {query_paginated, rummage} = all_incomes_query
    |> Rummage.Ecto.rummage(rummage_default)

    incomes = Apartmentex.all(Repo, query_paginated, tenant_name)
    searched_partial_income_budget = IncomeBudget.get_total_of(incomes)

    incomes_ui = incomes
    |> get_appointment_ids_from
    |> AppointmentRepo.search_appointment_by(tenant_name)
    |> IncomeUI.get_income_ui_from(incomes)
    
    income_total = 
    %{
      searched_partial_income_budget: searched_partial_income_budget, 
      searched_total_income_budget: searched_total_income_budget
    }

    conn
    |> put_session(:search_params, search_params)
    |> render("index.html", incomes: incomes_ui, rummage: rummage, income_total: income_total)
  end

  defp get_filters_in(params, conn, is_to_clear) when is_to_clear == "true", do: %{}
  defp get_filters_in(params, conn, _), do: params["income_search"] || get_session(conn, :search_params)

  def new(conn, _params) do
    changeset = IncomeUI.changeset(%IncomeUI{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"income_ui" => income_params}) do
    user_id = conn.assigns.current_user.id

    changeset = Income.changeset(%Income{}, income_params)

    case Apartmentex.insert(Repo, changeset, Tenant.get_name(user_id, Repo)) do
      {:ok, _income} ->
        conn
        |> put_flash(:info, "Receita criada com sucesso.")
        |> redirect(to: income_path(conn, :index))
      {:error, changeset} ->
        income_ui_changeset = IncomeUI.changeset(%IncomeUI{}, income_params)
        |> Map.put(:action, changeset.action)
        |> Map.put(:errors, changeset.errors)
        render(conn, "new.html", changeset: income_ui_changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    user_id = conn.assigns.current_user.id

    income = Apartmentex.get!(Repo, Income, id, Tenant.get_name(user_id, Repo))
    |> IncomeUI.get_income_ui_from

    changeset = IncomeUI.changeset(income)
    render(conn, "edit.html", income: income, changeset: changeset)
  end

  def update(conn, %{"id" => id, "income_ui" => income_params}) do
    user_id = conn.assigns.current_user.id

    income = Apartmentex.get!(Repo, Income, id, Tenant.get_name(user_id, Repo))
    
    changeset = Income.changeset(income, income_params)
  
    case Apartmentex.update(Repo, changeset, Tenant.get_name(user_id, Repo)) do
      {:ok, _income} ->
        conn
        |> put_flash(:info, "Receita atualizada com sucesso.")
        |> redirect(to: income_path(conn, :index))
      {:error, changeset} ->
        income_ui_changeset = IncomeUI.changeset(%IncomeUI{}, income_params)
        |> Map.put(:action, changeset.action)
        |> Map.put(:errors, changeset.errors)
        render(conn, "edit.html", income: income, changeset: income_ui_changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user_id = conn.assigns.current_user.id

    Apartmentex.get!(Repo, Income, id, Tenant.get_name(user_id, Repo))
    |> income_deleted(conn, user_id)
  end

  defp income_deleted(%Income{appointment_procedure_id: appointment_procedure_id} = income, conn, user_id) when is_nil(appointment_procedure_id) do
    Apartmentex.delete(Repo, income, Tenant.get_name(user_id, Repo))

    conn
    |> put_flash(:info, "Receita excluída com sucesso.")
    |> redirect(to: income_path(conn, :index))
  end

  defp income_deleted(_income, conn, _user_id) do
    conn
    |> put_flash(:error, "Receita não pode ser excluída pois está vinculada a um plano de tratamento.")
    |> redirect(to: income_path(conn, :index))
  end

  defp get_presentable_incomes(all_incomes, search_params) do
    from income in all_incomes, 
    left_join: appointment_procedure in AppointmentProcedure, 
    on: appointment_procedure.id == income.appointment_procedure_id,
    left_join: appointment in Appointment,
    on: appointment.id == appointment_procedure.appointment_id,
    left_join: patient in Patient,
    on: patient.id == appointment.patient_id,
    where: ^IncomeSearchFilters.build(search_params),
    select: income.id
  end

  defp get_income_query(incomes_id, all_incomes) do
    from ic in all_incomes,
    where: ic.id in ^incomes_id
  end

  defp get_appointment_ids_from(incomes) do
    Enum.map(incomes, fn income -> income.appointment_procedure_id end)
    |> Enum.reject(fn appointment_procedure_id -> is_nil(appointment_procedure_id) end)
  end
  
end
