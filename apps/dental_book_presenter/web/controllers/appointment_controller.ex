defmodule DentalBookPresenter.AppointmentController do
  use DentalBookPresenter.Web, :controller
  use Drab.Controller
  use Rummage.Phoenix.Controller
  
  alias Rummage.Ecto
  alias DentalBookPresenter.Appointment
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.AppointmentUI
  alias DentalBookPresenter.AppointmentService
  alias DentalBookPresenter.RummageConfig
  alias DentalBookPresenter.AppointmentCreatorController
  alias DentalBookPresenter.AppointmentUpdaterController
  alias DentalBookPresenter.AppointmentRepo
  alias DentalBookPresenter.StringToMap
  alias DentalBookPresenter.InvoicePrintPresenter
  alias DentalBookPresenter.CompanyRepo
  alias DentalBookPresenter.EncodeParser
  alias DentalBookPresenter.ReportGenerator
  

  def index(conn, params) do
    user_id = conn.assigns.current_user.id

    results_per_page = "5"
    rummage_default =  RummageConfig.get_default_params_from(params, results_per_page)

    search_term = RummageConfig.get_search_term(rummage_default)
    rummage_default = update_in(rummage_default["search"], &(&1 = %{})) 

    appointment_tenant = RummageConfig.get_rummage_query_with_tenant_name(Appointment, user_id)
    
    query_get_presentable_appointments = AppointmentRepo.get_presentable_appointments(appointment_tenant, search_term)

    tenant_name = Tenant.get_name(user_id, Repo)

    appointments_like = Apartmentex.all(Repo, query_get_presentable_appointments, tenant_name)

    {query_paginated, rummage} = 
    appointments_like
    |> AppointmentRepo.get_appointment_query(appointment_tenant)
    |> Rummage.Ecto.rummage(rummage_default)

    appointments = 
      AppointmentService.get_query_of_appointment_and_patients(query_paginated)
      |> AppointmentService.get_appointments(Tenant.get_name(user_id, Repo))
      |> AppointmentUI.get_appointment_ui_from

    render(conn, "index.html", appointments: appointments, rummage: rummage)
  end

  def new(conn, %{"patient_id" => patient_id, "patient_name" => patient_name, "patient_cellphone" => patient_cellphone}) do
    patient = %{patient_id: patient_id, patient_name: patient_name, patient_cellphone: patient_cellphone}
    AppointmentCreatorController.new(conn, patient)
  end

  def new(conn, _params) do
    AppointmentCreatorController.new(conn)
  end

  def create(conn, %{"appointment_ui" => appointment_params}) do
    AppointmentCreatorController.create(conn, appointment_params)
  end

  def edit(conn, %{"id" => id}) do
    AppointmentUpdaterController.edit(conn, id)
  end

  def update(conn, %{"id" => id, "appointment_ui" => appointment_params}) do
    AppointmentUpdaterController.update(conn, id, appointment_params)
  end

  def delete(conn, %{"id" => id}) do
    tenant_name = Tenant.get_name(conn.assigns.current_user.id, Repo)

    case AppointmentService.delete_appointment_procedures_incomes(tenant_name, id) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Plano de tratamento excluído com sucesso.")
        |> redirect(to: appointment_path(conn, :index))
      {:error, changeset} ->
        conn
        |> put_flash(:error, "Erro ao tentar excluir plano de tratamento.")
        render(conn, "edit.html", changeset: changeset)
    end

  end

  def print_dental_invoice(conn, %{"appointment_id" => appointment_id, "params" => params}) do
    tenant_name = Tenant.get_name(conn.assigns.current_user.id, Repo)
    
    invoice_params = String.split(params, "&") 
    [procedure_params | patient_params] = invoice_params
  
    procedure_params_utf8 =  EncodeParser.get_string_from(procedure_params)

    invoice_procedures = get_invoice_procedures(procedure_params_utf8)
    [patient_name | patient_cellphone] = get_patient_data(patient_params)

    company_name = CompanyRepo.get_company_name_by(conn.assigns.current_user.id, Repo)

    invoice_data = 
    %{
      company_name: EncodeParser.get_string_from(company_name), 
      patient: %{name: EncodeParser.get_string_from(patient_name), cellphone: patient_cellphone},
      procedures: invoice_procedures
     }

    #deletar arquivo gerado para evitar sobrecarga

    template_params = [conn: conn, invoice_data: invoice_data, invoice_print: InvoicePrintPresenter]
    invoice_file_params = get_invoice_file_params(template_params)

    {filename, pdf_content} = ReportGenerator.create_pdf(invoice_file_params)

    conn
    |> put_resp_content_type("application/pdf")
    |> put_resp_header("content-disposition", "attachment; filename=#{filename}")
    |> send_resp(200, pdf_content)
  end

  defp get_invoice_procedures(invoice_procedure_params) do
    invoice_procedure_params
    |> String.split(";") 
    |> Enum.reject(fn element -> element == "" end)
    |> Enum.map(&StringToMap.convert/1)
  end

  defp get_patient_data(patient_params) do
    find_everything_till_equal_sign = ~r/.+?=/

    patient_params
    |> Enum.map(fn element -> String.replace(element, find_everything_till_equal_sign, "") end)
  end

  defp get_template_path(), do: "/templates/dental_invoice/invoice_report.html.eex"

  defp get_invoice_file_params(template_params) do
    [
      file_name: "orcamento-dentario", 
      template_path: get_template_path(), 
      partial_css_template_dir: "/static/css", 
      template_params: template_params
    ]
  end
    
end
