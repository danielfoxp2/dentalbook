defmodule Coherence.Redirects do
  @moduledoc """
  Define controller action redirection functions.

  This module contains default redirect functions for each of the controller
  actions that perform redirects. By using this Module you get the following
  functions:

  * session_create/2
  * session_delete/2
  * password_create/2
  * password_update/2,
  * unlock_create_not_locked/2
  * unlock_create_invalid/2
  * unlock_create/2
  * unlock_edit_not_locked/2
  * unlock_edit/2
  * unlock_edit_invalid/2
  * registration_create/2
  * invitation_create/2
  * confirmation_create/2
  * confirmation_edit_invalid/2
  * confirmation_edit_expired/2
  * confirmation_edit/2
  * confirmation_edit_error/2

  You can override any of the functions to customize the redirect path. Each
  function is passed the `conn` and `params` arguments from the controller.

  ## Examples

      import MyProject.Router.Helpers

      # override the log out action back to the log in page
      def session_delete(conn, _), do: redirect(conn, session_path(conn, :new))

      # redirect the user to the login page after registering
      def registration_create(conn, _), do: redirect(conn, session_path(conn, :new))

      # disable the user_return_to feature on login
      def session_create(conn, _), do: redirect(conn, landing_path(conn, :index))

  """
  use Redirects
  # Uncomment the import below if adding overrides
  import DentalBookPresenter.Router.Helpers
  
  import Mix.Ecto, only: [source_repo_priv: 1]
  use DentalBookPresenter.Web, :controller
  # Add function overrides below

  alias DentalBookPresenter.ClientAccountService
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.Repo
  alias DentalBookPresenter.Orphans


  # Example usage
  # Uncomment the following line to return the user to the login form after logging out
  # def session_delete(conn, _), do: redirect(conn, to: session_path(conn, :new))
  def registration_create(conn, %{"registration" => registration_params}) do
    user = ClientAccountService.get_user(registration_params["email"])
    account_creation_result = ClientAccountService.create_new_client_account(registration_params, user)
    error_message = "Oops, ocorreu algo inesperado! Por favor entre em contato com o admnistrador do sistema."

    TaskAfter.task_after(3600000, fn -> Orphans.delete_if_needed(user) end)
    
    case account_creation_result do
      {:ok, _} ->
        plan_id = registration_params["plan"]
        conn
        |> redirect(to: payment_path(conn, :index, plan_id: plan_id, user_email: user.email))
      {:error, invalid_changeset} ->
        conn
        |> put_flash(:error, error_message)
        |> redirect(to: registration_path(conn, :new, changeset: invalid_changeset))
    end
  end

  def session_create(conn, %{"session" => session_params} = _params) do
    #deve redirecionar para o dashboard do cliente
    user_email = session_params["email"]
    user = ClientAccountService.get_user(user_email)

    tenant_of_the_user = Tenant.get_name(user.id, Repo)
    Apartmentex.migrate_tenant(Repo, tenant_of_the_user)

    public_migration_folder = Path.join(source_repo_priv(Repo), "migrations")
    Ecto.Migrator.run(Repo, public_migration_folder, :up, all: true)

    conn
    |> redirect(to: attendance_path(conn, :index))
  end

  def password_create(conn, session_params) do
    redirect(conn, to: session_path(conn, :new))
  end

  def password_update(conn, session_params) do
    redirect(conn, to: session_path(conn, :new))
  end
end
