defmodule DentalBookPresenter.PageController do
  use DentalBookPresenter.Web, :controller
  alias DentalBookPresenter.PartnerProspect

  def index(conn, %{"parceiro" => partner_key}) do
    prospect_already_exists_to(conn.remote_ip, partner_key)
    |> save_data_prospect_if_not_exists(conn.remote_ip, partner_key)

    render(conn, "index.html", partner_key: partner_key)
  end
  def index(conn, _params), do: render(conn, "index.html", partner_key: nil)

  defp prospect_already_exists_to(client_remote_ip, partner_key) do
    prospect_ip = format(client_remote_ip)

    get_prospect_query(prospect_ip, partner_key)
    |> Repo.one
  end

  defp format(client_remote_ip), do: client_remote_ip |> Tuple.to_list |> Enum.join

  defp save_data_prospect_if_not_exists(%PartnerProspect{}, _client_remote_ip, _partner_key), do: :prospect_already_exists
  defp save_data_prospect_if_not_exists(_prospect_params, client_remote_ip, partner_key) do
    partner_prospect_params = get_partner_prospect_params(client_remote_ip, partner_key)

    %PartnerProspect{}
    |> PartnerProspect.changeset(partner_prospect_params)
    |> Repo.insert
  end

  defp get_partner_prospect_params(client_remote_ip, partner_key) do
    prospect_ip = format(client_remote_ip)

    %{
      "prospect_ip" => prospect_ip,
      "partner_key" => partner_key
    }
  end

  defp get_prospect_query(prospect_ip, partner_key) do
    from p in PartnerProspect,
    where: p.prospect_ip == ^prospect_ip
    and p.partner_key == ^partner_key
  end
end
