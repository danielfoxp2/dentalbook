defmodule DentalBookPresenter.PrescriptionController do
  use DentalBookPresenter.Web, :controller

  alias DentalBookPresenter.Prescription
  alias DentalBookPresenter.Patient
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.ReportGenerator
  alias DentalBookPresenter.PrescriptionDataProcessor
  alias DentalBookPresenter.StringToMap

  def index(conn, %{"patient_id" => patient_id}) do
    tenant_name = Tenant.get_name(conn.assigns.current_user.id, Repo)
    dentist = conn.assigns.current_user
    patient = Apartmentex.get(Repo, Patient, patient_id, tenant_name)
    changeset = Prescription.changeset(%Prescription{})
    action = patient_prescription_path(conn, :create_prescription, patient_id)

    assigns = %{
      dentist: dentist,
      patient: patient,
      changeset: changeset,
      action: action
    }

    render(conn, "form.html", assigns: assigns)
  end

  def show_prescription(conn, params) do
    prescription_content = params["prescription_content"] |> StringToMap.convert
    render(conn, "show.html", prescription: params, prescription_content: prescription_content, conn: conn)
  end
  
  def create_prescription(conn, %{"params" => params}) do
    prescription_data = PrescriptionDataProcessor.get_prescription_data(params)
    save_prescription_to_maintain_history_of(prescription_data, conn.assigns.current_user.id)

    template_params = [conn: conn, prescription_data: prescription_data]
    prescription_file_params = get_prescription_file_params(template_params)

    {filename, pdf_content} = ReportGenerator.create_pdf(prescription_file_params)

    conn
    |> put_resp_content_type("application/pdf")
    |> put_resp_header("content-disposition", "attachment; filename=#{filename}")
    |> send_resp(200, pdf_content)
  end

  defp save_prescription_to_maintain_history_of(params, logged_user_id) do 
    tenant_name = Tenant.get_name(logged_user_id, Repo)
    changeset = Prescription.changeset(%Prescription{}, params)

    Apartmentex.insert!(Repo, changeset, tenant_name)
  end

  defp get_prescription_file_params(template_params) do
    [
      file_name: "receituario", 
      template_path: get_template_path(), 
      partial_css_template_dir: "/static/css", 
      template_params: template_params
    ]
  end
    
  defp get_template_path(), do: "/templates/prescription/prescription_report.html.eex"

end