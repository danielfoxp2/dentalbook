defmodule DentalBookPresenter.PaymentController do
  use DentalBookPresenter.Web, :controller
  require Logger

  alias DentalBookPresenter.ClientAccountService
  alias DentalBookPresenter.Subscription
  alias DentalBookPresenter.AccountPlan
  import Coherence.ControllerHelpers

  def index(conn, params) do
    # selected_plan = Repo.get_by(AccountPlan, id: params["plan_id"])
    # payment_params = get_payment_params_from()

    payment_params = Repo.get_by(AccountPlan, id: params["plan_id"])
    |> get_payment_params_from(params["user_email"])
    |> IO.inspect

    conn
    |> render("index.html", layout: {Coherence.LayoutView, "app.html"}, payment_params: payment_params)
  end

  def signature_confirmation(conn, %{"registration_email" => registration_email} = params) do
    delete_account_data(registration_email)

    reason = "Oops, ocorreu algo inesperado! O e-mail informado no cadastro e no pagamento são diferentes. Sua conta não foi criada"
    error_message = %{type: "error", message: reason}
    json(conn, error_message)
  end

  def signature_confirmation(conn, params) do
    headers = ["content-type": "application/json"]
    responsePagarme = HTTPoison.post("https://api.pagar.me/1/subscriptions", get_signature_data_for(params), headers)
    user_email = params["customer"]["email"]
    client_remote_ip = conn.remote_ip

    case responsePagarme do
      {:ok, response} ->
        signature_response = Poison.decode(response.body)
        account_status_message = resolve_response(client_remote_ip, signature_response, user_email)

        json(conn, account_status_message)
      {_, _response} ->
        error_message = %{type: "error", message: "Oops, ocorreu algo inesperado! Por favor entre em contato com o admnistrador do sistema."}
        delete_account_data(user_email)

        json(conn, error_message)
    end
  end

  def go_to_login_page(conn, status_params) do
    account_status_type = get(:status_type, status_params) |> String.to_atom
    account_status_message = get(:status_message, status_params)

    conn
    |> put_flash(account_status_type, account_status_message)
    |> redirect(to: session_path(conn, :new))
  end

  def signature_update(conn, %{"current_status" => status} = params) do 
    Logger.info "API postback signature_update"
    Logger.info "status #{inspect(status)}"

    customer_email = params["subscription"]["customer"]["email"]

    Logger.info "customer_email #{inspect(customer_email)}"
    Subscription.update_client_subscription_if_needed(status, customer_email)
  
    conn 
    |> put_resp_content_type("text/plain")
    |> send_resp(200, "success")
  end

  defp get_payment_params_from(selected_plan, user_email) do
    %{
      selected_plan_id: selected_plan.id, 
      selected_plan_price: selected_plan.price, 
      user_email: user_email
    }
  end

  defp get(:status_type, %{"account_status_message_type" => account_status_message_type}), do: account_status_message_type
  defp get(_status_type, %{"account_status_message" => account_status_message}), do: account_status_message

  defp get_signature_data_for(params) do
    {:ok, json } = %{
      api_key: System.get_env("AK_LIVE"),
      plan_id: params["selected_plan_id"],
      card_hash: params["card_hash"],
      postback_url: "https://dentalbook.herokuapp.com/api/atualizarassinatura",
      customer: %{
        name: params["customer"]["name"],
        email: params["customer"]["email"],
        document_number: params["customer"]["document_number"],
        address: %{
          street: params["customer"]["address"]["street"],
          street_number: params["customer"]["address"]["street_number"],
          complementary: params["customer"]["address"]["complementary"],
          neighborhood: params["customer"]["address"]["neighborhood"],
          zipcode: params["customer"]["address"]["zipcode"],
        },
        phone: %{
          ddd: params["customer"]["phone"]["ddd"],
          number: params["customer"]["phone"]["number"]
        }
      }
    } |> Poison.encode
    json
  end

  defp resolve_response(_client_remote_ip, {:ok, %{"errors" => errors}} = response, user_email) do
    result_message = get_message_from_payment_service_provider(errors)
  
    delete_account_data(user_email)
    %{type: "error", message: result_message}
  end

  defp resolve_response(client_remote_ip, {:ok, response}, user_email) do 
    partner_id = get_partner_key_by(client_remote_ip)
    |> get_partner_id_from

    company_data = %{"cpf_cnpj" => response["customer"]["document_number"], "partner_id" => partner_id}
    recurrency_signature = %{plan_id: response["plan"]["id"], value: response["plan"]["amount"]}

    user = ClientAccountService.get_user(user_email)

    complementary_client_data(user_email, company_data, recurrency_signature, client_remote_ip)
    |> ClientAccountService.finish_account_creation
    |> notify_with_email(user, recurrency_signature.plan_id)
    |> get_status_message
  end

  defp get_partner_key_by(client_remote_ip) do
    client_remote_ip 
    |> Tuple.to_list 
    |> Enum.join
    |> get_partner_key_query
    |> Repo.query
    |> get_rows_data
    |> get_partner_key
  end

  defp get_partner_key_query(client_remote_ip) do
    """
    select partner_key from partners_prospects p1
    where inserted_at = (select max(inserted_at) 
                         from partners_prospects p2
                         where prospect_ip = '#{client_remote_ip}')
    """
  end

  defp get_rows_data({_, %Postgrex.Result{rows: rows}}), do: rows
  defp get_rows_data(_), do: []

  defp get_partner_key(this_row) do
    this_row
    |> List.flatten
    |> Enum.at(0)
  end

  defp get_partner_id_from(partner_key) when is_nil(partner_key), do: ""
  defp get_partner_id_from(partner_key), do: partner_key |> String.split("-") |> List.last

  defp complementary_client_data(user_email, company_data, recurrency_signature, client_remote_ip) do
    {user_email, company_data, recurrency_signature, client_remote_ip}
  end

  defp get_message_from_payment_service_provider(errors) do
    final_message = " Sua conta não foi criada."
    Enum.map(errors, fn(error) -> Map.get(error, "message", "") end)
    |> Enum.reduce(final_message, fn (error, partial_message) -> error <> partial_message end)
  end

  defp delete_account_data(user_email) do
    ClientAccountService.delete_account_data(user_email)
  end

  defp notify_with_email({:ok, _} = status, user, plan_id) do
    send_user_email(:account_greetings, user, plan_id)
    status
  end
  defp notify_with_email(status, _user, _plan_id), do: status

  defp get_status_message({:ok, _}), do: %{type: "info", message: "Conta criada com sucesso. Informe os dados para login."}
  defp get_status_message(_status), do: %{type: "error", message: "Oops, ocorreu algo inesperado! Sua conta não foi criada."}

end