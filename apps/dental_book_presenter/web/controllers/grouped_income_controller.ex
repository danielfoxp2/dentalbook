defmodule DentalBookPresenter.GroupedIncomeController do
  use DentalBookPresenter.Web, :controller
  import Ecto.Query

  alias DentalBookPresenter.GroupedIncomeUI
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.Procedure
  alias DentalBookPresenter.Income
  alias DentalBookPresenter.IncomeByProcedure
  alias DentalBookPresenter.AppointmentProcedure
  alias DentalBookPresenter.GroupedIncomeFilter
  alias DentalBookPresenter.DateFormat

  def index(conn, _params), do: get_grouped_incomes(conn, :no_filter)
  
  def filter(conn, params) do
    start_date = params["grouped_income_ui"]["start_date"] |> DateFormat.string_to_ecto_date
    end_date = params["grouped_income_ui"]["end_date"] |> DateFormat.string_to_ecto_date
    procedures_ids =  params["grouped_income_ui"]["procedures"]

    search_params = {start_date, end_date, procedures_ids}
    get_grouped_incomes(conn, search_params)
  end

  def get_grouped_incomes(conn, search_params) do
    tenant_name = Tenant.get_name(conn.assigns.current_user.id, Repo)

    financial_changeset = %GroupedIncomeUI{} |> GroupedIncomeUI.changeset(%{})

    all_procedures_of_multi_select = Apartmentex.all(Repo, Procedure, tenant_name)
    all_incomes = Apartmentex.all(Repo, get_income_procedures_query(search_params), tenant_name)

    grouped_procedures = IncomeByProcedure.group(all_incomes)

    render(conn, "index.html", changeset: financial_changeset, action: grouped_income_path(conn, :filter), procedures: all_procedures_of_multi_select, grouped_procedures: grouped_procedures)
  end 

  defp get_income_procedures_query(search_params) do
    from income in Income,
    left_join: appointment_procedure in AppointmentProcedure,
    on: appointment_procedure.id == income.appointment_procedure_id,
    left_join: procedure in Procedure,
    on: procedure.id == appointment_procedure.procedure_id,
    where: ^GroupedIncomeFilter.build(search_params),
    select: %{
      procedure_id: procedure.id,
      procedure_name: procedure.name,
      value: income.value
    }
  end  
end