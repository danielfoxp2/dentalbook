defmodule DentalBookPresenter.ExpenseController do
  use DentalBookPresenter.Web, :controller
  use Rummage.Phoenix.Controller

  alias DentalBookPresenter.Expense
  alias DentalBookPresenter.ExpenseUI
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.RummageConfig
  alias Rummage.Ecto

  def index(conn, params) do
    user_id = conn.assigns.current_user.id

    results_per_page = "5"
    rummage_default =  RummageConfig.get_default_params_from(params, results_per_page)

    {query_paginated, rummage} = 
    RummageConfig.get_rummage_query_with_tenant_name(Expense, user_id)
    |> Rummage.Ecto.rummage(rummage_default)
    

    expenses = Apartmentex.all(Repo, query_paginated, Tenant.get_name(user_id, Repo))
    |> ExpenseUI.get_expense_ui_from
    render(conn, "index.html", expenses: expenses, rummage: rummage)
  end

  def new(conn, _params) do
    changeset = ExpenseUI.changeset(%ExpenseUI{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"expense_ui" => expense_params}) do
    user_id = conn.assigns.current_user.id

    changeset = Expense.changeset(%Expense{}, expense_params)

    case Apartmentex.insert(Repo, changeset, Tenant.get_name(user_id, Repo)) do
      {:ok, _expense} ->
        conn
        |> put_flash(:info, "Despesa criada com sucesso.")
        |> redirect(to: expense_path(conn, :index))
      {:error, changeset} ->
        expense_ui_changeset = ExpenseUI.changeset(%ExpenseUI{}, expense_params)
        |> Map.put(:action, changeset.action)
        |> Map.put(:errors, changeset.errors)
        render(conn, "new.html", changeset: expense_ui_changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    user_id = conn.assigns.current_user.id

    expense = Apartmentex.get!(Repo, Expense, id, Tenant.get_name(user_id, Repo))
    |> ExpenseUI.get_expense_ui_from

    changeset = ExpenseUI.changeset(expense)
    render(conn, "edit.html", expense: expense, changeset: changeset)
  end

  def update(conn, %{"id" => id, "expense_ui" => expense_params}) do
    user_id = conn.assigns.current_user.id
    expense = Apartmentex.get!(Repo, Expense, id, Tenant.get_name(user_id, Repo))
    changeset = Expense.changeset(expense, expense_params)

    case Apartmentex.update(Repo, changeset, Tenant.get_name(user_id, Repo)) do
      {:ok, _expense} ->
        conn
        |> put_flash(:info, "Despesa atualizada com sucesso.")
        |> redirect(to: expense_path(conn, :index))
      {:error, changeset} ->
        expense_ui_changeset = ExpenseUI.changeset(%ExpenseUI{}, expense_params)
        |> Map.put(:action, changeset.action)
        |> Map.put(:errors, changeset.errors)
        render(conn, "edit.html", expense: expense, changeset: expense_ui_changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user_id = conn.assigns.current_user.id

    expense = Apartmentex.get!(Repo, Expense, id, Tenant.get_name(user_id, Repo))
    Apartmentex.delete!(Repo, expense, Tenant.get_name(user_id, Repo))

    conn
    |> put_flash(:info, "Despesa excluída com sucesso.")
    |> redirect(to: expense_path(conn, :index))
  end
end
