defmodule DentalBookPresenter.TeamController do
  use DentalBookPresenter.Web, :controller
  use Rummage.Phoenix.Controller
  
  alias DentalBookPresenter.User
  alias DentalBookPresenter.RummageConfig
  alias DentalBookPresenter.ClientAccountService
  alias DentalBookPresenter.UserRepo
  alias Rummage.Ecto

  def index(conn, params) do
    user_id = conn.assigns.current_user.id
    is_admin = conn.assigns.current_user.admin

    results_per_page = "5"
    rummage_default =  RummageConfig.get_default_params_from(params, results_per_page)

    {query_paginated, rummage} = UserRepo.get_rummage_user_query(user_id, is_admin, Repo) |> Rummage.Ecto.rummage(rummage_default)

    team_members =  Repo.all(query_paginated)

    render(conn, "index.html", team: team_members, rummage: rummage)
  end

  def new(conn, _params) do
    changeset = User.changeset(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"registration" => member_params}) do
    logged_user_id = conn.assigns.current_user.id
    collaborator_changeset = User.changeset(%User{}, member_params)

    case ClientAccountService.insert_new_collaborator(collaborator_changeset, logged_user_id) do
      {:ok, _collaborator_inserted} ->
        conn
        |> put_flash(:info, "Colaborador criado com sucesso.")
        |> redirect(to: team_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    collaborator = Repo.get!(User, id)

    changeset = User.changeset(collaborator)
    render(conn, "edit.html", collaborator: collaborator, changeset: changeset)
  end

  def update(conn, %{"id" => id, "registration" => member_params}) do

    collaborator = Repo.get!(User, id)
    changeset = User.changeset(collaborator, member_params)

    case Repo.update(changeset) do
      {:ok, _collaborator_updated} ->
        conn
        |> put_flash(:info, "Colaborador atualizado com sucesso!")
        |> redirect(to: team_path(conn, :index))
      {:error, changeset} ->
        render(conn, "edit.html", collaborator: collaborator, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    String.to_integer(id) |> delete_collaborator(conn.assigns.current_user.id, conn)
  end

  def get_dentists(conn, %{"query" => query}) do
    user_id = conn.assigns.current_user.id
    dentists = UserRepo.search_dentists(user_id, query, Repo)

    dentists_map = Enum.map(dentists, &get_dentist_data_of/1)

    json conn, dentists_map
  end

  defp get_dentist_data_of(dentist) do
    %{
      label_of_dentist_name: "<b>#{dentist.name}</b>", 
      dentist_name: dentist.name, 
      dentist_id: dentist.id,
      dentist_cro: dentist.cro
    }
  end

  defp delete_collaborator(id, logged_user, conn) when id == logged_user do
    conn
    |> put_flash(:error, "Não é possível se auto excluir.")
    |> redirect(to: team_path(conn, :index))
  end

  defp delete_collaborator(id, _logged_user, conn) do
    ClientAccountService.delete_collaborator_completly(id)

    conn
    |> put_flash(:info, "Colaborador excluído com sucesso.")
    |> redirect(to: team_path(conn, :index))
  end

end