defmodule DentalBookPresenter.MedicineController do
  use DentalBookPresenter.Web, :controller
  use Rummage.Phoenix.Controller

  alias DentalBookPresenter.Medicine
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.RummageConfig
  alias Rummage.Ecto

  def index(conn, params) do
    user_id = conn.assigns.current_user.id

    results_per_page = "5"
    rummage_default =  RummageConfig.get_default_params_from(params, results_per_page)

    {query_paginated, rummage} = 
    RummageConfig.get_rummage_query_with_tenant_name(Medicine, user_id)
    |> Rummage.Ecto.rummage(rummage_default)

    medicines = Apartmentex.all(Repo, query_paginated, Tenant.get_name(user_id, Repo))
    render(conn, "index.html", medicines: medicines, rummage: rummage)
  end

  def new(conn, _params) do
    changeset = Medicine.changeset(%Medicine{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"medicine" => medicine_params}) do
    changeset = Medicine.changeset(%Medicine{}, medicine_params)

    user_id = conn.assigns.current_user.id

    case Apartmentex.insert(Repo, changeset, Tenant.get_name(user_id, Repo)) do
      {:ok, _medicine} ->
        conn
        |> put_flash(:info, "Medicamento criado com sucesso.")
        |> redirect(to: medicine_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    user_id = conn.assigns.current_user.id
    medicine = Apartmentex.get!(Repo, Medicine, id, Tenant.get_name(user_id, Repo))
    changeset = Medicine.changeset(medicine)
    render(conn, "edit.html", medicine: medicine, changeset: changeset)
  end

  def update(conn, %{"id" => id, "medicine" => medicine_params}) do
    user_id = conn.assigns.current_user.id
    tenant_name = Tenant.get_name(user_id, Repo)
    medicine = Apartmentex.get!(Repo, Medicine, id, tenant_name)
    changeset = Medicine.changeset(medicine, medicine_params)

    case Apartmentex.update(Repo, changeset, tenant_name) do
      {:ok, medicine} ->
        conn
        |> put_flash(:info, "Medicamento atualizado com sucesso.")
        |> redirect(to: medicine_path(conn, :index))
      {:error, changeset} ->
        render(conn, "edit.html", medicine: medicine, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user_id = conn.assigns.current_user.id
    tenant_name = Tenant.get_name(user_id, Repo)
    
    medicine = Apartmentex.get!(Repo, Medicine, id, tenant_name)
    Apartmentex.delete!(Repo, medicine, tenant_name)

    conn
    |> put_flash(:info, "Medicamento excluído com sucesso.")
    |> redirect(to: medicine_path(conn, :index))
  end

  def get_medicines(conn, %{"query" => query}) do
    user_id = conn.assigns.current_user.id

    medicines = Apartmentex.all(Repo, get_medicines_from(query), Tenant.get_name(user_id, Repo))
    
    medicines_map = Enum.map(medicines, &get_medicine_data_of/1)

    json conn, medicines_map
  end

  defp get_medicines_from(search_term) do
    from medicine in Medicine,
    where: ilike(medicine.description, ^search_term)
  end

  defp get_medicine_data_of(medicine) do
    %{
      label_of_medicine_description: "<b>#{medicine.description}</b>", 
      medicine_id: medicine.id,
      medicine_description: medicine.description, 
      medicine_use_type: medicine.use_type,
      medicine_default_prescription: medicine.default_prescription
    }
  end
end
