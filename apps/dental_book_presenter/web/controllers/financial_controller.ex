defmodule DentalBookPresenter.FinancialController do
  use DentalBookPresenter.Web, :controller
  use Drab.Controller
  alias DentalBookPresenter.FinancialUI
  alias DentalBookPresenter.FinancialRepo

  def index(conn, _params) do
    user_id = conn.assigns.current_user.id

    {today_income, today_expense, today_profit} = FinancialRepo.get_billing_of_today(user_id)
    {week_income, week_expense, week_profit} = FinancialRepo.get_billing_of_week(user_id)
    {month_income, month_expense, month_profit} = FinancialRepo.get_billing_of_month(user_id)

    initial_value = Money.new(0) |> Money.to_string

    financial_data = %{
      daily_income: today_income,
      daily_expense: today_expense,
      daily_results: today_profit,
      weekly_income: week_income,
      weekly_expense: week_expense,
      weekly_results: week_profit,
      monthly_income: month_income,
      monthly_expense: month_expense,
      monthly_results: month_profit,
      custom_income: initial_value,
      custom_expense: initial_value,
      custom_results: initial_value
    }
    
    financial_changeset = %FinancialUI{} |> FinancialUI.changeset(financial_data)

    conn
    |> render("index.html", conn: conn, changeset: financial_changeset, action: financial_path(conn, :get_custom_financial)) 
  end

end