defmodule DentalBookPresenter.PatientController do
  use DentalBookPresenter.Web, :controller
  use Rummage.Phoenix.Controller

  alias DentalBookPresenter.Patient
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.RummageConfig
  alias DentalBookPresenter.PatientUI
  alias DentalBookPresenter.Appointment
  alias DentalBookPresenter.PatientRepo
  alias DentalBookPresenter.PatientClinicalHistory
  alias DentalBookPresenter.PatientFinancialHistory
  alias DentalBookPresenter.Anamnesis
  alias DentalBookPresenter.ReportGenerator
  alias DentalBookPresenter.DateFormat

  def index(conn, params) do
    user_id = conn.assigns.current_user.id

    results_per_page = "5"
    rummage_default =  RummageConfig.get_default_params_from(params, results_per_page)

    {query_paginated, rummage} = 
    RummageConfig.get_rummage_query_with_tenant_name(Patient, user_id)
    |> Rummage.Ecto.rummage(rummage_default)

    patients = Apartmentex.all(Repo, query_paginated, Tenant.get_name(user_id, Repo))
    |> PatientUI.get_patient_ui_from

    render(conn, "index.html", patients: patients, rummage: rummage)
  end

  def new(conn, _params) do
    changeset = PatientUI.changeset(%PatientUI{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"patient_ui" => patient_params}) do
    changeset = Patient.changeset(%Patient{}, patient_params)
    user_id = conn.assigns.current_user.id

    case Apartmentex.insert(Repo, changeset, Tenant.get_name(user_id, Repo)) do
      {:ok, _patient} ->
        conn
        |> put_flash(:info, "Paciente criado com sucesso.")
        |> redirect(to: patient_path(conn, :index))
      {:error, changeset} ->
        patient_ui_changeset = PatientUI.changeset(%PatientUI{}, patient_params)
        |> Map.put(:action, changeset.action)
        |> Map.put(:errors, changeset.errors)
        render(conn, "new.html", changeset: patient_ui_changeset)
    end
  end

  def show(conn, %{"id" => patient_id}) do
    user_id = conn.assigns.current_user.id

    patient = Apartmentex.get!(Repo, Patient, patient_id, Tenant.get_name(user_id, Repo))

    raw_patient_history = PatientRepo.get_history_of(patient_id, user_id)

    clinical_history = PatientClinicalHistory.get_data_in(raw_patient_history)
    financial_history = PatientFinancialHistory.get_data_in(raw_patient_history)
    anamnesis_history = PatientRepo.get_anamnesis_history(patient_id, user_id)
    sick_notes_history = PatientRepo.get_sick_notes_history(patient_id, user_id)
    prescriptions_history = PatientRepo.get_prescriptions_history(patient_id, user_id)

    patient_history = %{
      clinical_history: clinical_history,
      financial_history: financial_history,
      anamnesis_history: anamnesis_history,
      sick_notes_history: sick_notes_history,
      prescriptions_history: prescriptions_history
    }

    render(conn, "show.html", patient: patient, patient_history: patient_history)
  end

  def edit(conn, %{"id" => id}) do
    user_id = conn.assigns.current_user.id

    patient = Apartmentex.get!(Repo, Patient, id, Tenant.get_name(user_id, Repo))
    |> PatientUI.get_patient_ui_from

    changeset = PatientUI.changeset(patient)
    render(conn, "edit.html", patient: patient, changeset: changeset)
  end

  def update(conn, %{"id" => id, "patient_ui" => patient_params}) do
    user_id = conn.assigns.current_user.id
    patient = Apartmentex.get!(Repo, Patient, id, Tenant.get_name(user_id, Repo))
    changeset = Patient.changeset(patient, patient_params)

    case Apartmentex.update(Repo, changeset, Tenant.get_name(user_id, Repo)) do
      {:ok, _patient} ->
        conn
        |> put_flash(:info, "Paciente atualizado com sucesso.")
        |> redirect(to: patient_path(conn, :index))
      {:error, changeset} ->
        patient_ui_changeset = PatientUI.changeset(%PatientUI{}, patient_params)
        |> Map.put(:action, changeset.action)
        |> Map.put(:errors, changeset.errors)
        render(conn, "edit.html", patient: patient, changeset: patient_ui_changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user_id = conn.assigns.current_user.id
    
    Apartmentex.all(Repo, search_patient_by(id), Tenant.get_name(user_id, Repo))
    |> patient_to_be_deleted(conn, user_id)
  end

  defp patient_to_be_deleted([], conn, _user_id) do
    conn
    |> put_flash(:error, "Paciente não pode ser excluído pois já está vinculado a um plano de tratamento.")
    |> redirect(to: patient_path(conn, :index))
  end

  defp patient_to_be_deleted([%Patient{}] = patients, conn, user_id) do
    first_position = 0
    {:ok, patient} = patients |> Enum.fetch(first_position)

    Apartmentex.delete(Repo, patient, Tenant.get_name(user_id, Repo))

    conn
    |> put_flash(:info, "Paciente excluído com sucesso.")
    |> redirect(to: patient_path(conn, :index))
  end

  def search_patient_by(patient_id) do
    from p in Patient,
    left_join: a in Appointment, 
    on: p.id == a.patient_id, 
    where: p.id == ^patient_id
    and is_nil(a.patient_id),
    select: p
  end

  def get_patients(conn, %{"query" => query}) do
    user_id = conn.assigns.current_user.id
    patients = search_patients(user_id, query)

    patients_map = Enum.map(patients, fn patient -> %{label_of_patient_name: "<b>#{patient.name}</b>", 
                                                   patient_name: patient.name, 
                                                   patient_cellphone: patient.cellphone, 
                                                   patient_id: patient.id} 
                                                   end)

    json conn, patients_map
  end

  def new_anamnesis(conn, %{"patient_id" => patient_id}) do
    user_id = conn.assigns.current_user.id
    patient = Apartmentex.get!(Repo, Patient, patient_id, Tenant.get_name(user_id, Repo))
    changeset = Anamnesis.changeset(%Anamnesis{})
    render(conn, "anamnesis.html", changeset: changeset, patient: patient)
  end

  def save_anamnesis(conn, %{"patient_id" => patient_id, "anamnesis" => anamnesis} ) do
    user_id = conn.assigns.current_user.id
    anamnesis_responses = %{responses: anamnesis}
    changeset = Anamnesis.changeset(%Anamnesis{}, anamnesis_responses)
    saved_anamnesis = Apartmentex.insert!(Repo, changeset, Tenant.get_name(user_id, Repo))
    patient = Apartmentex.get!(Repo, Patient, patient_id, Tenant.get_name(user_id, Repo))
    render(conn, "show_anamnesis.html", patient: patient, answered_anamnesis: saved_anamnesis, conn: conn)
  end

  def show_anamnesis(conn, %{"patient_id" => patient_id, "anamnesis_id" => anamnesis_id}) do
    user_id = conn.assigns.current_user.id
    patient = Apartmentex.get!(Repo, Patient, patient_id, Tenant.get_name(user_id, Repo))
    answered_anamnesis = PatientRepo.get_answered_anamnesis(anamnesis_id, user_id)

    render(conn, "show_anamnesis.html", patient: patient, answered_anamnesis: answered_anamnesis, conn: conn)
  end

  def print_anamnesis(conn, %{"patient_id" => patient_id, "params" => params}) do
    user_id = conn.assigns.current_user.id

    anamnesis_id = extract_values_of_query_string_from(params)
    patient = Apartmentex.get!(Repo, Patient, patient_id, Tenant.get_name(user_id, Repo))
    answered_anamnesis = PatientRepo.get_answered_anamnesis(anamnesis_id, user_id)

    template_params = get_template_params(patient, answered_anamnesis)

    anamnesis_file_params = [file_name: "anamnese", template_path: get_template_path(), template_params: template_params]
    
    {filename, pdf_content} = ReportGenerator.create_pdf(anamnesis_file_params)

    conn
    |> put_resp_content_type("application/pdf")
    |> put_resp_header("content-disposition", "attachment; filename=#{filename}")
    |> send_resp(200, pdf_content)
  end

  defp search_patients(user_id, search_term) do
     query = from p in Patient, 
     where: ilike(p.name, ^search_term),
     or_where: ilike(p.cellphone, ^search_term),
     select: %{id: p.id, name: p.name, cellphone: p.cellphone}
     
     Apartmentex.all(Repo, query, Tenant.get_name(user_id, Repo))
  end

  defp extract_values_of_query_string_from(params) do
    find_everything_till_equal_sign = ~r/.+?=/
    params 
    |> String.replace("&", "") 
    |> String.replace(find_everything_till_equal_sign, "")
  end
  defp get_template_path(), do: "/templates/dental_anamnesis/anamnesis_report.html.eex"

  defp get_template_params(patient, answered_anamnesis) do
    [
      answered_anamnesis: answered_anamnesis, 
      patient: patient, 
      phoenix_view: Phoenix.View, 
      patient_view: DentalBookPresenter.PatientView, 
      phoenix_html: Phoenix.HTML,
      emission_date: DateFormat.now() |> DateFormat.ecto_date_to_string,
      creation_date: answered_anamnesis.inserted_at |> DateFormat.naive_date_to_ecto_date |> DateFormat.ecto_date_to_string
    ]
  end

end
