defmodule DentalBookPresenter.AppointmentUpdaterController do 
  use DentalBookPresenter.Web, :controller
  alias DentalBookPresenter.AppointmentUI
  alias DentalBookPresenter.AppointmentService
  alias DentalBookPresenter.Tenant

  @empty_message ""
  @procedure_card_message_error "Não é possível salvar um plano de tratamento sem pelo menos um procedimento."

  def edit(conn, id) do
    tenant_name = Tenant.get_name(conn.assigns.current_user.id, Repo)

    appointment = get_complete_appointment_ui_by(id, tenant_name)

    changeset = AppointmentUI.changeset(appointment)
    procedures_quantity = AppointmentUI.get_how_many_procedures_exists_in(appointment)
    render(conn, "edit.html", appointment: appointment, changeset: changeset, procedures_quantity: procedures_quantity )
  end

  def update(conn, id, appointment_params) do
    tenant_name = Tenant.get_name(conn.assigns.current_user.id, Repo)

    update_when_appointment_is_valid(conn, tenant_name, id, appointment_params)
  end

  defp update_when_appointment_is_valid(conn, tenant_name, id, %{"procedures_incomes" => procedures} = appointment_params) do
    case AppointmentService.update_appointment_procedures_incomes(tenant_name, id, appointment_params) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Plano de tratamento atualizado com sucesso!!!")
        |> redirect(to: appointment_path(conn, :index))
      {:error, _changeset} ->
        render_error_message_for_edition(conn, id, tenant_name, @empty_message)
    end
  end

  defp update_when_appointment_is_valid(conn, tenant_name, id, appointment_params) do
    render_error_message_for_edition(conn, id, tenant_name, @procedure_card_message_error)
  end

  defp render_error_message_for_edition(conn, id, tenant_name, error_message) do
    appointment = get_complete_appointment_ui_by(id, tenant_name)

    changeset = AppointmentUI.changeset(appointment) |> put_action_if_needed
    procedures_quantity = AppointmentUI.get_how_many_procedures_exists_in(appointment)

    conn
    |> put_flash(:error, error_message)
    |> render("edit.html", changeset: changeset, appointment: appointment, procedures_quantity: procedures_quantity)
  end

  defp get_complete_appointment_ui_by(id, tenant_name) do
    AppointmentService.get_by(id, tenant_name)
    |> AppointmentUI.get_complete_appointment_ui_from
  end

  defp put_action_if_needed(%{action: action} = changeset) when is_nil(action), do: Map.put(changeset, :action, :invalid_params)
  defp put_action_if_needed(changeset), do: changeset

end