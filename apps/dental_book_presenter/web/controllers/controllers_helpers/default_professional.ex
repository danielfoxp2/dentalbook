defmodule DentalBookPresenter.DefaultProfessional do
  alias DentalBookPresenter.Repo
  alias DentalBookPresenter.User
  alias DentalBookPresenter.UserRepo

  @health_professional "Profissional de Saúde"

  def get(logged_user) do
    get_default_professional(logged_user)
  end

  defp get_default_professional(%User{role: role} = logged_user) when role == @health_professional, do: logged_user
  defp get_default_professional(logged_user), do: UserRepo.search_dentists(logged_user.id, "%%", Repo) |> List.first
end