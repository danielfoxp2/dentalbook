defmodule DentalBookPresenter.AppointmentCreatorController do
  use DentalBookPresenter.Web, :controller
  alias DentalBookPresenter.AppointmentUI
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.AppointmentService
  alias DentalBookPresenter.User
  alias DentalBookPresenter.UserRepo
  alias DentalBookPresenter.Appointment
  alias DentalBookPresenter.DefaultProfessional

  @empty_message ""
  @patient_should_be_informed "Paciente deve ser informado."
  @dentist_should_be_informed "Dentista deve ser informado."
  @procedure_card_message_error "Não é possível salvar um plano de tratamento sem pelo menos um procedimento."
  

  def new(conn, patient) do
    dentist = conn.assigns.current_user
    |> DefaultProfessional.get

    changeset = AppointmentUI.changeset(%AppointmentUI{}, AppointmentUI.get_default_params)
    render(conn, "new.html", changeset: changeset, appointment: Appointment, patient: patient, dentist: dentist)
  end

  def new(conn) do
    dentist = conn.assigns.current_user
    |> DefaultProfessional.get

    changeset = AppointmentUI.changeset(%AppointmentUI{}, AppointmentUI.get_default_params)
    render(conn, "new.html", changeset: changeset, appointment: Appointment, patient: nil, dentist: dentist)
  end

  def create(conn, appointment_params) do
    tenant_name = Tenant.get_name(conn.assigns.current_user.id, Repo)

    AppointmentUI.changeset_of_appointment(%AppointmentUI{}, appointment_params)
    |> insert_when_appointment_is_valid(conn, tenant_name, appointment_params)
  end

  defp insert_when_appointment_is_valid(%{valid?: is_valid}, conn, tenant_name, %{"procedures_incomes" => _procedures} = appointment_params) when is_valid == true do
    case AppointmentService.insert_appointment_procedures_incomes(tenant_name, appointment_params) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Plano de tratamento criado com sucesso!!!")
        |> redirect(to: appointment_path(conn, :index))
      {:error, changeset} ->
        render_error_message(changeset, conn, @empty_message)
    end
  end

  defp insert_when_appointment_is_valid(changeset, conn, _, %{"patient_id" => patient_id}) when patient_id == "" do
    render_error_message(changeset, conn, @patient_should_be_informed)
  end

  defp insert_when_appointment_is_valid(changeset, conn, _, %{"dentist_id" => dentist_id}) when dentist_id == "" do
    render_error_message(changeset, conn, @dentist_should_be_informed)
  end

  defp insert_when_appointment_is_valid(changeset, conn, _, _) do
     render_error_message(changeset, conn, @procedure_card_message_error)
  end

  defp render_error_message(changeset, conn, error_message) do
    changeset = changeset |> put_action_if_needed
    
    dentist = conn.assigns.current_user
    |> DefaultProfessional.get

    conn
    |> put_flash(:error, error_message)
    |> render("new.html", changeset: changeset, appointment: Appointment, patient: nil, dentist: dentist)
  end

  defp put_action_if_needed(%{action: action} = changeset) when is_nil(action), do: Map.put(changeset, :action, :invalid_params)
  defp put_action_if_needed(changeset), do: changeset

end