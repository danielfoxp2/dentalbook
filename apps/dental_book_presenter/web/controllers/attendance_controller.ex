defmodule DentalBookPresenter.AttendanceController do
  use DentalBookPresenter.Web, :controller
  alias DentalBookPresenter.AttendanceUI
  alias DentalBookPresenter.AttendanceRepo
  alias DentalBookPresenter.Appointment
  alias DentalBookPresenter.Tenant

  def index(conn, _params) do
    user_id = conn.assigns.current_user.id

    scheduled_patients = AttendanceRepo.get_scheduled_patients_of_the_week(user_id)
    |> AttendanceUI.get_attendance_ui_from

    tenant_name = Tenant.get_name(user_id, Repo)
    
    not_have_appointments = Apartmentex.all(Repo, Appointment, tenant_name)
    |> Enum.empty?

    render(conn, "index.html", scheduled_patients: scheduled_patients, not_have_appointments: not_have_appointments)
  end
end 