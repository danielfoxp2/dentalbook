defmodule DentalBookPresenter.SickNoteTemplateController do
  use DentalBookPresenter.Web, :controller

  alias DentalBookPresenter.SickNoteTemplate
  alias DentalBookPresenter.Tenant

  def index(conn, _params) do
    user_id = conn.assigns.current_user.id

    sick_note_templates = Apartmentex.all(Repo, SickNoteTemplate, Tenant.get_name(user_id, Repo))

    render(conn, "index.html", sick_note_templates: sick_note_templates)
  end

  def new(conn, _params) do
    changeset = SickNoteTemplate.changeset(%SickNoteTemplate{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"sick_note_template" => sick_note_template_params}) do
    user_id = conn.assigns.current_user.id
    changeset = SickNoteTemplate.changeset(%SickNoteTemplate{}, sick_note_template_params)

    case Apartmentex.insert(Repo, changeset, Tenant.get_name(user_id, Repo)) do
      {:ok, _sick_note_template} ->
        conn
        |> put_flash(:info, "Modelo de atestado criado com sucesso.")
        |> redirect(to: sick_note_template_path(conn, :index))
      {:error, changeset} ->
        sick_template_changeset = SickNoteTemplate.changeset(%SickNoteTemplate{}, sick_note_template_params)
        |> Map.put(:action, changeset.action)
        |> Map.put(:errors, changeset.errors)
        render(conn, "new.html", changeset: sick_template_changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    user_id = conn.assigns.current_user.id
    
    sick_note_template = Apartmentex.get!(Repo, SickNoteTemplate, id, Tenant.get_name(user_id, Repo))
    changeset = SickNoteTemplate.changeset(sick_note_template)
    render(conn, "edit.html", sick_note_template: sick_note_template, changeset: changeset)
  end

  def update(conn, %{"id" => id, "sick_note_template" => sick_note_template_params}) do
    user_id = conn.assigns.current_user.id

    sick_note_template = Apartmentex.get!(Repo, SickNoteTemplate, id, Tenant.get_name(user_id, Repo))
    changeset = SickNoteTemplate.changeset(sick_note_template, sick_note_template_params)

    case Apartmentex.update(Repo, changeset, Tenant.get_name(user_id, Repo)) do
      {:ok, sick_note_template} ->
        conn
        |> put_flash(:info, "Modelo de atestado atualizado com sucesso.")
        |> redirect(to: sick_note_template_path(conn, :index))
      {:error, changeset} ->
        sick_note_template_changeset = SickNoteTemplate.changeset(%SickNoteTemplate{}, sick_note_template_params)
        |> Map.put(:action, changeset.action)
        |> Map.put(:errors, changeset.errors)
        render(conn, "edit.html", sick_note_template: sick_note_template, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user_id = conn.assigns.current_user.id
    
    sick_note_template = Apartmentex.get!(Repo, SickNoteTemplate, id, Tenant.get_name(user_id, Repo))
    Apartmentex.delete!(Repo, sick_note_template, Tenant.get_name(user_id, Repo))

    conn
    |> put_flash(:info, "Modelo de atestado foi removido com sucesso.")
    |> redirect(to: sick_note_template_path(conn, :index))
  end
end
