defmodule DentalBookPresenter.ProcedureController do
  use DentalBookPresenter.Web, :controller
  use Rummage.Phoenix.Controller

  alias DentalBookPresenter.Procedure
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.RummageConfig
  alias DentalBookPresenter.AppointmentProcedure
  alias Rummage.Ecto

  def index(conn, params) do
    user_id = conn.assigns.current_user.id

    results_per_page = "5"
    rummage_default =  RummageConfig.get_default_params_from(params, results_per_page)

    {query_paginated, rummage} = 
    RummageConfig.get_rummage_query_with_tenant_name(Procedure, user_id)
    |> Rummage.Ecto.rummage(rummage_default)

    procedures = Apartmentex.all(Repo, query_paginated, Tenant.get_name(user_id, Repo))
    render(conn, "index.html", procedures: procedures, rummage: rummage)
  end

  def new(conn, _params) do
    changeset = Procedure.changeset(%Procedure{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"procedure" => procedure_params}) do
    changeset = Procedure.changeset(%Procedure{}, procedure_params)
    
    user_id = conn.assigns.current_user.id

    case Apartmentex.insert(Repo, changeset, Tenant.get_name(user_id, Repo)) do
      {:ok, _procedure} ->
        conn
        |> put_flash(:info, "Procedimento criado com sucesso!")
        |> redirect(to: procedure_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    user_id = conn.assigns.current_user.id
    procedure = Apartmentex.get!(Repo, Procedure, id, Tenant.get_name(user_id, Repo))
    changeset = Procedure.changeset(procedure)
    render(conn, "edit.html", procedure: procedure, changeset: changeset)
  end

  def update(conn, %{"id" => id, "procedure" => procedure_params}) do
    user_id = conn.assigns.current_user.id
    procedure = Apartmentex.get!(Repo, Procedure, id, Tenant.get_name(user_id, Repo))
    changeset = Procedure.changeset(procedure, procedure_params)

    case Apartmentex.update(Repo, changeset, Tenant.get_name(user_id, Repo)) do
      {:ok, _procedure} ->
        conn
        |> put_flash(:info, "Procedimento atualizado com sucesso!")
        |> redirect(to: procedure_path(conn, :index))
      {:error, changeset} ->
        render(conn, "edit.html", procedure: procedure, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user_id = conn.assigns.current_user.id

    Apartmentex.all(Repo, search_procedure_without_appointment(id), Tenant.get_name(user_id, Repo)) 
    |> procedure_to_be_deleted(conn, user_id)
  end

  defp procedure_to_be_deleted([], conn, _user_id) do
    conn
    |> put_flash(:error, "Procedimento não pode ser excluído pois já está sendo utilizado em um plano de tratamento.")
    |> redirect(to: procedure_path(conn, :index))
  end

  defp procedure_to_be_deleted([%Procedure{}] = procedures, conn, user_id) do
    first_position = 0
    {:ok, procedure} = procedures |> Enum.fetch(first_position)

    Apartmentex.delete(Repo, procedure, Tenant.get_name(user_id, Repo))

    conn
    |> put_flash(:info, "Procedimento excluído com sucesso.")
    |> redirect(to: procedure_path(conn, :index))
  end

  def get_procedures(conn, %{"query" => query}) do

    user_id = conn.assigns.current_user.id
    procedures = search_procedures(user_id, query)

    procedures_map = Enum.map(procedures, fn procedure -> %{label_of_procedure_name: "<b>#{procedure.name}</b>", 
                                                            procedure_name: procedure.name, 
                                                            procedure_price: Money.to_string(procedure.price), 
                                                            procedure_id: procedure.id} 
                                                            end)

    json conn, procedures_map
  end

   defp search_procedures(user_id, search_term) do
     query = from p in Procedure, 
     where: ilike(p.name, ^search_term),
     select: %{id: p.id, name: p.name, price: p.price}
     
     Apartmentex.all(Repo, query, Tenant.get_name(user_id, Repo))
  end

  defp search_procedure_without_appointment(procedure_id) do
    from p in Procedure,
    left_join: ap in AppointmentProcedure, 
    on: p.id == ap.procedure_id, 
    where: p.id == ^procedure_id
    and is_nil(ap.procedure_id),
    select: p
  end


end
