defmodule DentalBookPresenter.SickNoteController do
  use DentalBookPresenter.Web, :controller
  alias DentalBookPresenter.SickNote
  alias DentalBookPresenter.SickNoteTemplate
  alias DentalBookPresenter.Patient
  alias DentalBookPresenter.Tenant
  alias DentalBookPresenter.DefaultProfessional
  alias DentalBookPresenter.ReportGenerator

  def index(conn, %{"patient_id" => patient_id}) do
    current_user = conn.assigns.current_user
    tenant_name = Tenant.get_name(current_user.id, Repo)
    changeset = SickNote.changeset(%SickNote{})
    dentist = current_user |> DefaultProfessional.get

    patient = Apartmentex.get(Repo, Patient, patient_id, tenant_name)
    action = patient_sick_note_path(conn, :create_sick_note, patient)

    sick_notes_templates = Apartmentex.all(Repo, SickNoteTemplate, tenant_name)

    sick_notes_templates_as_json = get_as_json(sick_notes_templates)

    view_params = %{
      dentist: dentist,
      patient: patient,
      changeset: changeset,
      action: action,
      sick_notes_templates: sick_notes_templates,
      sick_notes_templates_as_json: sick_notes_templates_as_json
    }

    render(conn, "index.html", assigns: view_params) 
  end

  def show_sick_note(conn, params) do
    render(conn, "show_sick_note.html", sick_note: params, conn: conn) 
  end

  def create_sick_note(conn, %{"params" => params, "patient_id" => patient_id}) do
    params = Map.update!(params, "sick_note_content", &adjust_end_of_lines(&1))   

    save_sick_note_to_maintain_history_of(patient_id, params, conn.assigns.current_user.id)

    sick_note_data = get_sick_note_data(params, patient_id)

    template_params = [conn: conn, sick_note_data: sick_note_data]
    sick_note_file_params = get_sick_note_file_params(template_params)

    {filename, pdf_content} = ReportGenerator.create_pdf(sick_note_file_params)

    conn
    |> put_resp_content_type("application/pdf")
    |> put_resp_header("content-disposition", "attachment; filename=#{filename}")
    |> send_resp(200, pdf_content)
  end

  defp get_as_json(sick_notes_templates) do
    sick_notes_templates
    |> parse_to_map
    |> parse_to_json
  end

  defp parse_to_map(sick_note_templates) do
    Enum.map(sick_note_templates, fn sick_note_template ->
      %{
        id: sick_note_template.id, 
        title: sick_note_template.title,
        content: sick_note_template.content
      }
    end)
  end

  defp parse_to_json(sick_note_templates_as_map) do
    sick_note_templates_as_map
    |> Poison.encode
    |> get_json
  end

  defp get_json({:ok, json}), do: json
  defp get_json(_from_map), do: []

  defp get_sick_note_data(params, patient_id) do
  %{
    patient_id: patient_id,
    patient_name: params["patient_name"],
    dentist_name: params["dentist_name"],
    dentist_cro: params["dentist_cro"],
    sick_note_content: params["sick_note_content"]
  }
  end

  defp adjust_end_of_lines(content), do: String.replace(content, "\n", "</br>")

  defp get_sick_note_file_params(template_params) do
  [
    file_name: "atestado", 
    template_path: get_template_path(), 
    partial_css_template_dir: "/static/css", 
    template_params: template_params
  ]
  end
  defp get_template_path(), do: "/templates/sick_note/sick_note_report.html.eex"

  defp save_sick_note_to_maintain_history_of(patient_id, params, user_id) do
    tenant_name = Tenant.get_name(user_id, Repo)
    params_with_patient_id = Map.put(params, "patient_id", patient_id)
    changeset = SickNote.changeset(%SickNote{}, params_with_patient_id)

    Apartmentex.insert!(Repo, changeset, tenant_name)
  end

end