defmodule DentalBookPresenter.AttendanceUI do
  use DentalBookPresenter.Web, :model
  alias DentalBookPresenter.DateFormat
  alias DentalBookPresenter.TimeFormat
  alias DentalBookPresenter.AttendanceUI

  embedded_schema do
    field :appointment_id, :string
    field :patient_name, :string
    field :patient_cellphone, :string
    field :attendance_date, :string
    field :attendance_hour, :string
  end

  def get_attendance_ui_from(attendances) do
    %{
       for_today: today_schedule(attendances),
       for_rest_of_week: rest_of_week_schedule(attendances)
    }
  end

  defp today_schedule(attendances) do
    only_today_scheduled = Enum.filter attendances, fn attendance -> 
      attendance.attendance_date == DateFormat.now
    end
    
    Enum.map only_today_scheduled, fn today_attendance -> 
      get_attendance_from(today_attendance)
    end
  end

  defp rest_of_week_schedule(attendances) do
    week_scheduled = Enum.filter attendances, fn attendance -> 
      attendance.attendance_date != DateFormat.now
    end
    
    Enum.map week_scheduled, fn attendance -> 
      get_attendance_from(attendance)
    end
  end

  defp get_attendance_from(attendance) do
    %AttendanceUI{
      appointment_id: Map.get(attendance, :appointment_id),
      patient_name: Map.get(attendance, :patient_name),
      patient_cellphone: Map.get(attendance, :patient_cellphone),
      attendance_date: Map.get(attendance, :attendance_date) |> DateFormat.ecto_date_to_string,
      attendance_hour: Map.get(attendance, :attendance_hour) |> TimeFormat.to_string
    }
  end

end