defmodule DentalBookPresenter.IncomeUI do
  use DentalBookPresenter.Web, :model
  alias DentalBookPresenter.IncomeUI
  alias DentalBookPresenter.DateFormat

  embedded_schema do
    field :description, :string
    field :created_at, :string
    field :due_date_at, :string
    field :paid_at, :string
    field :value, :string
    field :patient_name, :string
  end

  def changeset(struct, params \\ %{}) do
   struct
    |> cast(params, [:description, :created_at, :due_date_at, :paid_at, :value])
    |> validate_required([:description, :created_at, :due_date_at, :value])
  end

  def get_income_ui_from(appointments, incomes) when is_list(incomes) do
    Enum.map(incomes, fn income -> 
      get_income_ui_from(appointments, income)
    end)
  end

  def get_income_ui_from(appointments, income) do
    patient_name = get_patient_name_of(income, appointments)
    get_income_ui_from(income) |> Map.put(:patient_name, patient_name)
  end

  def get_income_ui_from(income) do
    %IncomeUI{
      id: Map.get(income, :id),
      description: Map.get(income, :description),
      created_at: Map.get(income, :created_at) |> DateFormat.ecto_date_to_string,
      due_date_at: Map.get(income, :due_date_at) |> DateFormat.ecto_date_to_string,
      paid_at: Map.get(income, :paid_at) |> DateFormat.ecto_date_to_string,
      value: Map.get(income, :value)
    }
  end
  
  defp get_patient_name_of(income, appointments) do
    appointments
    |> get_appointment_of(income)
    |> get_patient_name
  end

  defp get_appointment_of(appointments, income) do
    Enum.find appointments, fn appointment -> 
      income.appointment_procedure_id == appointment.appointment_procedure_id
    end
  end

  defp get_patient_name(appointment) when is_nil(appointment), do: "Receita Avulsa"
  defp get_patient_name(appointment), do: appointment.patient_name

end