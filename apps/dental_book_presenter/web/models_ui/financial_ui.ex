defmodule DentalBookPresenter.FinancialUI do
  use DentalBookPresenter.Web, :model
  alias DentalBookPresenter.FinancialStatus

  embedded_schema do
    field :daily_income, :string
    field :daily_expense, :string
    field :daily_results, :string
    field :weekly_income, :string
    field :weekly_expense, :string
    field :weekly_results, :string
    field :monthly_income, :string
    field :monthly_expense, :string
    field :monthly_results, :string
    field :custom_begin_date, :string
    field :custom_end_date, :string
    field :custom_income, :string
    field :custom_expense, :string
    field :custom_results, :string
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, 
            [
             :daily_income,
             :daily_expense,
             :daily_results,
             :weekly_income,
             :weekly_expense,
             :weekly_results,
             :monthly_income,
             :monthly_expense,
             :monthly_results,
             :custom_income,
             :custom_expense,
             :custom_results
            ])
  end

end