defmodule DentalBookPresenter.PatientUI do
  use DentalBookPresenter.Web, :model
  alias DentalBookPresenter.PatientUI
  alias DentalBookPresenter.DateFormat

  embedded_schema do
    field :name, :string
    field :cellphone, :string
    field :gender, :string
    field :birth_date, :string
    field :age, :string
    field :cpf, :string
    field :origin, :string
  end

  def changeset(struct, params \\ %{}) do
   struct
    |> cast(params, [:name, :cellphone, :gender, :birth_date, :age, :cpf, :origin])
    |> validate_required([:name, :cellphone, :gender, :birth_date])
  end

  def get_patient_ui_from(patients) when is_list(patients) do
    Enum.map(patients, &get_patient_ui_from/1)
  end

  def get_patient_ui_from(patient) do
    %PatientUI{
      id: Map.get(patient, :id),
      name: Map.get(patient, :name),
      cellphone: Map.get(patient, :cellphone),
      gender: Map.get(patient, :gender),
      birth_date: Map.get(patient, :birth_date) |> DateFormat.ecto_date_to_string,
      age: "#{Map.get(patient, :age)} anos",
      cpf: Map.get(patient, :cpf),
      origin: Map.get(patient, :origin)
    }
  end
end