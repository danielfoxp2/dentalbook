defmodule DentalBookPresenter.ExpenseUI do
  use DentalBookPresenter.Web, :model
  alias DentalBookPresenter.ExpenseUI
  alias DentalBookPresenter.DateFormat

  embedded_schema do
    field :description, :string
    field :due_date_at, :string
    field :paid_at, :string
    field :value, :string
  end

  def changeset(struct, params \\ %{}) do
   struct
    |> cast(params, [:description, :due_date_at, :paid_at, :value])
    |> validate_required([:description, :due_date_at, :value])
  end

  def get_expense_ui_from(expenses) when is_list(expenses) do
    Enum.map(expenses, &get_expense_ui_from/1)
  end

  def get_expense_ui_from(expense) do
    %ExpenseUI{
      id: Map.get(expense, :id),
      description: Map.get(expense, :description),
      due_date_at: Map.get(expense, :due_date_at) |> DateFormat.ecto_date_to_string,
      paid_at: Map.get(expense, :paid_at) |> DateFormat.ecto_date_to_string,
      value: Map.get(expense, :value)
    }
  end

end