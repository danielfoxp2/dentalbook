defmodule DentalBookPresenter.AppointmentUI do
  use DentalBookPresenter.Web, :model
  
  alias DentalBookPresenter.AppointmentUI
  alias DentalBookPresenter.DateFormat
  alias DentalBookPresenter.IncomeBudget
  alias DentalBookPresenter.IncomeCreationDate
  alias DentalBookPresenter.TimeFormat

  embedded_schema do
    field :patient_id, :integer
    field :patient_name, :string
    field :patient_cellphone, :string
    field :dentist_id, :string
    field :dentist_name, :string
    field :invoice_value, :string

    embeds_many :procedures_incomes, ProcedureIncome do
      field :procedure_id, :integer
      field :procedure_name, :string
      field :income_id, :integer #verificar necessidade de existência
      field :description, :string
      field :price, :float
      field :due_date_at, :string
      field :done, :boolean
      field :paid_at, :string
      field :attendance_date_at, :string
      field :attendance_hour, :string
      field :procedure_tooth, :string
      field :procedure_tooth_faces, :string
    end

    embeds_many :observations, Observation do
      field :observation_id, :string
      field :observation, :string
      field :creation_date, :string
      field :creation_hour, :string
    end

  end

  def get_appointment_ui_from(appointments) when is_list(appointments) do
    Enum.map(appointments, &get_appointment_ui_from/1)
  end

  def get_appointment_ui_from(appointment) do
    %AppointmentUI{
      id: appointment.id,
      patient_id: appointment.patient_id,
      patient_name: appointment.patient.name,
      patient_cellphone: appointment.patient.cellphone
    }
  end

  def get_complete_appointment_ui_from({appointment, appointment_observations}) do 
    appointment_header = appointment |> List.first

    %AppointmentUI{
      id: appointment_header.id,
      patient_id: appointment_header.patient_id,
      dentist_id: appointment_header.dentist_id,
      patient_name: appointment_header.patient_name,
      dentist_name: appointment_header.dentist_name,
      patient_cellphone: appointment_header.patient_cellphone,
      invoice_value: IncomeBudget.get_total_of(appointment),
      procedures_incomes: get_procedures_incomes_ui_from(appointment),
      observations: get_observations_ui_from(appointment_observations)
    }
  end

  def get_how_many_procedures_exists_in(appointment_ui) do
    Enum.count appointment_ui.procedures_incomes
  end

  defp get_procedures_incomes_ui_from(appointment) do
    Enum.map appointment, fn appointment_procedure_income ->
      %AppointmentUI.ProcedureIncome{
        procedure_id: appointment_procedure_income.procedure_id,
        procedure_name: appointment_procedure_income.procedure_name,
        procedure_tooth: appointment_procedure_income.procedure_tooth |> get_tooth_value, 
        procedure_tooth_faces: appointment_procedure_income.procedure_tooth_faces |> get_tooth_value,
        done: appointment_procedure_income.done,
        description: appointment_procedure_income.description,
        price: appointment_procedure_income.price,
        due_date_at: DateFormat.ecto_date_to_string(appointment_procedure_income.due_date_at),
        paid_at: DateFormat.ecto_date_to_string(appointment_procedure_income.paid_at),
        attendance_date_at: DateFormat.ecto_date_to_string(appointment_procedure_income.attendance_date_at),
        attendance_hour: TimeFormat.to_string(appointment_procedure_income.attendance_hour)
      }
    end
  end

  defp get_observations_ui_from(appointment_observations) do
    Enum.map appointment_observations, fn appointment_observation ->
      %AppointmentUI.Observation{
        observation_id: appointment_observation.observation_id,
        observation: appointment_observation.observation,
        creation_date: appointment_observation.creation_date,
        creation_hour: appointment_observation.creation_hour
      }
    end
  end

  defp get_tooth_value(tooth_data) when is_nil(tooth_data), do: tooth_data
  defp get_tooth_value(tooth_data) do
    tooth_data |> String.split(",")
  end

  def get_procedures_params_from(procedures_incomes) do
    Enum.map(procedures_incomes, &get_procedure_details/1)
  end

  defp get_procedure_details(procedure_income) do
    {_, procedure} = procedure_income

    %{ 
       id: procedure["procedure_id"], 
       is_done: procedure["done"], 
       attendance_date_at: procedure["attendance_date_at"], 
       attendance_hour: procedure["attendance_hour"],
       procedure_tooth: procedure["procedure_tooth"],
       procedure_tooth_faces: procedure["procedure_tooth_faces"]
     }
  end

  def get_observations_from(appointment_observations, _) when is_nil(appointment_observations), do: []
  def get_observations_from(appointment_observations, appointment_id) do
    Enum.map(appointment_observations, &get_observation_details(&1, appointment_id))
  end

  defp get_observation_details(appointment_observation, appointment_id) do
    {_, observation_params} = appointment_observation
    
    %{
      observation_id: observation_params["observation_id"],
      appointment_id: appointment_id,
      observation: observation_params["observation"],
      creation_date: observation_params["creation_date"],
      creation_hour: observation_params["creation_hour"]
    }
  end

  def get_incomes_params_from(procedure_income, appointment_procedure_id) do
    get_income_details(procedure_income, appointment_procedure_id) 
  end

  defp get_income_details(procedure, appointment_procedure_id) do  
    attendance_date = procedure["attendance_date_at"]
    paid_at = procedure["paid_at"]
 
    %{
      "description" => get_income_name(procedure),
      "created_at" => IncomeCreationDate.create(attendance_date, paid_at), 
      "due_date_at" => procedure["due_date_at"],
      "paid_at" => procedure["paid_at"],
      "value" => procedure["price"],
      "appointment_procedure_id" => appointment_procedure_id
    }
  end

  defp get_income_name(procedure_params) do
    if income_description_exists?(procedure_params) do
      procedure_params["description"]
    else
      procedure_params["procedure_name"]
    end
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:patient_id, :dentist_id])
    |> validate_required([:patient_id, :dentist_id])
    |> cast_embed(:procedures_incomes, with: &procedure_income_changeset/2)
    |> cast_embed(:observations, with: &observations_changeset/2)
  end

  def changeset_of_appointment(struct, params \\ %{}) do
    struct
    |> cast(params, [:patient_id, :dentist_id])
    |> validate_required([:patient_id, :dentist_id])
  end

  def get_default_params do
    %{
      procedures_incomes: [
        %{price: nil, due_date_at: nil, done: :false, paid_at: nil }
      ]
    }
  end

  defp income_description_exists?(procedure_params) do
    procedure_params["description"] != nil && procedure_params["description"] != ""
  end

  defp procedure_income_changeset(struct, params) do
    struct
    |> cast(params, [:procedure_id, :income_id, :price, :due_date_at, :done, :paid_at, :attendance_date_at, :attendance_hour])
  end

  defp observations_changeset(struct, params) do
    struct
    |> cast(params, [:observation_id, :observation, :creation_date, :creation_hour])
  end
end