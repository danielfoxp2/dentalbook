defmodule DentalBookPresenter.GroupedIncomeUI do
  use DentalBookPresenter.Web, :model

  embedded_schema do
    field :start_date, :string
    field :end_date, :string
    field :procedure_amount_total, :string

    embeds_many :procedures, Procedure do
      field :procedure_id, :string
    end
  end

  def changeset(struct, params \\ %{}) do
    struct
     |> cast(params, [:start_date, :end_date])
     |> cast_embed(:procedures, with: &procedure_changeset/2)
  end

  defp procedure_changeset(struct, params) do
    struct
    |> cast(params, [:procedure_id])
  end

end